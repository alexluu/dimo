graph [
  directed 1
  multigraph 1
  last_utterance "313.-1.0.0&#10;CHI"
  root "313.-1.0.0&#10;CHI"
  corel_nodes "[]"
  node [
    id 0
    label "000.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 0
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 0"
  ]
  node [
    id 1
    label "000.09.1.0&#10;Brett"
    text "Brett"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "right"
    cats "nc"
    cats "ent"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: right&#10;cats: ['nc', 'ent']"
  ]
  node [
    id 2
    label "000.00.0.0&#10;"
    text ""
    shape "box"
    color "yellow"
    size 10
    syn_role "n/a"
    wrt_ROOT "n/a"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "!"
    title "syn_role: n/a&#10;wrt_ROOT: n/a&#10;cats: ['sfs']&#10;clause_type: !"
  ]
  node [
    id 3
    label "000.02.1.0&#10;I"
    text "I"
    shape "box"
    color "yellow"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "!"
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['sfs']&#10;clause_type: !"
  ]
  node [
    id 4
    label "001.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 1
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 1"
  ]
  node [
    id 5
    label "001.00.1.0&#10;Brett"
    text "Brett"
    shape "box"
    color "cyan"
    size 10
    syn_role "npadvmod (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "ent"
    title "syn_role: npadvmod (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['ent']"
  ]
  node [
    id 6
    label "001.02.1.0&#10;I"
    text "I"
    shape "box"
    color "yellow"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "."
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['sfs']&#10;clause_type: ."
  ]
  node [
    id 7
    label "002.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 2
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 2"
  ]
  node [
    id 8
    label "002.00.2.0&#10;what mom"
    text "what mom"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']"
  ]
  node [
    id 9
    label "003.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 3
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 3"
  ]
  node [
    id 10
    label "003.04.3.0&#10;your birthday party"
    text "your birthday party"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.6736481470665314, 0, 2, 2)]"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.6736481470665314, 0, 2, 2)]"
  ]
  node [
    id 11
    label "003.05.2.0&#10;birthday party"
    text "birthday party"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "wn_np"
    wn_sense "birthday_party.n.01"
    definition "a party held on the anniversary of someone's birth"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: right&#10;cats: ['wn_np']&#10;wn_sense: birthday_party.n.01&#10;definition: a party held on the anniversary of someone's birth"
  ]
  node [
    id 12
    label "004.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 4
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 4"
  ]
  node [
    id 13
    label "005.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 5
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 5"
  ]
  node [
    id 14
    label "005.02.1.0&#10;old"
    text "old"
    shape "box"
    color "pink"
    size 11
    syn_role "acomp (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.1
    title "syn_role: acomp (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['exp']&#10;polarity: 0.1"
  ]
  node [
    id 15
    label "005.10.1.0&#10;old"
    text "old"
    shape "box"
    color "pink"
    size 11
    syn_role "acomp (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.1
    title "syn_role: acomp (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['exp']&#10;polarity: 0.1"
  ]
  node [
    id 16
    label "005.12.1.0&#10;you"
    text "you"
    shape "box"
    color "yellow"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "?"
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['sfs']&#10;clause_type: ?"
  ]
  node [
    id 17
    label "006.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 6
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 6"
  ]
  node [
    id 18
    label "006.00.1.0&#10;seven"
    text "seven"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "ent"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['ent']"
  ]
  node [
    id 19
    label "007.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 7
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 7"
  ]
  node [
    id 20
    label "007.00.1.0&#10;Brett"
    text "Brett"
    shape "box"
    color "cyan"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "nc"
    cats "ent"
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['nc', 'ent']"
  ]
  node [
    id 21
    label "007.05.3.0&#10;your birthday party"
    text "your birthday party"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.9999999130441076, 4, 3, 3)]"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.9999999130441076, 4, 3, 3)]"
  ]
  node [
    id 22
    label "007.03.1.0&#10;me"
    text "me"
    shape "box"
    color "yellow"
    size 10
    syn_role "dobj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "."
    title "syn_role: dobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['sfs']&#10;clause_type: ."
  ]
  node [
    id 23
    label "007.06.2.0&#10;birthday party"
    text "birthday party"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "wn_np"
    wn_sense "birthday_party.n.01"
    definition "a party held on the anniversary of someone's birth"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: right&#10;cats: ['wn_np']&#10;wn_sense: birthday_party.n.01&#10;definition: a party held on the anniversary of someone's birth"
  ]
  node [
    id 24
    label "008.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 8
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 8"
  ]
  node [
    id 25
    label "008.04.4.0&#10;some real good looking"
    text "some real good looking"
    shape "box"
    color "cyan"
    size 10
    syn_role "dobj (head: ccomp)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7559426266191352, 5, 3, 7)]"
    title "syn_role: dobj (head: ccomp)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.7559426266191352, 5, 3, 7)]"
  ]
  node [
    id 26
    label "008.05.1.0&#10;real"
    text "real"
    shape "box"
    color "pink"
    size 12
    syn_role "amod (head: dobj)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.2
    title "syn_role: amod (head: dobj)&#10;wrt_ROOT: right&#10;cats: ['exp']&#10;polarity: 0.2"
  ]
  node [
    id 27
    label "008.06.1.0&#10;good"
    text "good"
    shape "box"
    color "pink"
    size 17
    syn_role "amod (head: dobj)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.7
    title "syn_role: amod (head: dobj)&#10;wrt_ROOT: right&#10;cats: ['exp']&#10;polarity: 0.7"
  ]
  node [
    id 28
    label "009.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 9
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 9"
  ]
  node [
    id 29
    label "009.00.2.0&#10;what mom"
    text "what mom"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.6379101919364686, 4, 4, 8)]"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']&#10;corels: [(0.6379101919364686, 4, 4, 8)]"
  ]
  node [
    id 30
    label "010.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 10
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 10"
  ]
  node [
    id 31
    label "010.00.1.0&#10;Brett"
    text "Brett"
    shape "box"
    color "cyan"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "nc"
    cats "ent"
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['nc', 'ent']"
  ]
  node [
    id 32
    label "010.05.3.0&#10;your birthday party"
    text "your birthday party"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.6736481470665314, 0, 2, 9)]"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.6736481470665314, 0, 2, 9)]"
  ]
  node [
    id 33
    label "010.03.1.0&#10;me"
    text "me"
    shape "box"
    color "yellow"
    size 10
    syn_role "dobj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "."
    title "syn_role: dobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['sfs']&#10;clause_type: ."
  ]
  node [
    id 34
    label "010.06.2.0&#10;birthday party"
    text "birthday party"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "wn_np"
    wn_sense "birthday_party.n.01"
    definition "a party held on the anniversary of someone's birth"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: right&#10;cats: ['wn_np']&#10;wn_sense: birthday_party.n.01&#10;definition: a party held on the anniversary of someone's birth"
  ]
  node [
    id 35
    label "011.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 11
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 11"
  ]
  node [
    id 36
    label "012.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 12
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 12"
  ]
  node [
    id 37
    label "012.03.3.0&#10;some good looking"
    text "some good looking"
    shape "box"
    color "cyan"
    size 10
    syn_role "dobj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7559426266191352, 5, 3, 10)]"
    title "syn_role: dobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.7559426266191352, 5, 3, 10)]"
  ]
  node [
    id 38
    label "012.00.1.0&#10;I"
    text "I"
    shape "box"
    color "yellow"
    size 10
    syn_role "nsubj (head: nsubj)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "."
    title "syn_role: nsubj (head: nsubj)&#10;wrt_ROOT: left&#10;cats: ['sfs']&#10;clause_type: ."
  ]
  node [
    id 39
    label "012.04.1.0&#10;good"
    text "good"
    shape "box"
    color "pink"
    size 17
    syn_role "amod (head: dobj)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.7
    title "syn_role: amod (head: dobj)&#10;wrt_ROOT: right&#10;cats: ['exp']&#10;polarity: 0.7"
  ]
  node [
    id 40
    label "012.03.3.1&#10;Good Lookin' (music work rendition)"
    entity_name "Good Lookin'"
    entity_type "music work rendition"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicWorkRendition&#34;, &#34;GoodLookinEttaJames::jr4qt&#34;]"
    comment "Good Lookin' can be a music work rendition or music work."
    source "MusicBrainz Database"
    certainty_type "quite_certain"
    title "Wolfram_expression: Entity[&#34;MusicWorkRendition&#34;, &#34;GoodLookinEttaJames::jr4qt&#34;]&#10;comment: Good Lookin' can be a music work rendition or music work.&#10;source: MusicBrainz Database"
  ]
  node [
    id 41
    label "012.03.3.2&#10;Tuff Lover (canonical album)"
    entity_name "Tuff Lover"
    entity_type "canonical album"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicAlbum&#34;, &#34;TuffLover::4f8mv&#34;]"
    comment "According to MusicBrainz Database, the canonical album, relating to the music work rendition, is Tuff Lover."
    source "MusicBrainz Database"
    certainty_type "absolutely_certain"
    title "Wolfram_expression: Entity[&#34;MusicAlbum&#34;, &#34;TuffLover::4f8mv&#34;]&#10;comment: According to MusicBrainz Database, the canonical album, relating to the music work rendition, is Tuff Lover.&#10;source: MusicBrainz Database"
  ]
  node [
    id 42
    label "012.03.3.3&#10;Good Lookin' (canonical recording)"
    entity_name "Good Lookin'"
    entity_type "canonical recording"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicWorkRecording&#34;, &#34;GoodLookin::y5cvk&#34;]"
    comment "According to MusicBrainz Database, the canonical recording, relating to the music work rendition, is also named Good Lookin'."
    source "MusicBrainz Database"
    certainty_type "absolutely_certain"
    title "Wolfram_expression: Entity[&#34;MusicWorkRecording&#34;, &#34;GoodLookin::y5cvk&#34;]&#10;comment: According to MusicBrainz Database, the canonical recording, relating to the music work rendition, is also named Good Lookin'.&#10;source: MusicBrainz Database"
  ]
  node [
    id 43
    label "013.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 13
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 13"
  ]
  node [
    id 44
    label "013.03.3.0&#10;your birthday party"
    text "your birthday party"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7559426266191352, 3, 3, 12)]"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.7559426266191352, 3, 3, 12)]"
  ]
  node [
    id 45
    label "013.01.1.0&#10;me"
    text "me"
    shape "box"
    color "yellow"
    size 10
    syn_role "dobj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "."
    title "syn_role: dobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['sfs']&#10;clause_type: ."
  ]
  node [
    id 46
    label "013.04.2.0&#10;birthday party"
    text "birthday party"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "wn_np"
    wn_sense "birthday_party.n.01"
    definition "a party held on the anniversary of someone's birth"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: right&#10;cats: ['wn_np']&#10;wn_sense: birthday_party.n.01&#10;definition: a party held on the anniversary of someone's birth"
  ]
  node [
    id 47
    label "014.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 14
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 14"
  ]
  node [
    id 48
    label "014.04.3.0&#10;your birthday party"
    text "your birthday party"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.9999999130441076, 3, 3, 13)]"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.9999999130441076, 3, 3, 13)]"
  ]
  node [
    id 49
    label "014.01.1.0&#10;me"
    text "me"
    shape "box"
    color "yellow"
    size 10
    syn_role "dative (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "."
    title "syn_role: dative (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['sfs']&#10;clause_type: ."
  ]
  node [
    id 50
    label "014.05.2.0&#10;birthday party"
    text "birthday party"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "wn_np"
    wn_sense "birthday_party.n.01"
    definition "a party held on the anniversary of someone's birth"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: right&#10;cats: ['wn_np']&#10;wn_sense: birthday_party.n.01&#10;definition: a party held on the anniversary of someone's birth"
  ]
  node [
    id 51
    label "015.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 15
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 15"
  ]
  node [
    id 52
    label "015.02.3.0&#10;my birthday party"
    text "my birthday party"
    shape "box"
    color "cyan"
    size 10
    syn_role "dobj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.9999999130441076, 4, 3, 14)]"
    title "syn_role: dobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.9999999130441076, 4, 3, 14)]"
  ]
  node [
    id 53
    label "015.08.1.0&#10;seven"
    text "seven"
    shape "box"
    color "cyan"
    size 10
    syn_role "acomp (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "ent"
    title "syn_role: acomp (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['ent']"
  ]
  node [
    id 54
    label "015.00.1.0&#10;I"
    text "I"
    shape "box"
    color "yellow"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "."
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['sfs']&#10;clause_type: ."
  ]
  node [
    id 55
    label "015.06.1.0&#10;I"
    text "I"
    shape "box"
    color "yellow"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "."
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['sfs']&#10;clause_type: ."
  ]
  node [
    id 56
    label "015.03.2.0&#10;birthday party"
    text "birthday party"
    shape "box"
    color "cyan"
    size 10
    syn_role "dobj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "wn_np"
    wn_sense "birthday_party.n.01"
    definition "a party held on the anniversary of someone's birth"
    title "syn_role: dobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['wn_np']&#10;wn_sense: birthday_party.n.01&#10;definition: a party held on the anniversary of someone's birth"
  ]
  node [
    id 57
    label "016.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 16
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 16"
  ]
  node [
    id 58
    label "017.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 17
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 17"
  ]
  node [
    id 59
    label "017.03.2.0&#10;my birthday"
    text "my birthday"
    shape "box"
    color "cyan"
    size 10
    syn_role "dobj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7574771986438121, 2, 3, 15)]"
    title "syn_role: dobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.7574771986438121, 2, 3, 15)]"
  ]
  node [
    id 60
    label "017.01.1.0&#10;I"
    text "I"
    shape "box"
    color "yellow"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "."
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['sfs']&#10;clause_type: ."
  ]
  node [
    id 61
    label "018.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 18
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 18"
  ]
  node [
    id 62
    label "019.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 19
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 19"
  ]
  node [
    id 63
    label "019.02.2.0&#10;a cake"
    text "a cake"
    shape "box"
    color "cyan"
    size 10
    syn_role "dobj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.609895437266936, 3, 2, 17)]"
    title "syn_role: dobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.609895437266936, 3, 2, 17)]"
  ]
  node [
    id 64
    label "019.00.1.0&#10;I"
    text "I"
    shape "box"
    color "yellow"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "."
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['sfs']&#10;clause_type: ."
  ]
  node [
    id 65
    label "020.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 20
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 20"
  ]
  node [
    id 66
    label "021.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 21
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 21"
  ]
  node [
    id 67
    label "021.03.2.0&#10;a pinwheel"
    text "a pinwheel"
    shape "box"
    color "cyan"
    size 10
    syn_role "dobj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.6612148075920553, 2, 2, 19)]"
    title "syn_role: dobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.6612148075920553, 2, 2, 19)]"
  ]
  node [
    id 68
    label "021.01.1.0&#10;I"
    text "I"
    shape "box"
    color "yellow"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "."
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['sfs']&#10;clause_type: ."
  ]
  node [
    id 69
    label "022.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 22
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 22"
  ]
  node [
    id 70
    label "023.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 23
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 23"
  ]
  node [
    id 71
    label "023.02.1.0&#10;you"
    text "you"
    shape "box"
    color "yellow"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "?"
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['sfs']&#10;clause_type: ?"
  ]
  node [
    id 72
    label "024.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 24
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 24"
  ]
  node [
    id 73
    label "024.02.7.0&#10;[/?] cookie , cake"
    text "[/?] cookie , cake"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.6612148075920553, 3, 2, 21)]"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']&#10;corels: [(0.6612148075920553, 3, 2, 21)]"
  ]
  node [
    id 74
    label "024.00.2.0&#10;&#38;-um"
    text "&#38;-um"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "ent"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['ent']"
  ]
  node [
    id 75
    label "025.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 25
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 25"
  ]
  node [
    id 76
    label "026.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 26
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 26"
  ]
  node [
    id 77
    label "027.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 27
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 27"
  ]
  node [
    id 78
    label "028.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 28
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 28"
  ]
  node [
    id 79
    label "028.00.4.0&#10;oh , my pinatas@s"
    text "oh , my pinatas@s"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']"
  ]
  node [
    id 80
    label "028.05.1.0&#10;spa"
    text "spa"
    shape "box"
    color "cyan"
    size 10
    syn_role "appos (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.6673593283774537, 2, 7, 24)]"
    title "syn_role: appos (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.6673593283774537, 2, 7, 24)]"
  ]
  node [
    id 81
    label "029.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 29
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 29"
  ]
  node [
    id 82
    label "030.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 30
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 30"
  ]
  node [
    id 83
    label "030.04.2.0&#10;little pieces"
    text "little pieces"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.622096043686025, 5, 1, 28)]"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.622096043686025, 5, 1, 28)]"
  ]
  node [
    id 84
    label "030.04.1.0&#10;little"
    text "little"
    shape "box"
    color "pink"
    size 12
    syn_role "amod (head: pobj)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "exp"
    polarity -0.1875
    title "syn_role: amod (head: pobj)&#10;wrt_ROOT: right&#10;cats: ['exp']&#10;polarity: -0.1875"
  ]
  node [
    id 85
    label "031.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 31
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 31"
  ]
  node [
    id 86
    label "032.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 32
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 32"
  ]
  node [
    id 87
    label "033.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 33
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 33"
  ]
  node [
    id 88
    label "033.05.2.0&#10;the trash"
    text "the trash"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7034903316345432, 4, 2, 30)]"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.7034903316345432, 4, 2, 30)]"
  ]
  node [
    id 89
    label "034.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 34
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 34"
  ]
  node [
    id 90
    label "034.03.2.0&#10;the pinata@s"
    text "the pinata@s"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: right&#10;cats: ['nc']"
  ]
  node [
    id 91
    label "034.06.1.0&#10;spa"
    text "spa"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.6620885460992072, 5, 2, 33)]"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']&#10;corels: [(0.6620885460992072, 5, 2, 33)]"
  ]
  node [
    id 92
    label "034.08.1.0&#10;you"
    text "you"
    shape "box"
    color "yellow"
    size 10
    syn_role "nsubj (head: advcl)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "?"
    title "syn_role: nsubj (head: advcl)&#10;wrt_ROOT: right&#10;cats: ['sfs']&#10;clause_type: ?"
  ]
  node [
    id 93
    label "035.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 35
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 35"
  ]
  node [
    id 94
    label "035.00.2.0&#10;a candys"
    text "a candys"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7183236082220523, 6, 1, 34)]"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']&#10;corels: [(0.7183236082220523, 6, 1, 34)]"
  ]
  node [
    id 95
    label "036.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 36
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 36"
  ]
  node [
    id 96
    label "036.03.2.0&#10;some candys"
    text "some candys"
    shape "box"
    color "cyan"
    size 10
    syn_role "dobj (head: ccomp)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.9999999882960363, 0, 2, 35)]"
    title "syn_role: dobj (head: ccomp)&#10;wrt_ROOT: left&#10;cats: ['nc']&#10;corels: [(0.9999999882960363, 0, 2, 35)]"
  ]
  node [
    id 97
    label "036.06.2.0&#10;the pinata@s"
    text "the pinata@s"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "nc"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: left&#10;cats: ['nc']"
  ]
  node [
    id 98
    label "036.01.1.0&#10;you"
    text "you"
    shape "box"
    color "yellow"
    size 10
    syn_role "nsubj (head: ccomp)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "?"
    title "syn_role: nsubj (head: ccomp)&#10;wrt_ROOT: left&#10;cats: ['sfs']&#10;clause_type: ?"
  ]
  node [
    id 99
    label "037.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 37
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 37"
  ]
  node [
    id 100
    label "038.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 38
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 38"
  ]
  node [
    id 101
    label "038.01.1.0&#10;I"
    text "I"
    shape "box"
    color "yellow"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "."
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['sfs']&#10;clause_type: ."
  ]
  node [
    id 102
    label "039.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 39
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 39"
  ]
  node [
    id 103
    label "040.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 40
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 40"
  ]
  node [
    id 104
    label "041.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 41
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 41"
  ]
  node [
    id 105
    label "041.01.1.0&#10;sure"
    text "sure"
    shape "box"
    color "pink"
    size 15
    syn_role "advmod (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.5
    title "syn_role: advmod (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['exp']&#10;polarity: 0.5"
  ]
  node [
    id 106
    label "042.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 42
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 42"
  ]
  node [
    id 107
    label "042.05.2.0&#10;the trash"
    text "the trash"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.6236684595321675, 3, 2, 36)]"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.6236684595321675, 3, 2, 36)]"
  ]
  node [
    id 108
    label "043.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 43
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 43"
  ]
  node [
    id 109
    label "043.04.2.0&#10;the dump"
    text "the dump"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.8344216858801877, 5, 2, 42)]"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.8344216858801877, 5, 2, 42)]"
  ]
  node [
    id 110
    label "044.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 44
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 44"
  ]
  node [
    id 111
    label "044.00.1.0&#10;okay"
    text "okay"
    shape "box"
    color "pink"
    size 15
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.5
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['exp']&#10;polarity: 0.5"
  ]
  node [
    id 112
    label "045.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 45
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 45"
  ]
  node [
    id 113
    label "045.23.2.0&#10;your party"
    text "your party"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7311073075431102, 4, 2, 43)]"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.7311073075431102, 4, 2, 43)]"
  ]
  node [
    id 114
    label "045.23.2.1&#10;Your Party (music work rendition)"
    entity_name "Your Party"
    entity_type "music work rendition"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicWorkRendition&#34;, &#34;YourPartyWeen::668n9&#34;]"
    comment "Your Party can be a music work rendition or music work."
    source "MusicBrainz Database"
    certainty_type "quite_certain"
    title "Wolfram_expression: Entity[&#34;MusicWorkRendition&#34;, &#34;YourPartyWeen::668n9&#34;]&#10;comment: Your Party can be a music work rendition or music work.&#10;source: MusicBrainz Database"
  ]
  node [
    id 115
    label "045.23.2.2&#10;La Cucaracha (canonical album)"
    entity_name "La Cucaracha"
    entity_type "canonical album"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicAlbum&#34;, &#34;LaCucaracha::bdf4x&#34;]"
    comment "According to MusicBrainz Database, the canonical album, relating to the music work rendition, is La Cucaracha."
    source "MusicBrainz Database"
    certainty_type "absolutely_certain"
    title "Wolfram_expression: Entity[&#34;MusicAlbum&#34;, &#34;LaCucaracha::bdf4x&#34;]&#10;comment: According to MusicBrainz Database, the canonical album, relating to the music work rendition, is La Cucaracha.&#10;source: MusicBrainz Database"
  ]
  node [
    id 116
    label "045.23.2.3&#10;Your Party (canonical recording)"
    entity_name "Your Party"
    entity_type "canonical recording"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicWorkRecording&#34;, &#34;YourParty::6xnp5&#34;]"
    comment "According to MusicBrainz Database, the canonical recording, relating to the music work rendition, is also named Your Party."
    source "MusicBrainz Database"
    certainty_type "absolutely_certain"
    title "Wolfram_expression: Entity[&#34;MusicWorkRecording&#34;, &#34;YourParty::6xnp5&#34;]&#10;comment: According to MusicBrainz Database, the canonical recording, relating to the music work rendition, is also named Your Party.&#10;source: MusicBrainz Database"
  ]
  node [
    id 117
    label "046.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 46
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 46"
  ]
  node [
    id 118
    label "047.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 47
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 47"
  ]
  node [
    id 119
    label "047.09.4.0&#10;then <that mixes"
    text "then <that mixes"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7746215455607665, 23, 2, 45)]"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']&#10;corels: [(0.7746215455607665, 23, 2, 45)]"
  ]
  node [
    id 120
    label "048.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 48
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 48"
  ]
  node [
    id 121
    label "048.03.2.0&#10;your party"
    text "your party"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7191380942504905, 9, 4, 47)]"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.7191380942504905, 9, 4, 47)]"
  ]
  node [
    id 122
    label "048.03.2.1&#10;Your Party (music work rendition)"
    entity_name "Your Party"
    entity_type "music work rendition"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicWorkRendition&#34;, &#34;YourPartyWeen::668n9&#34;]"
    comment "Your Party can be a music work rendition or music work."
    source "MusicBrainz Database"
    certainty_type "quite_certain"
    title "Wolfram_expression: Entity[&#34;MusicWorkRendition&#34;, &#34;YourPartyWeen::668n9&#34;]&#10;comment: Your Party can be a music work rendition or music work.&#10;source: MusicBrainz Database"
  ]
  node [
    id 123
    label "048.03.2.2&#10;La Cucaracha (canonical album)"
    entity_name "La Cucaracha"
    entity_type "canonical album"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicAlbum&#34;, &#34;LaCucaracha::bdf4x&#34;]"
    comment "According to MusicBrainz Database, the canonical album, relating to the music work rendition, is La Cucaracha."
    source "MusicBrainz Database"
    certainty_type "absolutely_certain"
    title "Wolfram_expression: Entity[&#34;MusicAlbum&#34;, &#34;LaCucaracha::bdf4x&#34;]&#10;comment: According to MusicBrainz Database, the canonical album, relating to the music work rendition, is La Cucaracha.&#10;source: MusicBrainz Database"
  ]
  node [
    id 124
    label "048.03.2.3&#10;Your Party (canonical recording)"
    entity_name "Your Party"
    entity_type "canonical recording"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicWorkRecording&#34;, &#34;YourParty::6xnp5&#34;]"
    comment "According to MusicBrainz Database, the canonical recording, relating to the music work rendition, is also named Your Party."
    source "MusicBrainz Database"
    certainty_type "absolutely_certain"
    title "Wolfram_expression: Entity[&#34;MusicWorkRecording&#34;, &#34;YourParty::6xnp5&#34;]&#10;comment: According to MusicBrainz Database, the canonical recording, relating to the music work rendition, is also named Your Party.&#10;source: MusicBrainz Database"
  ]
  node [
    id 125
    label "049.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 49
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 49"
  ]
  node [
    id 126
    label "049.09.2.0&#10;your party"
    text "your party"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.8485926386983647, 3, 2, 48)]"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.8485926386983647, 3, 2, 48)]"
  ]
  node [
    id 127
    label "049.00.1.0&#10;Brett"
    text "Brett"
    shape "box"
    color "cyan"
    size 10
    syn_role "npadvmod (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "ent"
    title "syn_role: npadvmod (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['ent']"
  ]
  node [
    id 128
    label "049.02.1.0&#10;I"
    text "I"
    shape "box"
    color "yellow"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "."
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['sfs']&#10;clause_type: ."
  ]
  node [
    id 129
    label "049.09.2.1&#10;Your Party (music work rendition)"
    entity_name "Your Party"
    entity_type "music work rendition"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicWorkRendition&#34;, &#34;YourPartyWeen::668n9&#34;]"
    comment "Your Party can be a music work rendition or music work."
    source "MusicBrainz Database"
    certainty_type "quite_certain"
    title "Wolfram_expression: Entity[&#34;MusicWorkRendition&#34;, &#34;YourPartyWeen::668n9&#34;]&#10;comment: Your Party can be a music work rendition or music work.&#10;source: MusicBrainz Database"
  ]
  node [
    id 130
    label "049.09.2.2&#10;La Cucaracha (canonical album)"
    entity_name "La Cucaracha"
    entity_type "canonical album"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicAlbum&#34;, &#34;LaCucaracha::bdf4x&#34;]"
    comment "According to MusicBrainz Database, the canonical album, relating to the music work rendition, is La Cucaracha."
    source "MusicBrainz Database"
    certainty_type "absolutely_certain"
    title "Wolfram_expression: Entity[&#34;MusicAlbum&#34;, &#34;LaCucaracha::bdf4x&#34;]&#10;comment: According to MusicBrainz Database, the canonical album, relating to the music work rendition, is La Cucaracha.&#10;source: MusicBrainz Database"
  ]
  node [
    id 131
    label "049.09.2.3&#10;Your Party (canonical recording)"
    entity_name "Your Party"
    entity_type "canonical recording"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicWorkRecording&#34;, &#34;YourParty::6xnp5&#34;]"
    comment "According to MusicBrainz Database, the canonical recording, relating to the music work rendition, is also named Your Party."
    source "MusicBrainz Database"
    certainty_type "absolutely_certain"
    title "Wolfram_expression: Entity[&#34;MusicWorkRecording&#34;, &#34;YourParty::6xnp5&#34;]&#10;comment: According to MusicBrainz Database, the canonical recording, relating to the music work rendition, is also named Your Party.&#10;source: MusicBrainz Database"
  ]
  node [
    id 132
    label "050.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 50
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 50"
  ]
  node [
    id 133
    label "050.00.1.0&#10;Peggy"
    text "Peggy"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "nc"
    cats "ent"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc', 'ent']"
  ]
  node [
    id 134
    label "050.03.1.0&#10;Charlene"
    text "Charlene"
    shape "box"
    color "cyan"
    size 10
    syn_role "conj (head: ROOT)"
    wrt_ROOT "right"
    cats "nc"
    cats "ent"
    title "syn_role: conj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc', 'ent']"
  ]
  node [
    id 135
    label "050.19.2.0&#10;and nana"
    text "and nana"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']"
  ]
  node [
    id 136
    label "050.22.1.0&#10;Paul"
    text "Paul"
    shape "box"
    color "cyan"
    size 10
    syn_role "conj (head: ROOT)"
    wrt_ROOT "right"
    cats "nc"
    cats "ent"
    corels "[(0.6368795676771811, 9, 2, 49)]"
    title "syn_role: conj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc', 'ent']&#10;corels: [(0.6368795676771811, 9, 2, 49)]"
  ]
  node [
    id 137
    label "050.20.1.0&#10;nana"
    text "nana"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "ent"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['ent']"
  ]
  node [
    id 138
    label "051.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 51
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 51"
  ]
  node [
    id 139
    label "051.01.1.0&#10;Uncle_Glenn"
    text "Uncle_Glenn"
    shape "box"
    color "cyan"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "nc"
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['nc']"
  ]
  node [
    id 140
    label "052.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 52
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 52"
  ]
  node [
    id 141
    label "053.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 53
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 53"
  ]
  node [
    id 142
    label "053.03.2.0&#10;a pinwheel"
    text "a pinwheel"
    shape "box"
    color "cyan"
    size 10
    syn_role "dobj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.6200081108404467, 22, 1, 50)]"
    title "syn_role: dobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.6200081108404467, 22, 1, 50)]"
  ]
  node [
    id 143
    label "053.01.1.0&#10;I"
    text "I"
    shape "box"
    color "yellow"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "."
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['sfs']&#10;clause_type: ."
  ]
  node [
    id 144
    label "054.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 54
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 54"
  ]
  node [
    id 145
    label "054.02.3.0&#10;some other people"
    text "some other people"
    shape "box"
    color "cyan"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.6716958699588885, 3, 2, 53)]"
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['nc']&#10;corels: [(0.6716958699588885, 3, 2, 53)]"
  ]
  node [
    id 146
    label "054.07.2.0&#10;your party"
    text "your party"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.6405473590565586, 0, 4, 28)]"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.6405473590565586, 0, 4, 28)]"
  ]
  node [
    id 147
    label "054.03.1.0&#10;other"
    text "other"
    shape "box"
    color "pink"
    size 11
    syn_role "amod (head: nsubj)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "exp"
    polarity -0.125
    title "syn_role: amod (head: nsubj)&#10;wrt_ROOT: left&#10;cats: ['exp']&#10;polarity: -0.125"
  ]
  node [
    id 148
    label "054.07.2.1&#10;Your Party (music work rendition)"
    entity_name "Your Party"
    entity_type "music work rendition"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicWorkRendition&#34;, &#34;YourPartyWeen::668n9&#34;]"
    comment "Your Party can be a music work rendition or music work."
    source "MusicBrainz Database"
    certainty_type "quite_certain"
    title "Wolfram_expression: Entity[&#34;MusicWorkRendition&#34;, &#34;YourPartyWeen::668n9&#34;]&#10;comment: Your Party can be a music work rendition or music work.&#10;source: MusicBrainz Database"
  ]
  node [
    id 149
    label "054.07.2.2&#10;La Cucaracha (canonical album)"
    entity_name "La Cucaracha"
    entity_type "canonical album"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicAlbum&#34;, &#34;LaCucaracha::bdf4x&#34;]"
    comment "According to MusicBrainz Database, the canonical album, relating to the music work rendition, is La Cucaracha."
    source "MusicBrainz Database"
    certainty_type "absolutely_certain"
    title "Wolfram_expression: Entity[&#34;MusicAlbum&#34;, &#34;LaCucaracha::bdf4x&#34;]&#10;comment: According to MusicBrainz Database, the canonical album, relating to the music work rendition, is La Cucaracha.&#10;source: MusicBrainz Database"
  ]
  node [
    id 150
    label "054.07.2.3&#10;Your Party (canonical recording)"
    entity_name "Your Party"
    entity_type "canonical recording"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicWorkRecording&#34;, &#34;YourParty::6xnp5&#34;]"
    comment "According to MusicBrainz Database, the canonical recording, relating to the music work rendition, is also named Your Party."
    source "MusicBrainz Database"
    certainty_type "absolutely_certain"
    title "Wolfram_expression: Entity[&#34;MusicWorkRecording&#34;, &#34;YourParty::6xnp5&#34;]&#10;comment: According to MusicBrainz Database, the canonical recording, relating to the music work rendition, is also named Your Party.&#10;source: MusicBrainz Database"
  ]
  node [
    id 151
    label "055.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 55
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 55"
  ]
  node [
    id 152
    label "055.04.2.0&#10;the pinwheel"
    text "the pinwheel"
    shape "box"
    color "cyan"
    size 10
    syn_role "dobj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.6900641941051103, 7, 2, 54)]"
    title "syn_role: dobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.6900641941051103, 7, 2, 54)]"
  ]
  node [
    id 153
    label "055.02.1.0&#10;I"
    text "I"
    shape "box"
    color "yellow"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "."
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['sfs']&#10;clause_type: ."
  ]
  node [
    id 154
    label "056.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 56
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 56"
  ]
  node [
    id 155
    label "056.08.2.0&#10;some guys"
    text "some guys"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7947118469379837, 2, 3, 54)]"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']&#10;corels: [(0.7947118469379837, 2, 3, 54)]"
  ]
  node [
    id 156
    label "056.02.1.0&#10;short"
    text "short"
    shape "box"
    color "pink"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.0
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['exp']&#10;polarity: 0.0"
  ]
  node [
    id 157
    label "057.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 57
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 57"
  ]
  node [
    id 158
    label "057.03.2.0&#10;the pinwheel"
    text "the pinwheel"
    shape "box"
    color "cyan"
    size 10
    syn_role "dobj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(1.0000000108908578, 4, 2, 55)]"
    title "syn_role: dobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(1.0000000108908578, 4, 2, 55)]"
  ]
  node [
    id 159
    label "057.01.1.0&#10;I"
    text "I"
    shape "box"
    color "yellow"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "."
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['sfs']&#10;clause_type: ."
  ]
  node [
    id 160
    label "058.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 58
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 58"
  ]
  node [
    id 161
    label "059.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 59
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 59"
  ]
  node [
    id 162
    label "059.09.2.0&#10;your friends"
    text "your friends"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.6521466064818977, 8, 2, 56)]"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: left&#10;cats: ['nc']&#10;corels: [(0.6521466064818977, 8, 2, 56)]"
  ]
  node [
    id 163
    label "059.13.2.0&#10;the party"
    text "the party"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.6539257798908481, 3, 2, 57)]"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.6539257798908481, 3, 2, 57)]"
  ]
  node [
    id 164
    label "059.05.1.0&#10;Brett"
    text "Brett"
    shape "box"
    color "cyan"
    size 10
    syn_role "npadvmod (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "ent"
    title "syn_role: npadvmod (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['ent']"
  ]
  node [
    id 165
    label "059.13.2.1&#10;Tthhee Ppaarrttyy (music work rendition)"
    entity_name "Tthhee Ppaarrttyy"
    entity_type "music work rendition"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicWorkRendition&#34;, &#34;TthheePpaarrttyyJustice::432kr&#34;]"
    comment "Tthhee Ppaarrttyy can be a movie, music work rendition or music work."
    source "MusicBrainz Database"
    certainty_type "quite_certain"
    title "Wolfram_expression: Entity[&#34;MusicWorkRendition&#34;, &#34;TthheePpaarrttyyJustice::432kr&#34;]&#10;comment: Tthhee Ppaarrttyy can be a movie, music work rendition or music work.&#10;source: MusicBrainz Database"
  ]
  node [
    id 166
    label "059.13.2.2&#10;&#10013; (canonical album)"
    entity_name "&#10013;"
    entity_type "canonical album"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicAlbum&#34;, &#34;Cross::fd2rr&#34;]"
    comment "According to MusicBrainz Database, the canonical album, relating to the music work rendition, is &#10013;."
    source "MusicBrainz Database"
    certainty_type "absolutely_certain"
    title "Wolfram_expression: Entity[&#34;MusicAlbum&#34;, &#34;Cross::fd2rr&#34;]&#10;comment: According to MusicBrainz Database, the canonical album, relating to the music work rendition, is &#10013;.&#10;source: MusicBrainz Database"
  ]
  node [
    id 167
    label "059.13.2.3&#10;Tthhee Ppaarrttyy (canonical recording)"
    entity_name "Tthhee Ppaarrttyy"
    entity_type "canonical recording"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicWorkRecording&#34;, &#34;TthheePpaarrttyy::b5wp7&#34;]"
    comment "According to MusicBrainz Database, the canonical recording, relating to the music work rendition, is also named Tthhee Ppaarrttyy."
    source "MusicBrainz Database"
    certainty_type "absolutely_certain"
    title "Wolfram_expression: Entity[&#34;MusicWorkRecording&#34;, &#34;TthheePpaarrttyy::b5wp7&#34;]&#10;comment: According to MusicBrainz Database, the canonical recording, relating to the music work rendition, is also named Tthhee Ppaarrttyy.&#10;source: MusicBrainz Database"
  ]
  node [
    id 168
    label "060.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 60
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 60"
  ]
  node [
    id 169
    label "060.02.2.0&#10;your friends"
    text "your friends"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(1.0000000355020076, 9, 2, 59)]"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(1.0000000355020076, 9, 2, 59)]"
  ]
  node [
    id 170
    label "060.05.1.0&#10;school"
    text "school"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.687523613856338, 13, 2, 59)]"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.687523613856338, 13, 2, 59)]"
  ]
  node [
    id 171
    label "061.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 61
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 61"
  ]
  node [
    id 172
    label "061.00.1.0&#10;fly"
    text "fly"
    shape "box"
    color "pink"
    size 18
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.8
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['exp']&#10;polarity: 0.8"
  ]
  node [
    id 173
    label "062.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 62
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 62"
  ]
  node [
    id 174
    label "062.02.2.0&#10;a fly"
    text "a fly"
    shape "box"
    color "cyan"
    size 10
    syn_role "attr (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.6585163387160425, 5, 1, 60)]"
    title "syn_role: attr (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.6585163387160425, 5, 1, 60)]"
  ]
  node [
    id 175
    label "062.03.1.0&#10;fly"
    text "fly"
    shape "box"
    color "pink"
    size 18
    syn_role "attr (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.8
    title "syn_role: attr (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['exp']&#10;polarity: 0.8"
  ]
  node [
    id 176
    label "063.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 63
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 63"
  ]
  node [
    id 177
    label "063.01.1.0&#10;you"
    text "you"
    shape "box"
    color "yellow"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "?"
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['sfs']&#10;clause_type: ?"
  ]
  node [
    id 178
    label "064.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 64
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 64"
  ]
  node [
    id 179
    label "064.00.2.0&#10;&#38;-um"
    text "&#38;-um"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "ent"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['ent']"
  ]
  node [
    id 180
    label "064.10.1.0&#10;later"
    text "later"
    shape "box"
    color "pink"
    size 10
    syn_role "advmod (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.0
    title "syn_role: advmod (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['exp']&#10;polarity: 0.0"
  ]
  node [
    id 181
    label "064.06.1.0&#10;I"
    text "I"
    shape "box"
    color "yellow"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "."
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['sfs']&#10;clause_type: ."
  ]
  node [
    id 182
    label "065.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 65
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 65"
  ]
  node [
    id 183
    label "065.02.1.0&#10;busy"
    text "busy"
    shape "box"
    color "pink"
    size 11
    syn_role "acomp (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.1
    title "syn_role: acomp (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['exp']&#10;polarity: 0.1"
  ]
  node [
    id 184
    label "065.00.1.0&#10;I"
    text "I"
    shape "box"
    color "yellow"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "."
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['sfs']&#10;clause_type: ."
  ]
  node [
    id 185
    label "066.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 66
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 66"
  ]
  node [
    id 186
    label "066.14.2.0&#10;some friends"
    text "some friends"
    shape "box"
    color "cyan"
    size 10
    syn_role "dobj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(1.0000000355020076, 2, 2, 60)]"
    title "syn_role: dobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(1.0000000355020076, 2, 2, 60)]"
  ]
  node [
    id 187
    label "066.19.2.0&#10;your party"
    text "your party"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.6770380472070083, 2, 2, 62)]"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.6770380472070083, 2, 2, 62)]"
  ]
  node [
    id 188
    label "066.00.1.0&#10;Brett"
    text "Brett"
    shape "box"
    color "cyan"
    size 10
    syn_role "npadvmod (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "ent"
    title "syn_role: npadvmod (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['ent']"
  ]
  node [
    id 189
    label "066.12.1.0&#10;you"
    text "you"
    shape "box"
    color "yellow"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "?"
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['sfs']&#10;clause_type: ?"
  ]
  node [
    id 190
    label "066.19.2.1&#10;Your Party (music work rendition)"
    entity_name "Your Party"
    entity_type "music work rendition"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicWorkRendition&#34;, &#34;YourPartyWeen::668n9&#34;]"
    comment "Your Party can be a music work rendition or music work."
    source "MusicBrainz Database"
    certainty_type "quite_certain"
    title "Wolfram_expression: Entity[&#34;MusicWorkRendition&#34;, &#34;YourPartyWeen::668n9&#34;]&#10;comment: Your Party can be a music work rendition or music work.&#10;source: MusicBrainz Database"
  ]
  node [
    id 191
    label "066.19.2.2&#10;La Cucaracha (canonical album)"
    entity_name "La Cucaracha"
    entity_type "canonical album"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicAlbum&#34;, &#34;LaCucaracha::bdf4x&#34;]"
    comment "According to MusicBrainz Database, the canonical album, relating to the music work rendition, is La Cucaracha."
    source "MusicBrainz Database"
    certainty_type "absolutely_certain"
    title "Wolfram_expression: Entity[&#34;MusicAlbum&#34;, &#34;LaCucaracha::bdf4x&#34;]&#10;comment: According to MusicBrainz Database, the canonical album, relating to the music work rendition, is La Cucaracha.&#10;source: MusicBrainz Database"
  ]
  node [
    id 192
    label "066.19.2.3&#10;Your Party (canonical recording)"
    entity_name "Your Party"
    entity_type "canonical recording"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicWorkRecording&#34;, &#34;YourParty::6xnp5&#34;]"
    comment "According to MusicBrainz Database, the canonical recording, relating to the music work rendition, is also named Your Party."
    source "MusicBrainz Database"
    certainty_type "absolutely_certain"
    title "Wolfram_expression: Entity[&#34;MusicWorkRecording&#34;, &#34;YourParty::6xnp5&#34;]&#10;comment: According to MusicBrainz Database, the canonical recording, relating to the music work rendition, is also named Your Party.&#10;source: MusicBrainz Database"
  ]
  node [
    id 193
    label "067.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 67
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 67"
  ]
  node [
    id 194
    label "068.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 68
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 68"
  ]
  node [
    id 195
    label "069.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 69
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 69"
  ]
  node [
    id 196
    label "069.02.5.0&#10;[/?] john"
    text "[/?] john"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.6854492724560142, 19, 2, 66)]"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']&#10;corels: [(0.6854492724560142, 19, 2, 66)]"
  ]
  node [
    id 197
    label "069.08.1.0&#10;Jamie"
    text "Jamie"
    shape "box"
    color "cyan"
    size 10
    syn_role "conj (head: ROOT)"
    wrt_ROOT "right"
    cats "nc"
    cats "ent"
    title "syn_role: conj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc', 'ent']"
  ]
  node [
    id 198
    label "069.00.2.0&#10;&#38;-um"
    text "&#38;-um"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "ent"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['ent']"
  ]
  node [
    id 199
    label "069.06.1.0&#10;john"
    text "john"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "ent"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['ent']"
  ]
  node [
    id 200
    label "070.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 70
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 70"
  ]
  node [
    id 201
    label "070.01.1.0&#10;john"
    text "john"
    shape "box"
    color "cyan"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.6968035413370409, 14, 2, 66)]"
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['nc']&#10;corels: [(0.6968035413370409, 14, 2, 66)]"
  ]
  node [
    id 202
    label "070.04.2.0&#10;your party"
    text "your party"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.6854492724560142, 2, 5, 69)]"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.6854492724560142, 2, 5, 69)]"
  ]
  node [
    id 203
    label "070.01.2.0&#10;john come"
    text "john come"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "ent"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['ent']"
  ]
  node [
    id 204
    label "070.04.2.1&#10;Your Party (music work rendition)"
    entity_name "Your Party"
    entity_type "music work rendition"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicWorkRendition&#34;, &#34;YourPartyWeen::668n9&#34;]"
    comment "Your Party can be a music work rendition or music work."
    source "MusicBrainz Database"
    certainty_type "quite_certain"
    title "Wolfram_expression: Entity[&#34;MusicWorkRendition&#34;, &#34;YourPartyWeen::668n9&#34;]&#10;comment: Your Party can be a music work rendition or music work.&#10;source: MusicBrainz Database"
  ]
  node [
    id 205
    label "070.04.2.2&#10;La Cucaracha (canonical album)"
    entity_name "La Cucaracha"
    entity_type "canonical album"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicAlbum&#34;, &#34;LaCucaracha::bdf4x&#34;]"
    comment "According to MusicBrainz Database, the canonical album, relating to the music work rendition, is La Cucaracha."
    source "MusicBrainz Database"
    certainty_type "absolutely_certain"
    title "Wolfram_expression: Entity[&#34;MusicAlbum&#34;, &#34;LaCucaracha::bdf4x&#34;]&#10;comment: According to MusicBrainz Database, the canonical album, relating to the music work rendition, is La Cucaracha.&#10;source: MusicBrainz Database"
  ]
  node [
    id 206
    label "070.04.2.3&#10;Your Party (canonical recording)"
    entity_name "Your Party"
    entity_type "canonical recording"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicWorkRecording&#34;, &#34;YourParty::6xnp5&#34;]"
    comment "According to MusicBrainz Database, the canonical recording, relating to the music work rendition, is also named Your Party."
    source "MusicBrainz Database"
    certainty_type "absolutely_certain"
    title "Wolfram_expression: Entity[&#34;MusicWorkRecording&#34;, &#34;YourParty::6xnp5&#34;]&#10;comment: According to MusicBrainz Database, the canonical recording, relating to the music work rendition, is also named Your Party.&#10;source: MusicBrainz Database"
  ]
  node [
    id 207
    label "071.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 71
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 71"
  ]
  node [
    id 208
    label "072.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 72
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 72"
  ]
  node [
    id 209
    label "073.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 73
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 73"
  ]
  node [
    id 210
    label "073.15.1.0&#10;school"
    text "school"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7286955108086063, 4, 2, 70)]"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.7286955108086063, 4, 2, 70)]"
  ]
  node [
    id 211
    label "074.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 74
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 74"
  ]
  node [
    id 212
    label "075.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 75
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 75"
  ]
  node [
    id 213
    label "075.04.2.0&#10;another place"
    text "another place"
    shape "box"
    color "cyan"
    size 10
    syn_role "dobj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7067322081305525, 15, 1, 73)]"
    title "syn_role: dobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.7067322081305525, 15, 1, 73)]"
  ]
  node [
    id 214
    label "075.04.2.1&#10;Another Place (historical site)"
    entity_name "Another Place"
    entity_type "historical site"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;HistoricalSite&#34;, &#34;AnotherPlace::w52zd&#34;]"
    comment "Another Place can be a historical site or artwork."
    source "Freebase"
    certainty_type "quite_certain"
    title "Wolfram_expression: Entity[&#34;HistoricalSite&#34;, &#34;AnotherPlace::w52zd&#34;]&#10;comment: Another Place can be a historical site or artwork.&#10;source: Freebase"
  ]
  node [
    id 215
    label "076.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 76
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 76"
  ]
  node [
    id 216
    label "077.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 77
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 77"
  ]
  node [
    id 217
    label "077.03.1.0&#10;sad"
    text "sad"
    shape "box"
    color "pink"
    size 15
    syn_role "acomp (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "exp"
    polarity -0.5
    title "syn_role: acomp (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['exp']&#10;polarity: -0.5"
  ]
  node [
    id 218
    label "078.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 78
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 78"
  ]
  node [
    id 219
    label "078.08.1.0&#10;home"
    text "home"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7937302592388699, 4, 2, 75)]"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.7937302592388699, 4, 2, 75)]"
  ]
  node [
    id 220
    label "078.08.1.1&#10;A Home (music work rendition)"
    entity_name "A Home"
    entity_type "music work rendition"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicWorkRendition&#34;, &#34;AHomeDixieChicks::grs46&#34;]"
    comment "A Home can be a music work rendition, music work, book, movie, financial or word."
    source "MusicBrainz Database"
    certainty_type "quite_certain"
    title "Wolfram_expression: Entity[&#34;MusicWorkRendition&#34;, &#34;AHomeDixieChicks::grs46&#34;]&#10;comment: A Home can be a music work rendition, music work, book, movie, financial or word.&#10;source: MusicBrainz Database"
  ]
  node [
    id 221
    label "078.08.1.2&#10;Home (canonical album)"
    entity_name "Home"
    entity_type "canonical album"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicAlbum&#34;, &#34;Home::n6jqf&#34;]"
    comment "According to MusicBrainz Database, the canonical album, relating to the music work rendition, is Home."
    source "MusicBrainz Database"
    certainty_type "absolutely_certain"
    title "Wolfram_expression: Entity[&#34;MusicAlbum&#34;, &#34;Home::n6jqf&#34;]&#10;comment: According to MusicBrainz Database, the canonical album, relating to the music work rendition, is Home.&#10;source: MusicBrainz Database"
  ]
  node [
    id 222
    label "078.08.1.3&#10;A Home (canonical recording)"
    entity_name "A Home"
    entity_type "canonical recording"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicWorkRecording&#34;, &#34;AHome::2826d&#34;]"
    comment "According to MusicBrainz Database, the canonical recording, relating to the music work rendition, is also named A Home."
    source "MusicBrainz Database"
    certainty_type "absolutely_certain"
    title "Wolfram_expression: Entity[&#34;MusicWorkRecording&#34;, &#34;AHome::2826d&#34;]&#10;comment: According to MusicBrainz Database, the canonical recording, relating to the music work rendition, is also named A Home.&#10;source: MusicBrainz Database"
  ]
  node [
    id 223
    label "079.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 79
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 79"
  ]
  node [
    id 224
    label "080.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 80
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 80"
  ]
  node [
    id 225
    label "080.04.2.0&#10;extra work"
    text "extra work"
    shape "box"
    color "cyan"
    size 10
    syn_role "dobj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.8328143959866809, 8, 1, 78)]"
    title "syn_role: dobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.8328143959866809, 8, 1, 78)]"
  ]
  node [
    id 226
    label "080.07.1.0&#10;home"
    text "home"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: right&#10;cats: ['nc']"
  ]
  node [
    id 227
    label "080.04.1.0&#10;extra"
    text "extra"
    shape "box"
    color "pink"
    size 10
    syn_role "amod (head: dobj)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.0
    title "syn_role: amod (head: dobj)&#10;wrt_ROOT: right&#10;cats: ['exp']&#10;polarity: 0.0"
  ]
  node [
    id 228
    label "080.07.1.1&#10;A Home (music work rendition)"
    entity_name "A Home"
    entity_type "music work rendition"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicWorkRendition&#34;, &#34;AHomeDixieChicks::grs46&#34;]"
    comment "A Home can be a music work rendition, music work, book, movie, financial or word."
    source "MusicBrainz Database"
    certainty_type "quite_certain"
    title "Wolfram_expression: Entity[&#34;MusicWorkRendition&#34;, &#34;AHomeDixieChicks::grs46&#34;]&#10;comment: A Home can be a music work rendition, music work, book, movie, financial or word.&#10;source: MusicBrainz Database"
  ]
  node [
    id 229
    label "080.07.1.2&#10;Home (canonical album)"
    entity_name "Home"
    entity_type "canonical album"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicAlbum&#34;, &#34;Home::n6jqf&#34;]"
    comment "According to MusicBrainz Database, the canonical album, relating to the music work rendition, is Home."
    source "MusicBrainz Database"
    certainty_type "absolutely_certain"
    title "Wolfram_expression: Entity[&#34;MusicAlbum&#34;, &#34;Home::n6jqf&#34;]&#10;comment: According to MusicBrainz Database, the canonical album, relating to the music work rendition, is Home.&#10;source: MusicBrainz Database"
  ]
  node [
    id 230
    label "080.07.1.3&#10;A Home (canonical recording)"
    entity_name "A Home"
    entity_type "canonical recording"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicWorkRecording&#34;, &#34;AHome::2826d&#34;]"
    comment "According to MusicBrainz Database, the canonical recording, relating to the music work rendition, is also named A Home."
    source "MusicBrainz Database"
    certainty_type "absolutely_certain"
    title "Wolfram_expression: Entity[&#34;MusicWorkRecording&#34;, &#34;AHome::2826d&#34;]&#10;comment: According to MusicBrainz Database, the canonical recording, relating to the music work rendition, is also named A Home.&#10;source: MusicBrainz Database"
  ]
  node [
    id 231
    label "081.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 81
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 81"
  ]
  node [
    id 232
    label "081.06.1.0&#10;good"
    text "good"
    shape "box"
    color "pink"
    size 17
    syn_role "dobj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.7
    title "syn_role: dobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['exp']&#10;polarity: 0.7"
  ]
  node [
    id 233
    label "082.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 82
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 82"
  ]
  node [
    id 234
    label "082.04.1.0&#10;live"
    text "live"
    shape "box"
    color "pink"
    size 11
    syn_role "advmod (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.13636363636363635
    title "syn_role: advmod (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['exp']&#10;polarity: 0.13636363636363635"
  ]
  node [
    id 235
    label "082.05.1.0&#10;far"
    text "far"
    shape "box"
    color "pink"
    size 11
    syn_role "advmod (head: advmod)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.1
    title "syn_role: advmod (head: advmod)&#10;wrt_ROOT: right&#10;cats: ['exp']&#10;polarity: 0.1"
  ]
  node [
    id 236
    label "082.00.1.0&#10;I"
    text "I"
    shape "box"
    color "yellow"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "."
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['sfs']&#10;clause_type: ."
  ]
  node [
    id 237
    label "083.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 83
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 83"
  ]
  node [
    id 238
    label "083.04.1.0&#10;live"
    text "live"
    shape "box"
    color "pink"
    size 11
    syn_role "advmod (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.13636363636363635
    title "syn_role: advmod (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['exp']&#10;polarity: 0.13636363636363635"
  ]
  node [
    id 239
    label "083.05.1.0&#10;far"
    text "far"
    shape "box"
    color "pink"
    size 11
    syn_role "advmod (head: advmod)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.1
    title "syn_role: advmod (head: advmod)&#10;wrt_ROOT: right&#10;cats: ['exp']&#10;polarity: 0.1"
  ]
  node [
    id 240
    label "084.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 84
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 84"
  ]
  node [
    id 241
    label "084.05.1.0&#10;live"
    text "live"
    shape "box"
    color "pink"
    size 11
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.13636363636363635
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['exp']&#10;polarity: 0.13636363636363635"
  ]
  node [
    id 242
    label "084.06.1.0&#10;far"
    text "far"
    shape "box"
    color "pink"
    size 11
    syn_role "advmod (head: advmod)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.1
    title "syn_role: advmod (head: advmod)&#10;wrt_ROOT: right&#10;cats: ['exp']&#10;polarity: 0.1"
  ]
  node [
    id 243
    label "084.03.1.0&#10;you"
    text "you"
    shape "box"
    color "yellow"
    size 10
    syn_role "nsubj (head: nsubj)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "?"
    title "syn_role: nsubj (head: nsubj)&#10;wrt_ROOT: left&#10;cats: ['sfs']&#10;clause_type: ?"
  ]
  node [
    id 244
    label "085.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 85
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 85"
  ]
  node [
    id 245
    label "085.01.1.0&#10;I"
    text "I"
    shape "box"
    color "yellow"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "."
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['sfs']&#10;clause_type: ."
  ]
  node [
    id 246
    label "086.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 86
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 86"
  ]
  node [
    id 247
    label "087.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 87
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 87"
  ]
  node [
    id 248
    label "087.01.1.0&#10;Charlene"
    text "Charlene"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: ROOT)"
    wrt_ROOT "right"
    cats "nc"
    cats "ent"
    title "syn_role: pobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc', 'ent']"
  ]
  node [
    id 249
    label "088.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 88
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 88"
  ]
  node [
    id 250
    label "088.02.1.0&#10;right"
    text "right"
    shape "box"
    color "pink"
    size 13
    syn_role "acomp (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.2857142857142857
    title "syn_role: acomp (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['exp']&#10;polarity: 0.2857142857142857"
  ]
  node [
    id 251
    label "089.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 89
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 89"
  ]
  node [
    id 252
    label "089.11.3.0&#10;your extra work"
    text "your extra work"
    shape "box"
    color "cyan"
    size 10
    syn_role "dobj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(1.0000000039903203, 4, 2, 80)]"
    title "syn_role: dobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(1.0000000039903203, 4, 2, 80)]"
  ]
  node [
    id 253
    label "089.12.1.0&#10;extra"
    text "extra"
    shape "box"
    color "pink"
    size 10
    syn_role "amod (head: dobj)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.0
    title "syn_role: amod (head: dobj)&#10;wrt_ROOT: right&#10;cats: ['exp']&#10;polarity: 0.0"
  ]
  node [
    id 254
    label "090.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 90
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 90"
  ]
  node [
    id 255
    label "090.07.3.0&#10;my extra work"
    text "my extra work"
    shape "box"
    color "cyan"
    size 10
    syn_role "dobj (head: advcl)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(1.0000000039903203, 11, 3, 89)]"
    title "syn_role: dobj (head: advcl)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(1.0000000039903203, 11, 3, 89)]"
  ]
  node [
    id 256
    label "090.08.1.0&#10;extra"
    text "extra"
    shape "box"
    color "pink"
    size 10
    syn_role "amod (head: dobj)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.0
    title "syn_role: amod (head: dobj)&#10;wrt_ROOT: right&#10;cats: ['exp']&#10;polarity: 0.0"
  ]
  node [
    id 257
    label "091.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 91
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 91"
  ]
  node [
    id 258
    label "091.00.1.0&#10;excuse"
    text "excuse"
    shape "box"
    color "pink"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "exp"
    polarity -0.05
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['exp']&#10;polarity: -0.05"
  ]
  node [
    id 259
    label "092.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 92
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 92"
  ]
  node [
    id 260
    label "092.02.2.0&#10;your work"
    text "your work"
    shape "box"
    color "cyan"
    size 10
    syn_role "dobj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(1.0000000039903203, 7, 3, 90)]"
    title "syn_role: dobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(1.0000000039903203, 7, 3, 90)]"
  ]
  node [
    id 261
    label "092.05.1.0&#10;home"
    text "home"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(1.0000000408776748, 7, 1, 80)]"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(1.0000000408776748, 7, 1, 80)]"
  ]
  node [
    id 262
    label "092.05.1.1&#10;A Home (music work rendition)"
    entity_name "A Home"
    entity_type "music work rendition"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicWorkRendition&#34;, &#34;AHomeDixieChicks::grs46&#34;]"
    comment "A Home can be a music work rendition, music work, book, movie, financial or word."
    source "MusicBrainz Database"
    certainty_type "quite_certain"
    title "Wolfram_expression: Entity[&#34;MusicWorkRendition&#34;, &#34;AHomeDixieChicks::grs46&#34;]&#10;comment: A Home can be a music work rendition, music work, book, movie, financial or word.&#10;source: MusicBrainz Database"
  ]
  node [
    id 263
    label "092.05.1.2&#10;Home (canonical album)"
    entity_name "Home"
    entity_type "canonical album"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicAlbum&#34;, &#34;Home::n6jqf&#34;]"
    comment "According to MusicBrainz Database, the canonical album, relating to the music work rendition, is Home."
    source "MusicBrainz Database"
    certainty_type "absolutely_certain"
    title "Wolfram_expression: Entity[&#34;MusicAlbum&#34;, &#34;Home::n6jqf&#34;]&#10;comment: According to MusicBrainz Database, the canonical album, relating to the music work rendition, is Home.&#10;source: MusicBrainz Database"
  ]
  node [
    id 264
    label "092.05.1.3&#10;A Home (canonical recording)"
    entity_name "A Home"
    entity_type "canonical recording"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicWorkRecording&#34;, &#34;AHome::2826d&#34;]"
    comment "According to MusicBrainz Database, the canonical recording, relating to the music work rendition, is also named A Home."
    source "MusicBrainz Database"
    certainty_type "absolutely_certain"
    title "Wolfram_expression: Entity[&#34;MusicWorkRecording&#34;, &#34;AHome::2826d&#34;]&#10;comment: According to MusicBrainz Database, the canonical recording, relating to the music work rendition, is also named A Home.&#10;source: MusicBrainz Database"
  ]
  node [
    id 265
    label "093.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 93
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 93"
  ]
  node [
    id 266
    label "094.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 94
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 94"
  ]
  node [
    id 267
    label "094.06.2.0&#10;but john"
    text "but john"
    shape "box"
    color "cyan"
    size 10
    syn_role "conj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(1.0000000065261785, 1, 1, 70)]"
    title "syn_role: conj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(1.0000000065261785, 1, 1, 70)]"
  ]
  node [
    id 268
    label "094.14.1.0&#10;john"
    text "john"
    shape "box"
    color "cyan"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "nc"
    cats "ent"
    corels "[(1.0000000065261785, 6, 2, 94)]"
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['nc', 'ent']&#10;corels: [(1.0000000065261785, 6, 2, 94)]"
  ]
  node [
    id 269
    label "094.18.3.0&#10;his extra work"
    text "his extra work"
    shape "box"
    color "cyan"
    size 10
    syn_role "dobj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(1.0000000039903203, 2, 2, 92)]"
    title "syn_role: dobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(1.0000000039903203, 2, 2, 92)]"
  ]
  node [
    id 270
    label "094.22.1.0&#10;home"
    text "home"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(1.0000000408776748, 5, 1, 92)]"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(1.0000000408776748, 5, 1, 92)]"
  ]
  node [
    id 271
    label "094.07.3.0&#10;john> ["
    text "john> ["
    shape "box"
    color "cyan"
    size 10
    syn_role "conj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "ent"
    title "syn_role: conj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['ent']"
  ]
  node [
    id 272
    label "094.19.1.0&#10;extra"
    text "extra"
    shape "box"
    color "pink"
    size 10
    syn_role "amod (head: dobj)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.0
    title "syn_role: amod (head: dobj)&#10;wrt_ROOT: right&#10;cats: ['exp']&#10;polarity: 0.0"
  ]
  node [
    id 273
    label "094.22.1.1&#10;A Home (music work rendition)"
    entity_name "A Home"
    entity_type "music work rendition"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicWorkRendition&#34;, &#34;AHomeDixieChicks::grs46&#34;]"
    comment "A Home can be a music work rendition, music work, book, movie, financial or word."
    source "MusicBrainz Database"
    certainty_type "quite_certain"
    title "Wolfram_expression: Entity[&#34;MusicWorkRendition&#34;, &#34;AHomeDixieChicks::grs46&#34;]&#10;comment: A Home can be a music work rendition, music work, book, movie, financial or word.&#10;source: MusicBrainz Database"
  ]
  node [
    id 274
    label "094.22.1.2&#10;Home (canonical album)"
    entity_name "Home"
    entity_type "canonical album"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicAlbum&#34;, &#34;Home::n6jqf&#34;]"
    comment "According to MusicBrainz Database, the canonical album, relating to the music work rendition, is Home."
    source "MusicBrainz Database"
    certainty_type "absolutely_certain"
    title "Wolfram_expression: Entity[&#34;MusicAlbum&#34;, &#34;Home::n6jqf&#34;]&#10;comment: According to MusicBrainz Database, the canonical album, relating to the music work rendition, is Home.&#10;source: MusicBrainz Database"
  ]
  node [
    id 275
    label "094.22.1.3&#10;A Home (canonical recording)"
    entity_name "A Home"
    entity_type "canonical recording"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicWorkRecording&#34;, &#34;AHome::2826d&#34;]"
    comment "According to MusicBrainz Database, the canonical recording, relating to the music work rendition, is also named A Home."
    source "MusicBrainz Database"
    certainty_type "absolutely_certain"
    title "Wolfram_expression: Entity[&#34;MusicWorkRecording&#34;, &#34;AHome::2826d&#34;]&#10;comment: According to MusicBrainz Database, the canonical recording, relating to the music work rendition, is also named A Home.&#10;source: MusicBrainz Database"
  ]
  node [
    id 276
    label "095.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 95
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 95"
  ]
  node [
    id 277
    label "095.01.2.0&#10;john hadta"
    text "john hadta"
    shape "box"
    color "cyan"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "nc"
    cats "ent"
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['nc', 'ent']"
  ]
  node [
    id 278
    label "095.06.2.0&#10;a hospital"
    text "a hospital"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7791815191548143, 18, 3, 94)]"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.7791815191548143, 18, 3, 94)]"
  ]
  node [
    id 279
    label "095.10.3.0&#10;his extra work"
    text "his extra work"
    shape "box"
    color "cyan"
    size 10
    syn_role "dobj (head: advcl)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.8328143959866809, 22, 1, 94)]"
    title "syn_role: dobj (head: advcl)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.8328143959866809, 22, 1, 94)]"
  ]
  node [
    id 280
    label "095.04.1.0&#10;live"
    text "live"
    shape "box"
    color "pink"
    size 11
    syn_role "advmod (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.13636363636363635
    title "syn_role: advmod (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['exp']&#10;polarity: 0.13636363636363635"
  ]
  node [
    id 281
    label "095.11.1.0&#10;extra"
    text "extra"
    shape "box"
    color "pink"
    size 10
    syn_role "amod (head: dobj)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.0
    title "syn_role: amod (head: dobj)&#10;wrt_ROOT: right&#10;cats: ['exp']&#10;polarity: 0.0"
  ]
  node [
    id 282
    label "096.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 96
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 96"
  ]
  node [
    id 283
    label "097.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 97
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 97"
  ]
  node [
    id 284
    label "098.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 98
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 98"
  ]
  node [
    id 285
    label "099.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 99
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 99"
  ]
  node [
    id 286
    label "099.08.1.0&#10;Brett"
    text "Brett"
    shape "box"
    color "cyan"
    size 10
    syn_role "npadvmod (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "ent"
    title "syn_role: npadvmod (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['ent']"
  ]
  node [
    id 287
    label "100.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 100
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 100"
  ]
  node [
    id 288
    label "100.00.2.0&#10;extra work"
    text "extra work"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(1.0000000039903203, 10, 3, 95)]"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']&#10;corels: [(1.0000000039903203, 10, 3, 95)]"
  ]
  node [
    id 289
    label "100.04.1.0&#10;home"
    text "home"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7690394412912005, 6, 2, 95)]"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.7690394412912005, 6, 2, 95)]"
  ]
  node [
    id 290
    label "100.00.1.0&#10;extra"
    text "extra"
    shape "box"
    color "pink"
    size 10
    syn_role "amod (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.0
    title "syn_role: amod (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['exp']&#10;polarity: 0.0"
  ]
  node [
    id 291
    label "100.04.1.1&#10;A Home (music work rendition)"
    entity_name "A Home"
    entity_type "music work rendition"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicWorkRendition&#34;, &#34;AHomeDixieChicks::grs46&#34;]"
    comment "A Home can be a music work rendition, music work, book, movie, financial or word."
    source "MusicBrainz Database"
    certainty_type "quite_certain"
    title "Wolfram_expression: Entity[&#34;MusicWorkRendition&#34;, &#34;AHomeDixieChicks::grs46&#34;]&#10;comment: A Home can be a music work rendition, music work, book, movie, financial or word.&#10;source: MusicBrainz Database"
  ]
  node [
    id 292
    label "100.04.1.2&#10;Home (canonical album)"
    entity_name "Home"
    entity_type "canonical album"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicAlbum&#34;, &#34;Home::n6jqf&#34;]"
    comment "According to MusicBrainz Database, the canonical album, relating to the music work rendition, is Home."
    source "MusicBrainz Database"
    certainty_type "absolutely_certain"
    title "Wolfram_expression: Entity[&#34;MusicAlbum&#34;, &#34;Home::n6jqf&#34;]&#10;comment: According to MusicBrainz Database, the canonical album, relating to the music work rendition, is Home.&#10;source: MusicBrainz Database"
  ]
  node [
    id 293
    label "100.04.1.3&#10;A Home (canonical recording)"
    entity_name "A Home"
    entity_type "canonical recording"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicWorkRecording&#34;, &#34;AHome::2826d&#34;]"
    comment "According to MusicBrainz Database, the canonical recording, relating to the music work rendition, is also named A Home."
    source "MusicBrainz Database"
    certainty_type "absolutely_certain"
    title "Wolfram_expression: Entity[&#34;MusicWorkRecording&#34;, &#34;AHome::2826d&#34;]&#10;comment: According to MusicBrainz Database, the canonical recording, relating to the music work rendition, is also named A Home.&#10;source: MusicBrainz Database"
  ]
  node [
    id 294
    label "101.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 101
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 101"
  ]
  node [
    id 295
    label "101.00.1.0&#10;Brett"
    text "Brett"
    shape "box"
    color "cyan"
    size 10
    syn_role "npadvmod (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "ent"
    title "syn_role: npadvmod (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['ent']"
  ]
  node [
    id 296
    label "102.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 102
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 102"
  ]
  node [
    id 297
    label "103.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 103
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 103"
  ]
  node [
    id 298
    label "104.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 104
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 104"
  ]
  node [
    id 299
    label "104.07.2.0&#10;your party"
    text "your party"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.8038481359541441, 0, 2, 100)]"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.8038481359541441, 0, 2, 100)]"
  ]
  node [
    id 300
    label "104.07.2.1&#10;Your Party (music work rendition)"
    entity_name "Your Party"
    entity_type "music work rendition"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicWorkRendition&#34;, &#34;YourPartyWeen::668n9&#34;]"
    comment "Your Party can be a music work rendition or music work."
    source "MusicBrainz Database"
    certainty_type "quite_certain"
    title "Wolfram_expression: Entity[&#34;MusicWorkRendition&#34;, &#34;YourPartyWeen::668n9&#34;]&#10;comment: Your Party can be a music work rendition or music work.&#10;source: MusicBrainz Database"
  ]
  node [
    id 301
    label "104.07.2.2&#10;La Cucaracha (canonical album)"
    entity_name "La Cucaracha"
    entity_type "canonical album"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicAlbum&#34;, &#34;LaCucaracha::bdf4x&#34;]"
    comment "According to MusicBrainz Database, the canonical album, relating to the music work rendition, is La Cucaracha."
    source "MusicBrainz Database"
    certainty_type "absolutely_certain"
    title "Wolfram_expression: Entity[&#34;MusicAlbum&#34;, &#34;LaCucaracha::bdf4x&#34;]&#10;comment: According to MusicBrainz Database, the canonical album, relating to the music work rendition, is La Cucaracha.&#10;source: MusicBrainz Database"
  ]
  node [
    id 302
    label "104.07.2.3&#10;Your Party (canonical recording)"
    entity_name "Your Party"
    entity_type "canonical recording"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicWorkRecording&#34;, &#34;YourParty::6xnp5&#34;]"
    comment "According to MusicBrainz Database, the canonical recording, relating to the music work rendition, is also named Your Party."
    source "MusicBrainz Database"
    certainty_type "absolutely_certain"
    title "Wolfram_expression: Entity[&#34;MusicWorkRecording&#34;, &#34;YourParty::6xnp5&#34;]&#10;comment: According to MusicBrainz Database, the canonical recording, relating to the music work rendition, is also named Your Party.&#10;source: MusicBrainz Database"
  ]
  node [
    id 303
    label "105.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 105
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 105"
  ]
  node [
    id 304
    label "105.00.1.0&#10;john"
    text "john"
    shape "box"
    color "cyan"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "nc"
    cats "ent"
    corels "[(0.7501978936483702, 4, 1, 100)]"
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['nc', 'ent']&#10;corels: [(0.7501978936483702, 4, 1, 100)]"
  ]
  node [
    id 305
    label "105.10.1.0&#10;school"
    text "school"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7286955108086063, 7, 2, 104)]"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.7286955108086063, 7, 2, 104)]"
  ]
  node [
    id 306
    label "106.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 106
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 106"
  ]
  node [
    id 307
    label "106.06.1.0&#10;Jamie"
    text "Jamie"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "nc"
    cats "ent"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc', 'ent']"
  ]
  node [
    id 308
    label "106.00.2.0&#10;&#38;-um"
    text "&#38;-um"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "ent"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['ent']"
  ]
  node [
    id 309
    label "107.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 107
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 107"
  ]
  node [
    id 310
    label "108.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 108
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 108"
  ]
  node [
    id 311
    label "109.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 109
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 109"
  ]
  node [
    id 312
    label "109.04.1.0&#10;john"
    text "john"
    shape "box"
    color "cyan"
    size 10
    syn_role "nsubj (head: ccomp)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(1.0000000065261785, 14, 1, 94)]"
    title "syn_role: nsubj (head: ccomp)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(1.0000000065261785, 14, 1, 94)]"
  ]
  node [
    id 313
    label "109.06.1.0&#10;live"
    text "live"
    shape "box"
    color "pink"
    size 11
    syn_role "ccomp (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.13636363636363635
    title "syn_role: ccomp (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['exp']&#10;polarity: 0.13636363636363635"
  ]
  node [
    id 314
    label "109.00.1.0&#10;I"
    text "I"
    shape "box"
    color "yellow"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "."
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['sfs']&#10;clause_type: ."
  ]
  node [
    id 315
    label "110.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 110
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 110"
  ]
  node [
    id 316
    label "110.01.6.0&#10;k [/?] kevin"
    text "k [/?] kevin"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    title "syn_role: pobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']"
  ]
  node [
    id 317
    label "110.06.4.0&#10;kevin [/?"
    text "kevin [/?"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "ent"
    title "syn_role: pobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['ent']"
  ]
  node [
    id 318
    label "111.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 111
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 111"
  ]
  node [
    id 319
    label "111.00.1.0&#10;kevin"
    text "kevin"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']"
  ]
  node [
    id 320
    label "111.00.4.0&#10;kevin [/?"
    text "kevin [/?"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "ent"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['ent']"
  ]
  node [
    id 321
    label "112.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 112
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 112"
  ]
  node [
    id 322
    label "112.04.1.0&#10;right"
    text "right"
    shape "box"
    color "pink"
    size 13
    syn_role "acomp (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.2857142857142857
    title "syn_role: acomp (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['exp']&#10;polarity: 0.2857142857142857"
  ]
  node [
    id 323
    label "113.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 113
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 113"
  ]
  node [
    id 324
    label "114.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 114
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 114"
  ]
  node [
    id 325
    label "114.11.1.0&#10;you"
    text "you"
    shape "box"
    color "yellow"
    size 10
    syn_role "dative (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "?"
    title "syn_role: dative (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['sfs']&#10;clause_type: ?"
  ]
  node [
    id 326
    label "115.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 115
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 115"
  ]
  node [
    id 327
    label "116.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 116
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 116"
  ]
  node [
    id 328
    label "116.00.1.0&#10;Jamie"
    text "Jamie"
    shape "box"
    color "cyan"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "nc"
    cats "ent"
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['nc', 'ent']"
  ]
  node [
    id 329
    label "116.03.2.0&#10;a boy"
    text "a boy"
    shape "box"
    color "cyan"
    size 10
    syn_role "attr (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7532067960964989, 0, 1, 105)]"
    title "syn_role: attr (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.7532067960964989, 0, 1, 105)]"
  ]
  node [
    id 330
    label "117.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 117
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 117"
  ]
  node [
    id 331
    label "118.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 118
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 118"
  ]
  node [
    id 332
    label "118.00.2.0&#10;a girl"
    text "a girl"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.8338516962585786, 3, 2, 116)]"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']&#10;corels: [(0.8338516962585786, 3, 2, 116)]"
  ]
  node [
    id 333
    label "119.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 119
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 119"
  ]
  node [
    id 334
    label "119.00.1.0&#10;Brett"
    text "Brett"
    shape "box"
    color "cyan"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "nc"
    cats "ent"
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['nc', 'ent']"
  ]
  node [
    id 335
    label "119.02.1.0&#10;Jamie"
    text "Jamie"
    shape "box"
    color "cyan"
    size 10
    syn_role "attr (head: ROOT)"
    wrt_ROOT "right"
    cats "nc"
    cats "ent"
    title "syn_role: attr (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc', 'ent']"
  ]
  node [
    id 336
    label "119.03.2.0&#10;a girl"
    text "a girl"
    shape "box"
    color "cyan"
    size 10
    syn_role "attr (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7227535240283695, 10, 1, 105)]"
    title "syn_role: attr (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.7227535240283695, 10, 1, 105)]"
  ]
  node [
    id 337
    label "120.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 120
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 120"
  ]
  node [
    id 338
    label "121.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 121
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 121"
  ]
  node [
    id 339
    label "122.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 122
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 122"
  ]
  node [
    id 340
    label "122.00.2.0&#10;a boy"
    text "a boy"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.8338516962585786, 0, 2, 118)]"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']&#10;corels: [(0.8338516962585786, 0, 2, 118)]"
  ]
  node [
    id 341
    label "123.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 123
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 123"
  ]
  node [
    id 342
    label "123.04.2.0&#10;a boy"
    text "a boy"
    shape "box"
    color "cyan"
    size 10
    syn_role "attr (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(1.0000000228299875, 0, 2, 122)]"
    title "syn_role: attr (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(1.0000000228299875, 0, 2, 122)]"
  ]
  node [
    id 343
    label "124.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 124
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 124"
  ]
  node [
    id 344
    label "124.01.1.0&#10;you"
    text "you"
    shape "box"
    color "yellow"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "?"
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['sfs']&#10;clause_type: ?"
  ]
  node [
    id 345
    label "125.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 125
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 125"
  ]
  node [
    id 346
    label "126.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 126
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 126"
  ]
  node [
    id 347
    label "126.00.1.0&#10;okay"
    text "okay"
    shape "box"
    color "pink"
    size 15
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.5
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['exp']&#10;polarity: 0.5"
  ]
  node [
    id 348
    label "127.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 127
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 127"
  ]
  node [
    id 349
    label "127.00.1.0&#10;Brett"
    text "Brett"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "nc"
    cats "ent"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc', 'ent']"
  ]
  node [
    id 350
    label "128.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 128
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 128"
  ]
  node [
    id 351
    label "129.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 129
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 129"
  ]
  node [
    id 352
    label "129.00.1.0&#10;Brett"
    text "Brett"
    shape "box"
    color "cyan"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "nc"
    cats "ent"
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['nc', 'ent']"
  ]
  node [
    id 353
    label "129.04.1.0&#10;things"
    text "things"
    shape "box"
    color "cyan"
    size 10
    syn_role "dobj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.745925671392336, 3, 2, 119)]"
    title "syn_role: dobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.745925671392336, 3, 2, 119)]"
  ]
  node [
    id 354
    label "129.06.1.0&#10;you"
    text "you"
    shape "box"
    color "yellow"
    size 10
    syn_role "pobj (head: dative)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "?"
    title "syn_role: pobj (head: dative)&#10;wrt_ROOT: right&#10;cats: ['sfs']&#10;clause_type: ?"
  ]
  node [
    id 355
    label "130.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 130
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 130"
  ]
  node [
    id 356
    label "131.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 131
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 131"
  ]
  node [
    id 357
    label "131.04.1.0&#10;you"
    text "you"
    shape "box"
    color "yellow"
    size 10
    syn_role "dobj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "?"
    title "syn_role: dobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['sfs']&#10;clause_type: ?"
  ]
  node [
    id 358
    label "132.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 132
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 132"
  ]
  node [
    id 359
    label "132.02.5.0&#10;[/?] food"
    text "[/?] food"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7462066792039541, 4, 1, 129)]"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']&#10;corels: [(0.7462066792039541, 4, 1, 129)]"
  ]
  node [
    id 360
    label "132.00.2.0&#10;&#38;-um"
    text "&#38;-um"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "ent"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['ent']"
  ]
  node [
    id 361
    label "133.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 133
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 133"
  ]
  node [
    id 362
    label "133.00.2.0&#10;a cake"
    text "a cake"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.6644786263819696, 4, 2, 123)]"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']&#10;corels: [(0.6644786263819696, 4, 2, 123)]"
  ]
  node [
    id 363
    label "134.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 134
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 134"
  ]
  node [
    id 364
    label "134.02.2.0&#10;the people"
    text "the people"
    shape "box"
    color "cyan"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7120459235817346, 0, 2, 133)]"
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['nc']&#10;corels: [(0.7120459235817346, 0, 2, 133)]"
  ]
  node [
    id 365
    label "134.06.2.0&#10;your party"
    text "your party"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7881016684269865, 2, 5, 132)]"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.7881016684269865, 2, 5, 132)]"
  ]
  node [
    id 366
    label "134.11.1.0&#10;you"
    text "you"
    shape "box"
    color "yellow"
    size 10
    syn_role "pobj (head: dative)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "?"
    title "syn_role: pobj (head: dative)&#10;wrt_ROOT: right&#10;cats: ['sfs']&#10;clause_type: ?"
  ]
  node [
    id 367
    label "134.06.2.1&#10;Your Party (music work rendition)"
    entity_name "Your Party"
    entity_type "music work rendition"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicWorkRendition&#34;, &#34;YourPartyWeen::668n9&#34;]"
    comment "Your Party can be a music work rendition or music work."
    source "MusicBrainz Database"
    certainty_type "quite_certain"
    title "Wolfram_expression: Entity[&#34;MusicWorkRendition&#34;, &#34;YourPartyWeen::668n9&#34;]&#10;comment: Your Party can be a music work rendition or music work.&#10;source: MusicBrainz Database"
  ]
  node [
    id 368
    label "134.06.2.2&#10;La Cucaracha (canonical album)"
    entity_name "La Cucaracha"
    entity_type "canonical album"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicAlbum&#34;, &#34;LaCucaracha::bdf4x&#34;]"
    comment "According to MusicBrainz Database, the canonical album, relating to the music work rendition, is La Cucaracha."
    source "MusicBrainz Database"
    certainty_type "absolutely_certain"
    title "Wolfram_expression: Entity[&#34;MusicAlbum&#34;, &#34;LaCucaracha::bdf4x&#34;]&#10;comment: According to MusicBrainz Database, the canonical album, relating to the music work rendition, is La Cucaracha.&#10;source: MusicBrainz Database"
  ]
  node [
    id 369
    label "134.06.2.3&#10;Your Party (canonical recording)"
    entity_name "Your Party"
    entity_type "canonical recording"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicWorkRecording&#34;, &#34;YourParty::6xnp5&#34;]"
    comment "According to MusicBrainz Database, the canonical recording, relating to the music work rendition, is also named Your Party."
    source "MusicBrainz Database"
    certainty_type "absolutely_certain"
    title "Wolfram_expression: Entity[&#34;MusicWorkRecording&#34;, &#34;YourParty::6xnp5&#34;]&#10;comment: According to MusicBrainz Database, the canonical recording, relating to the music work rendition, is also named Your Party.&#10;source: MusicBrainz Database"
  ]
  node [
    id 370
    label "135.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 135
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 135"
  ]
  node [
    id 371
    label "135.00.2.0&#10;a pinwheel"
    text "a pinwheel"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.6985503623814711, 6, 2, 134)]"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']&#10;corels: [(0.6985503623814711, 6, 2, 134)]"
  ]
  node [
    id 372
    label "135.00.0.0&#10;"
    text ""
    shape "box"
    color "yellow"
    size 10
    syn_role "n/a"
    wrt_ROOT "n/a"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "!"
    title "syn_role: n/a&#10;wrt_ROOT: n/a&#10;cats: ['sfs']&#10;clause_type: !"
  ]
  node [
    id 373
    label "136.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 136
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 136"
  ]
  node [
    id 374
    label "136.01.1.0&#10;careful"
    text "careful"
    shape "box"
    color "pink"
    size 11
    syn_role "acomp (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "exp"
    polarity -0.1
    title "syn_role: acomp (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['exp']&#10;polarity: -0.1"
  ]
  node [
    id 375
    label "137.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 137
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 137"
  ]
  node [
    id 376
    label "137.00.1.0&#10;I"
    text "I"
    shape "box"
    color "yellow"
    size 10
    syn_role "nsubj (head: compound)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "."
    title "syn_role: nsubj (head: compound)&#10;wrt_ROOT: left&#10;cats: ['sfs']&#10;clause_type: ."
  ]
  node [
    id 377
    label "138.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 138
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 138"
  ]
  node [
    id 378
    label "138.00.3.0&#10;okay , Brett"
    text "okay , Brett"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']"
  ]
  node [
    id 379
    label "138.02.1.0&#10;Brett"
    text "Brett"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "ent"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['ent']"
  ]
  node [
    id 380
    label "138.00.1.0&#10;okay"
    text "okay"
    shape "box"
    color "pink"
    size 15
    syn_role "intj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.5
    title "syn_role: intj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['exp']&#10;polarity: 0.5"
  ]
  node [
    id 381
    label "139.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 139
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 139"
  ]
  node [
    id 382
    label "140.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 140
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 140"
  ]
  node [
    id 383
    label "140.00.1.0&#10;Brett"
    text "Brett"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "nc"
    cats "ent"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc', 'ent']"
  ]
  node [
    id 384
    label "141.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 141
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 141"
  ]
  node [
    id 385
    label "141.00.2.0&#10;what mom"
    text "what mom"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7181777270575451, 2, 2, 134)]"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']&#10;corels: [(0.7181777270575451, 2, 2, 134)]"
  ]
  node [
    id 386
    label "142.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 142
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 142"
  ]
  node [
    id 387
    label "142.04.2.0&#10;the pinwheel"
    text "the pinwheel"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.79462309361351, 0, 2, 135)]"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.79462309361351, 0, 2, 135)]"
  ]
  node [
    id 388
    label "142.06.1.0&#10;enough"
    text "enough"
    shape "box"
    color "pink"
    size 10
    syn_role "amod (head: npadvmod)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.0
    title "syn_role: amod (head: npadvmod)&#10;wrt_ROOT: right&#10;cats: ['exp']&#10;polarity: 0.0"
  ]
  node [
    id 389
    label "143.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 143
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 143"
  ]
  node [
    id 390
    label "143.04.2.0&#10;the pinwheel"
    text "the pinwheel"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(1.0000000108908578, 4, 2, 142)]"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(1.0000000108908578, 4, 2, 142)]"
  ]
  node [
    id 391
    label "143.01.1.0&#10;more"
    text "more"
    shape "box"
    color "pink"
    size 15
    syn_role "advmod (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.5
    title "syn_role: advmod (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['exp']&#10;polarity: 0.5"
  ]
  node [
    id 392
    label "144.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 144
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 144"
  ]
  node [
    id 393
    label "145.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 145
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 145"
  ]
  node [
    id 394
    label "145.02.2.0&#10;the kid"
    text "the kid"
    shape "box"
    color "cyan"
    size 10
    syn_role "dobj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.6878199499499585, 4, 2, 143)]"
    title "syn_role: dobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.6878199499499585, 4, 2, 143)]"
  ]
  node [
    id 395
    label "146.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 146
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 146"
  ]
  node [
    id 396
    label "147.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 147
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 147"
  ]
  node [
    id 397
    label "147.00.1.0&#10;Brett"
    text "Brett"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "nc"
    cats "ent"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc', 'ent']"
  ]
  node [
    id 398
    label "148.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 148
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 148"
  ]
  node [
    id 399
    label "148.03.2.0&#10;that game"
    text "that game"
    shape "box"
    color "cyan"
    size 10
    syn_role "dobj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.6937727092056006, 2, 2, 145)]"
    title "syn_role: dobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.6937727092056006, 2, 2, 145)]"
  ]
  node [
    id 400
    label "148.04.1.0&#10;game"
    text "game"
    shape "box"
    color "pink"
    size 14
    syn_role "dobj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "exp"
    polarity -0.4
    title "syn_role: dobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['exp']&#10;polarity: -0.4"
  ]
  node [
    id 401
    label "149.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 149
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 149"
  ]
  node [
    id 402
    label "149.00.1.0&#10;Brett"
    text "Brett"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "nc"
    cats "ent"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc', 'ent']"
  ]
  node [
    id 403
    label "150.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 150
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 150"
  ]
  node [
    id 404
    label "150.02.2.0&#10;the people"
    text "the people"
    shape "box"
    color "cyan"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7181777270575451, 0, 2, 141)]"
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['nc']&#10;corels: [(0.7181777270575451, 0, 2, 141)]"
  ]
  node [
    id 405
    label "150.06.1.0&#10;you"
    text "you"
    shape "box"
    color "yellow"
    size 10
    syn_role "pobj (head: dative)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "?"
    title "syn_role: pobj (head: dative)&#10;wrt_ROOT: right&#10;cats: ['sfs']&#10;clause_type: ?"
  ]
  node [
    id 406
    label "151.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 151
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 151"
  ]
  node [
    id 407
    label "151.06.3.0&#10;a hockey thing"
    text "a hockey thing"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.75726330990877, 0, 1, 127)]"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']&#10;corels: [(0.75726330990877, 0, 1, 127)]"
  ]
  node [
    id 408
    label "151.00.2.0&#10;&#38;-um"
    text "&#38;-um"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "ent"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['ent']"
  ]
  node [
    id 409
    label "152.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 152
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 152"
  ]
  node [
    id 410
    label "153.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 153
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 153"
  ]
  node [
    id 411
    label "153.02.2.0&#10;the people"
    text "the people"
    shape "box"
    color "cyan"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.9999999503230271, 2, 2, 150)]"
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['nc']&#10;corels: [(0.9999999503230271, 2, 2, 150)]"
  ]
  node [
    id 412
    label "154.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 154
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 154"
  ]
  node [
    id 413
    label "155.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 155
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 155"
  ]
  node [
    id 414
    label "155.00.3.0&#10;all the people"
    text "all the people"
    shape "box"
    color "cyan"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.9999999503230271, 2, 2, 153)]"
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['nc']&#10;corels: [(0.9999999503230271, 2, 2, 153)]"
  ]
  node [
    id 415
    label "155.06.2.0&#10;your party"
    text "your party"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7249968713714251, 3, 2, 148)]"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: left&#10;cats: ['nc']&#10;corels: [(0.7249968713714251, 3, 2, 148)]"
  ]
  node [
    id 416
    label "155.09.1.0&#10;things"
    text "things"
    shape "box"
    color "cyan"
    size 10
    syn_role "dobj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.6343954541710531, 6, 3, 151)]"
    title "syn_role: dobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.6343954541710531, 6, 3, 151)]"
  ]
  node [
    id 417
    label "155.14.2.0&#10;pretty paper"
    text "pretty paper"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.6144812652109073, 4, 1, 109)]"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.6144812652109073, 4, 1, 109)]"
  ]
  node [
    id 418
    label "155.14.1.0&#10;pretty"
    text "pretty"
    shape "box"
    color "pink"
    size 12
    syn_role "amod (head: pobj)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.25
    title "syn_role: amod (head: pobj)&#10;wrt_ROOT: right&#10;cats: ['exp']&#10;polarity: 0.25"
  ]
  node [
    id 419
    label "155.06.2.1&#10;Your Party (music work rendition)"
    entity_name "Your Party"
    entity_type "music work rendition"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicWorkRendition&#34;, &#34;YourPartyWeen::668n9&#34;]"
    comment "Your Party can be a music work rendition or music work."
    source "MusicBrainz Database"
    certainty_type "quite_certain"
    title "Wolfram_expression: Entity[&#34;MusicWorkRendition&#34;, &#34;YourPartyWeen::668n9&#34;]&#10;comment: Your Party can be a music work rendition or music work.&#10;source: MusicBrainz Database"
  ]
  node [
    id 420
    label "155.06.2.2&#10;La Cucaracha (canonical album)"
    entity_name "La Cucaracha"
    entity_type "canonical album"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicAlbum&#34;, &#34;LaCucaracha::bdf4x&#34;]"
    comment "According to MusicBrainz Database, the canonical album, relating to the music work rendition, is La Cucaracha."
    source "MusicBrainz Database"
    certainty_type "absolutely_certain"
    title "Wolfram_expression: Entity[&#34;MusicAlbum&#34;, &#34;LaCucaracha::bdf4x&#34;]&#10;comment: According to MusicBrainz Database, the canonical album, relating to the music work rendition, is La Cucaracha.&#10;source: MusicBrainz Database"
  ]
  node [
    id 421
    label "155.06.2.3&#10;Your Party (canonical recording)"
    entity_name "Your Party"
    entity_type "canonical recording"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicWorkRecording&#34;, &#34;YourParty::6xnp5&#34;]"
    comment "According to MusicBrainz Database, the canonical recording, relating to the music work rendition, is also named Your Party."
    source "MusicBrainz Database"
    certainty_type "absolutely_certain"
    title "Wolfram_expression: Entity[&#34;MusicWorkRecording&#34;, &#34;YourParty::6xnp5&#34;]&#10;comment: According to MusicBrainz Database, the canonical recording, relating to the music work rendition, is also named Your Party.&#10;source: MusicBrainz Database"
  ]
  node [
    id 422
    label "155.14.2.1&#10;Pretty Paper (music work rendition)"
    entity_name "Pretty Paper"
    entity_type "music work rendition"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicWorkRendition&#34;, &#34;PrettyPaperCarlySimon::n5q97&#34;]"
    comment "Pretty Paper can be a music album, music work rendition or music work."
    source "MusicBrainz Database"
    certainty_type "quite_certain"
    title "Wolfram_expression: Entity[&#34;MusicWorkRendition&#34;, &#34;PrettyPaperCarlySimon::n5q97&#34;]&#10;comment: Pretty Paper can be a music album, music work rendition or music work.&#10;source: MusicBrainz Database"
  ]
  node [
    id 423
    label "155.14.2.2&#10;Christmas Is Almost Here (canonical album)"
    entity_name "Christmas Is Almost Here"
    entity_type "canonical album"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicAlbum&#34;, &#34;ChristmasIsAlmostHere::fz2z9&#34;]"
    comment "According to MusicBrainz Database, the canonical album, relating to the music work rendition, is Christmas Is Almost Here."
    source "MusicBrainz Database"
    certainty_type "absolutely_certain"
    title "Wolfram_expression: Entity[&#34;MusicAlbum&#34;, &#34;ChristmasIsAlmostHere::fz2z9&#34;]&#10;comment: According to MusicBrainz Database, the canonical album, relating to the music work rendition, is Christmas Is Almost Here.&#10;source: MusicBrainz Database"
  ]
  node [
    id 424
    label "155.14.2.3&#10;Pretty Paper (canonical recording)"
    entity_name "Pretty Paper"
    entity_type "canonical recording"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicWorkRecording&#34;, &#34;PrettyPaper::89w77&#34;]"
    comment "According to MusicBrainz Database, the canonical recording, relating to the music work rendition, is also named Pretty Paper."
    source "MusicBrainz Database"
    certainty_type "absolutely_certain"
    title "Wolfram_expression: Entity[&#34;MusicWorkRecording&#34;, &#34;PrettyPaper::89w77&#34;]&#10;comment: According to MusicBrainz Database, the canonical recording, relating to the music work rendition, is also named Pretty Paper.&#10;source: MusicBrainz Database"
  ]
  node [
    id 425
    label "156.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 156
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 156"
  ]
  node [
    id 426
    label "156.04.2.0&#10;those things"
    text "those things"
    shape "box"
    color "cyan"
    size 10
    syn_role "dobj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7899236587012705, 9, 1, 155)]"
    title "syn_role: dobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.7899236587012705, 9, 1, 155)]"
  ]
  node [
    id 427
    label "157.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 157
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 157"
  ]
  node [
    id 428
    label "157.00.3.0&#10;a rock tumbler"
    text "a rock tumbler"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7301424255687371, 6, 2, 155)]"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']&#10;corels: [(0.7301424255687371, 6, 2, 155)]"
  ]
  node [
    id 429
    label "158.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 158
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 158"
  ]
  node [
    id 430
    label "158.00.1.0&#10;presents"
    text "presents"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7364813133955704, 4, 2, 156)]"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']&#10;corels: [(0.7364813133955704, 4, 2, 156)]"
  ]
  node [
    id 431
    label "159.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 159
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 159"
  ]
  node [
    id 432
    label "159.00.1.0&#10;printers"
    text "printers"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.73761591270814, 0, 1, 158)]"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']&#10;corels: [(0.73761591270814, 0, 1, 158)]"
  ]
  node [
    id 433
    label "160.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 160
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 160"
  ]
  node [
    id 434
    label "160.00.2.0&#10;p@l r@l"
    text "p@l r@l"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']"
  ]
  node [
    id 435
    label "161.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 161
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 161"
  ]
  node [
    id 436
    label "161.00.1.0&#10;pintsk"
    text "pintsk"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "nc"
    cats "ent"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc', 'ent']"
  ]
  node [
    id 437
    label "162.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 162
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 162"
  ]
  node [
    id 438
    label "162.00.2.0&#10;p@l r@l"
    text "p@l r@l"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']"
  ]
  node [
    id 439
    label "162.08.1.0&#10;sound"
    text "sound"
    shape "box"
    color "pink"
    size 14
    syn_role "ccomp (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.4
    title "syn_role: ccomp (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['exp']&#10;polarity: 0.4"
  ]
  node [
    id 440
    label "163.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 163
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 163"
  ]
  node [
    id 441
    label "163.00.2.0&#10;p@l r@l"
    text "p@l r@l"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']"
  ]
  node [
    id 442
    label "164.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 164
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 164"
  ]
  node [
    id 443
    label "164.00.2.0&#10;p@l r@l"
    text "p@l r@l"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']"
  ]
  node [
    id 444
    label "164.07.1.0&#10;printer"
    text "printer"
    shape "box"
    color "cyan"
    size 10
    syn_role "appos (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.9999999254605505, 0, 1, 159)]"
    title "syn_role: appos (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.9999999254605505, 0, 1, 159)]"
  ]
  node [
    id 445
    label "165.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 165
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 165"
  ]
  node [
    id 446
    label "165.00.3.0&#10;p@l r@l e@l"
    text "p@l r@l e@l"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']"
  ]
  node [
    id 447
    label "166.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 166
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 166"
  ]
  node [
    id 448
    label "166.00.2.0&#10;n@l t@l"
    text "n@l t@l"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']"
  ]
  node [
    id 449
    label "166.02.2.0&#10;e@l r@l"
    text "e@l r@l"
    shape "box"
    color "cyan"
    size 10
    syn_role "appos (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    title "syn_role: appos (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']"
  ]
  node [
    id 450
    label "166.00.4.0&#10;n@l t@l e@l r@l"
    text "n@l t@l e@l r@l"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "ent"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['ent']"
  ]
  node [
    id 451
    label "167.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 167
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 167"
  ]
  node [
    id 452
    label "167.00.4.0&#10;p@l r@l e@l s@l"
    text "p@l r@l e@l s@l"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']"
  ]
  node [
    id 453
    label "168.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 168
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 168"
  ]
  node [
    id 454
    label "168.00.1.0&#10;sound"
    text "sound"
    shape "box"
    color "pink"
    size 14
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.4
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['exp']&#10;polarity: 0.4"
  ]
  node [
    id 455
    label "169.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 169
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 169"
  ]
  node [
    id 456
    label "169.00.4.0&#10;p@l r@l e@l s@l"
    text "p@l r@l e@l s@l"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']"
  ]
  node [
    id 457
    label "170.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 170
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 170"
  ]
  node [
    id 458
    label "170.00.2.0&#10;shortie sound"
    text "shortie sound"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "nc"
    cats "ent"
    corels "[(0.747925663873226, 7, 1, 164)]"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc', 'ent']&#10;corels: [(0.747925663873226, 7, 1, 164)]"
  ]
  node [
    id 459
    label "170.01.1.0&#10;sound"
    text "sound"
    shape "box"
    color "pink"
    size 14
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.4
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['exp']&#10;polarity: 0.4"
  ]
  node [
    id 460
    label "171.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 171
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 171"
  ]
  node [
    id 461
    label "171.00.1.0&#10;printer"
    text "printer"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.747925663873226, 0, 2, 170)]"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']&#10;corels: [(0.747925663873226, 0, 2, 170)]"
  ]
  node [
    id 462
    label "171.00.1.1&#10;US letter (display format)"
    entity_name "US letter"
    entity_type "display format"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;DisplayFormat&#34;, &#34;PaperUSLetter&#34;]"
    comment "US letter can be a sound, display format, icon or word."
    source "Wikipedia"
    certainty_type "quite_certain"
    title "Wolfram_expression: Entity[&#34;DisplayFormat&#34;, &#34;PaperUSLetter&#34;]&#10;comment: US letter can be a sound, display format, icon or word.&#10;source: Wikipedia"
  ]
  node [
    id 463
    label "172.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 172
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 172"
  ]
  node [
    id 464
    label "172.00.2.0&#10;shortie sound"
    text "shortie sound"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "nc"
    cats "ent"
    corels "[(0.747925663873226, 0, 1, 171)]"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc', 'ent']&#10;corels: [(0.747925663873226, 0, 1, 171)]"
  ]
  node [
    id 465
    label "172.01.1.0&#10;sound"
    text "sound"
    shape "box"
    color "pink"
    size 14
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.4
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['exp']&#10;polarity: 0.4"
  ]
  node [
    id 466
    label "173.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 173
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 173"
  ]
  node [
    id 467
    label "173.00.4.0&#10;p@l r@l e@l s@l"
    text "p@l r@l e@l s@l"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']"
  ]
  node [
    id 468
    label "174.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 174
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 174"
  ]
  node [
    id 469
    label "174.00.4.0&#10;p@l r@l e@l s@l"
    text "p@l r@l e@l s@l"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']"
  ]
  node [
    id 470
    label "175.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 175
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 175"
  ]
  node [
    id 471
    label "175.02.1.0&#10;right"
    text "right"
    shape "box"
    color "pink"
    size 13
    syn_role "acomp (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.2857142857142857
    title "syn_role: acomp (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['exp']&#10;polarity: 0.2857142857142857"
  ]
  node [
    id 472
    label "176.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 176
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 176"
  ]
  node [
    id 473
    label "176.00.1.0&#10;e@l"
    text "e@l"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']"
  ]
  node [
    id 474
    label "176.01.3.0&#10;n@l t@l s@l"
    text "n@l t@l s@l"
    shape "box"
    color "cyan"
    size 10
    syn_role "appos (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    title "syn_role: appos (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']"
  ]
  node [
    id 475
    label "176.00.4.0&#10;e@l n@l t@l s@l"
    text "e@l n@l t@l s@l"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "ent"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['ent']"
  ]
  node [
    id 476
    label "177.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 177
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 177"
  ]
  node [
    id 477
    label "177.00.1.0&#10;e@l"
    text "e@l"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "nc"
    cats "ent"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc', 'ent']"
  ]
  node [
    id 478
    label "178.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 178
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 178"
  ]
  node [
    id 479
    label "178.00.3.0&#10;e@l n@l t@l"
    text "e@l n@l t@l"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']"
  ]
  node [
    id 480
    label "178.03.2.0&#10;e@l s@l"
    text "e@l s@l"
    shape "box"
    color "cyan"
    size 10
    syn_role "appos (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    title "syn_role: appos (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']"
  ]
  node [
    id 481
    label "178.00.2.0&#10;e@l n@l"
    text "e@l n@l"
    shape "box"
    color "cyan"
    size 10
    syn_role "compound (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "ent"
    title "syn_role: compound (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['ent']"
  ]
  node [
    id 482
    label "179.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 179
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 179"
  ]
  node [
    id 483
    label "179.00.1.0&#10;presents"
    text "presents"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7054817713468109, 0, 2, 172)]"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']&#10;corels: [(0.7054817713468109, 0, 2, 172)]"
  ]
  node [
    id 484
    label "180.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 180
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 180"
  ]
  node [
    id 485
    label "180.00.1.0&#10;presents"
    text "presents"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.999999921440576, 0, 1, 179)]"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']&#10;corels: [(0.999999921440576, 0, 1, 179)]"
  ]
  node [
    id 486
    label "181.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 181
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 181"
  ]
  node [
    id 487
    label "181.00.1.0&#10;presents"
    text "presents"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.999999921440576, 0, 1, 180)]"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']&#10;corels: [(0.999999921440576, 0, 1, 180)]"
  ]
  node [
    id 488
    label "182.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 182
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 182"
  ]
  node [
    id 489
    label "182.02.4.0&#10;e@l n@l t@l s@l"
    text "e@l n@l t@l s@l"
    shape "box"
    color "cyan"
    size 10
    syn_role "attr (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    title "syn_role: attr (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']"
  ]
  node [
    id 490
    label "182.11.2.0&#10;the sound"
    text "the sound"
    shape "box"
    color "cyan"
    size 10
    syn_role "dobj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7388969951104069, 0, 1, 181)]"
    title "syn_role: dobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.7388969951104069, 0, 1, 181)]"
  ]
  node [
    id 491
    label "182.02.2.0&#10;e@l n@l"
    text "e@l n@l"
    shape "box"
    color "cyan"
    size 10
    syn_role "nmod (head: attr)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "ent"
    title "syn_role: nmod (head: attr)&#10;wrt_ROOT: right&#10;cats: ['ent']"
  ]
  node [
    id 492
    label "182.12.1.0&#10;sound"
    text "sound"
    shape "box"
    color "pink"
    size 14
    syn_role "dobj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.4
    title "syn_role: dobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['exp']&#10;polarity: 0.4"
  ]
  node [
    id 493
    label "183.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 183
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 183"
  ]
  node [
    id 494
    label "183.00.1.0&#10;e@l"
    text "e@l"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']"
  ]
  node [
    id 495
    label "183.01.3.0&#10;n@l t@l s@l"
    text "n@l t@l s@l"
    shape "box"
    color "cyan"
    size 10
    syn_role "appos (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    title "syn_role: appos (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']"
  ]
  node [
    id 496
    label "183.00.4.0&#10;e@l n@l t@l s@l"
    text "e@l n@l t@l s@l"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "ent"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['ent']"
  ]
  node [
    id 497
    label "184.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 184
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 184"
  ]
  node [
    id 498
    label "184.00.1.0&#10;presents"
    text "presents"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7388969951104069, 11, 2, 182)]"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']&#10;corels: [(0.7388969951104069, 11, 2, 182)]"
  ]
  node [
    id 499
    label "185.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 185
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 185"
  ]
  node [
    id 500
    label "185.00.1.0&#10;presents"
    text "presents"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.999999921440576, 0, 1, 184)]"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']&#10;corels: [(0.999999921440576, 0, 1, 184)]"
  ]
  node [
    id 501
    label "186.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 186
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 186"
  ]
  node [
    id 502
    label "186.00.1.0&#10;presents"
    text "presents"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.999999921440576, 0, 1, 185)]"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']&#10;corels: [(0.999999921440576, 0, 1, 185)]"
  ]
  node [
    id 503
    label "187.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 187
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 187"
  ]
  node [
    id 504
    label "187.03.2.0&#10;the presents"
    text "the presents"
    shape "box"
    color "cyan"
    size 10
    syn_role "dobj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.999999921440576, 0, 1, 186)]"
    title "syn_role: dobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.999999921440576, 0, 1, 186)]"
  ]
  node [
    id 505
    label "188.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 188
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 188"
  ]
  node [
    id 506
    label "189.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 189
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 189"
  ]
  node [
    id 507
    label "190.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 190
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 190"
  ]
  node [
    id 508
    label "190.03.3.0&#10;a rock tumbler"
    text "a rock tumbler"
    shape "box"
    color "cyan"
    size 10
    syn_role "dobj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(1.0000000634262258, 0, 3, 157)]"
    title "syn_role: dobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(1.0000000634262258, 0, 3, 157)]"
  ]
  node [
    id 509
    label "190.01.1.0&#10;I"
    text "I"
    shape "box"
    color "yellow"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "."
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['sfs']&#10;clause_type: ."
  ]
  node [
    id 510
    label "191.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 191
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 191"
  ]
  node [
    id 511
    label "192.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 192
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 192"
  ]
  node [
    id 512
    label "193.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 193
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 193"
  ]
  node [
    id 513
    label "193.02.3.0&#10;all the rocks"
    text "all the rocks"
    shape "box"
    color "cyan"
    size 10
    syn_role "dobj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.6659714575585227, 3, 2, 187)]"
    title "syn_role: dobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.6659714575585227, 3, 2, 187)]"
  ]
  node [
    id 514
    label "194.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 194
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 194"
  ]
  node [
    id 515
    label "194.03.3.0&#10;a rock tumbler"
    text "a rock tumbler"
    shape "box"
    color "cyan"
    size 10
    syn_role "dobj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(1.0000000634262258, 3, 3, 190)]"
    title "syn_role: dobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(1.0000000634262258, 3, 3, 190)]"
  ]
  node [
    id 516
    label "195.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 195
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 195"
  ]
  node [
    id 517
    label "196.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 196
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 196"
  ]
  node [
    id 518
    label "196.00.3.0&#10;what other kinds"
    text "what other kinds"
    shape "box"
    color "cyan"
    size 10
    syn_role "dobj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7293779545329983, 14, 2, 155)]"
    title "syn_role: dobj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['nc']&#10;corels: [(0.7293779545329983, 14, 2, 155)]"
  ]
  node [
    id 519
    label "196.04.1.0&#10;presents"
    text "presents"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.6669985743584493, 0, 3, 155)]"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: left&#10;cats: ['nc']&#10;corels: [(0.6669985743584493, 0, 3, 155)]"
  ]
  node [
    id 520
    label "196.06.1.0&#10;you"
    text "you"
    shape "box"
    color "yellow"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "?"
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['sfs']&#10;clause_type: ?"
  ]
  node [
    id 521
    label "196.01.1.0&#10;other"
    text "other"
    shape "box"
    color "pink"
    size 11
    syn_role "amod (head: dobj)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "exp"
    polarity -0.125
    title "syn_role: amod (head: dobj)&#10;wrt_ROOT: left&#10;cats: ['exp']&#10;polarity: -0.125"
  ]
  node [
    id 522
    label "197.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 197
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 197"
  ]
  node [
    id 523
    label "197.06.2.0&#10;the tumble"
    text "the tumble"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7714392218124476, 2, 3, 193)]"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']&#10;corels: [(0.7714392218124476, 2, 3, 193)]"
  ]
  node [
    id 524
    label "197.00.2.0&#10;&#38;-um"
    text "&#38;-um"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "ent"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['ent']"
  ]
  node [
    id 525
    label "198.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 198
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 198"
  ]
  node [
    id 526
    label "198.03.4.0&#10;some nice still hands"
    text "some nice still hands"
    shape "box"
    color "cyan"
    size 10
    syn_role "dobj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7879314387847862, 4, 1, 196)]"
    title "syn_role: dobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.7879314387847862, 4, 1, 196)]"
  ]
  node [
    id 527
    label "198.01.1.0&#10;you"
    text "you"
    shape "box"
    color "yellow"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "?"
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['sfs']&#10;clause_type: ?"
  ]
  node [
    id 528
    label "198.04.1.0&#10;nice"
    text "nice"
    shape "box"
    color "pink"
    size 16
    syn_role "amod (head: dobj)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.6
    title "syn_role: amod (head: dobj)&#10;wrt_ROOT: right&#10;cats: ['exp']&#10;polarity: 0.6"
  ]
  node [
    id 529
    label "199.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 199
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 199"
  ]
  node [
    id 530
    label "199.06.2.0&#10;the tumbler"
    text "the tumbler"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(1.0000000634262258, 3, 3, 194)]"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(1.0000000634262258, 3, 3, 194)]"
  ]
  node [
    id 531
    label "199.00.1.0&#10;okay"
    text "okay"
    shape "box"
    color "pink"
    size 15
    syn_role "intj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.5
    title "syn_role: intj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['exp']&#10;polarity: 0.5"
  ]
  node [
    id 532
    label "199.04.1.0&#10;me"
    text "me"
    shape "box"
    color "yellow"
    size 10
    syn_role "dobj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "."
    title "syn_role: dobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['sfs']&#10;clause_type: ."
  ]
  node [
    id 533
    label "200.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 200
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 200"
  ]
  node [
    id 534
    label "200.11.3.0&#10;your other presents"
    text "your other presents"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7879314387847862, 3, 4, 198)]"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.7879314387847862, 3, 4, 198)]"
  ]
  node [
    id 535
    label "200.07.1.0&#10;you"
    text "you"
    shape "box"
    color "yellow"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "?"
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['sfs']&#10;clause_type: ?"
  ]
  node [
    id 536
    label "200.12.1.0&#10;other"
    text "other"
    shape "box"
    color "pink"
    size 11
    syn_role "amod (head: pobj)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "exp"
    polarity -0.125
    title "syn_role: amod (head: pobj)&#10;wrt_ROOT: right&#10;cats: ['exp']&#10;polarity: -0.125"
  ]
  node [
    id 537
    label "201.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 201
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 201"
  ]
  node [
    id 538
    label "201.08.3.0&#10;a hockey stick"
    text "a hockey stick"
    shape "box"
    color "cyan"
    size 10
    syn_role "dobj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7141621038665686, 11, 3, 200)]"
    title "syn_role: dobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.7141621038665686, 11, 3, 200)]"
  ]
  node [
    id 539
    label "201.00.2.0&#10;&#38;-um"
    text "&#38;-um"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "ent"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['ent']"
  ]
  node [
    id 540
    label "201.06.1.0&#10;I"
    text "I"
    shape "box"
    color "yellow"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "."
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['sfs']&#10;clause_type: ."
  ]
  node [
    id 541
    label "201.09.2.0&#10;hockey stick"
    text "hockey stick"
    shape "box"
    color "cyan"
    size 10
    syn_role "dobj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "wn_np"
    wn_sense "hockey_stick.n.01"
    definition "sports implement consisting of a stick used by hockey players to move the puck"
    title "syn_role: dobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['wn_np']&#10;wn_sense: hockey_stick.n.01&#10;definition: sports implement consisting of a stick used by hockey players to move the puck"
  ]
  node [
    id 542
    label "201.08.3.1&#10;NHL hockey stick (sport object)"
    entity_name "NHL hockey stick"
    entity_type "sport object"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;SportObject&#34;, &#34;HockeyStickNHL&#34;]"
    comment "NHL hockey stick can be a global climate data, word or sport object."
    source "2011-12 Official NHL Rulebook: Rule 10 - Sticks"
    certainty_type "quite_certain"
    title "Wolfram_expression: Entity[&#34;SportObject&#34;, &#34;HockeyStickNHL&#34;]&#10;comment: NHL hockey stick can be a global climate data, word or sport object.&#10;source: 2011-12 Official NHL Rulebook: Rule 10 - Sticks"
  ]
  node [
    id 543
    label "201.08.3.2&#10;ice hockey (sport)"
    entity_name "ice hockey"
    entity_type "sport"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;Sport&#34;, &#34;IceHockey&#34;]"
    comment "According to 2011-12 Official NHL Rulebook: Rule 10 - Sticks, the sport, relating to the sport object, is ice hockey."
    source "2011-12 Official NHL Rulebook: Rule 10 - Sticks"
    certainty_type "absolutely_certain"
    title "Wolfram_expression: Entity[&#34;Sport&#34;, &#34;IceHockey&#34;]&#10;comment: According to 2011-12 Official NHL Rulebook: Rule 10 - Sticks, the sport, relating to the sport object, is ice hockey.&#10;source: 2011-12 Official NHL Rulebook: Rule 10 - Sticks"
  ]
  node [
    id 544
    label "202.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 202
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 202"
  ]
  node [
    id 545
    label "203.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 203
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 203"
  ]
  node [
    id 546
    label "203.03.2.0&#10;a present"
    text "a present"
    shape "box"
    color "cyan"
    size 10
    syn_role "dobj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7665873673805427, 0, 3, 196)]"
    title "syn_role: dobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.7665873673805427, 0, 3, 196)]"
  ]
  node [
    id 547
    label "203.04.1.0&#10;present"
    text "present"
    shape "box"
    color "pink"
    size 10
    syn_role "dobj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.0
    title "syn_role: dobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['exp']&#10;polarity: 0.0"
  ]
  node [
    id 548
    label "204.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 204
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 204"
  ]
  node [
    id 549
    label "204.00.2.0&#10;what present"
    text "what present"
    shape "box"
    color "cyan"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.999999921440576, 3, 2, 203)]"
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['nc']&#10;corels: [(0.999999921440576, 3, 2, 203)]"
  ]
  node [
    id 550
    label "204.01.1.0&#10;present"
    text "present"
    shape "box"
    color "pink"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.0
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['exp']&#10;polarity: 0.0"
  ]
  node [
    id 551
    label "205.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 205
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 205"
  ]
  node [
    id 552
    label "205.00.2.0&#10;that present"
    text "that present"
    shape "box"
    color "cyan"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.6908767432737982, 6, 2, 199)]"
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['nc']&#10;corels: [(0.6908767432737982, 6, 2, 199)]"
  ]
  node [
    id 553
    label "205.01.1.0&#10;present"
    text "present"
    shape "box"
    color "pink"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.0
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['exp']&#10;polarity: 0.0"
  ]
  node [
    id 554
    label "206.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 206
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 206"
  ]
  node [
    id 555
    label "206.15.2.0&#10;some men"
    text "some men"
    shape "box"
    color "cyan"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7188526397901721, 0, 2, 205)]"
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['nc']&#10;corels: [(0.7188526397901721, 0, 2, 205)]"
  ]
  node [
    id 556
    label "206.07.2.0&#10;next week"
    text "next week"
    shape "box"
    color "cyan"
    size 10
    syn_role "npadvmod (head: xcomp)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "ent"
    title "syn_role: npadvmod (head: xcomp)&#10;wrt_ROOT: right&#10;cats: ['ent']"
  ]
  node [
    id 557
    label "206.07.1.0&#10;next"
    text "next"
    shape "box"
    color "pink"
    size 10
    syn_role "amod (head: npadvmod)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.0
    title "syn_role: amod (head: npadvmod)&#10;wrt_ROOT: right&#10;cats: ['exp']&#10;polarity: 0.0"
  ]
  node [
    id 558
    label "206.15.2.1&#10;Some Men (music work rendition)"
    entity_name "Some Men"
    entity_type "music work rendition"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicWorkRendition&#34;, &#34;SomeMenHallAndOates::8mykt&#34;]"
    comment "Some Men can be a music work rendition or music work."
    source "MusicBrainz Database"
    certainty_type "quite_certain"
    title "Wolfram_expression: Entity[&#34;MusicWorkRendition&#34;, &#34;SomeMenHallAndOates::8mykt&#34;]&#10;comment: Some Men can be a music work rendition or music work.&#10;source: MusicBrainz Database"
  ]
  node [
    id 559
    label "206.15.2.2&#10;Private Eyes (canonical album)"
    entity_name "Private Eyes"
    entity_type "canonical album"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicAlbum&#34;, &#34;PrivateEyes::88m36&#34;]"
    comment "According to MusicBrainz Database, the canonical album, relating to the music work rendition, is Private Eyes."
    source "MusicBrainz Database"
    certainty_type "absolutely_certain"
    title "Wolfram_expression: Entity[&#34;MusicAlbum&#34;, &#34;PrivateEyes::88m36&#34;]&#10;comment: According to MusicBrainz Database, the canonical album, relating to the music work rendition, is Private Eyes.&#10;source: MusicBrainz Database"
  ]
  node [
    id 560
    label "206.15.2.3&#10;Some Men (canonical recording)"
    entity_name "Some Men"
    entity_type "canonical recording"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicWorkRecording&#34;, &#34;SomeMen::67g26&#34;]"
    comment "According to MusicBrainz Database, the canonical recording, relating to the music work rendition, is also named Some Men."
    source "MusicBrainz Database"
    certainty_type "absolutely_certain"
    title "Wolfram_expression: Entity[&#34;MusicWorkRecording&#34;, &#34;SomeMen::67g26&#34;]&#10;comment: According to MusicBrainz Database, the canonical recording, relating to the music work rendition, is also named Some Men.&#10;source: MusicBrainz Database"
  ]
  node [
    id 561
    label "207.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 207
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 207"
  ]
  node [
    id 562
    label "208.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 208
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 208"
  ]
  node [
    id 563
    label "208.00.2.0&#10;the swings"
    text "the swings"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7259285242755035, 8, 3, 201)]"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']&#10;corels: [(0.7259285242755035, 8, 3, 201)]"
  ]
  node [
    id 564
    label "209.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 209
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 209"
  ]
  node [
    id 565
    label "210.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 210
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 210"
  ]
  node [
    id 566
    label "210.01.2.0&#10;the trees"
    text "the trees"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7203428179501424, 0, 2, 204)]"
    title "syn_role: pobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.7203428179501424, 0, 2, 204)]"
  ]
  node [
    id 567
    label "211.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 211
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 211"
  ]
  node [
    id 568
    label "211.03.2.0&#10;the ground"
    text "the ground"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.6883309052864174, 15, 2, 206)]"
    title "syn_role: pobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.6883309052864174, 15, 2, 206)]"
  ]
  node [
    id 569
    label "212.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 212
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 212"
  ]
  node [
    id 570
    label "212.03.2.0&#10;the ground"
    text "the ground"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.8708349174028114, 3, 2, 211)]"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.8708349174028114, 3, 2, 211)]"
  ]
  node [
    id 571
    label "213.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 213
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 213"
  ]
  node [
    id 572
    label "213.00.4.0&#10;well , &#38;-um"
    text "well , &#38;-um"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "ent"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['ent']"
  ]
  node [
    id 573
    label "214.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 214
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 214"
  ]
  node [
    id 574
    label "214.05.1.0&#10;swings"
    text "swings"
    shape "box"
    color "cyan"
    size 10
    syn_role "attr (head: xcomp)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.9999999890893256, 0, 2, 208)]"
    title "syn_role: attr (head: xcomp)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.9999999890893256, 0, 2, 208)]"
  ]
  node [
    id 575
    label "214.08.3.0&#10;daddy's house"
    text "daddy's house"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7586774426944894, 3, 2, 212)]"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.7586774426944894, 3, 2, 212)]"
  ]
  node [
    id 576
    label "215.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 215
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 215"
  ]
  node [
    id 577
    label "215.06.3.0&#10;daddy's house"
    text "daddy's house"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.9999999485566696, 8, 3, 214)]"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.9999999485566696, 8, 3, 214)]"
  ]
  node [
    id 578
    label "216.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 216
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 216"
  ]
  node [
    id 579
    label "216.02.2.0&#10;what kind"
    text "what kind"
    shape "box"
    color "cyan"
    size 10
    syn_role "attr (head: ccomp)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7721263892863034, 1, 2, 210)]"
    title "syn_role: attr (head: ccomp)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.7721263892863034, 1, 2, 210)]"
  ]
  node [
    id 580
    label "216.03.1.0&#10;kind"
    text "kind"
    shape "box"
    color "pink"
    size 16
    syn_role "attr (head: ccomp)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.6
    title "syn_role: attr (head: ccomp)&#10;wrt_ROOT: right&#10;cats: ['exp']&#10;polarity: 0.6"
  ]
  node [
    id 581
    label "217.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 217
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 217"
  ]
  node [
    id 582
    label "217.04.1.0&#10;fun"
    text "fun"
    shape "box"
    color "pink"
    size 13
    syn_role "compound (head: dobj)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.3
    title "syn_role: compound (head: dobj)&#10;wrt_ROOT: right&#10;cats: ['exp']&#10;polarity: 0.3"
  ]
  node [
    id 583
    label "218.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 218
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 218"
  ]
  node [
    id 584
    label "219.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 219
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 219"
  ]
  node [
    id 585
    label "219.02.3.0&#10;some other presents"
    text "some other presents"
    shape "box"
    color "cyan"
    size 10
    syn_role "dobj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7665873673805427, 2, 2, 216)]"
    title "syn_role: dobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.7665873673805427, 2, 2, 216)]"
  ]
  node [
    id 586
    label "219.03.1.0&#10;other"
    text "other"
    shape "box"
    color "pink"
    size 11
    syn_role "amod (head: dobj)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "exp"
    polarity -0.125
    title "syn_role: amod (head: dobj)&#10;wrt_ROOT: right&#10;cats: ['exp']&#10;polarity: -0.125"
  ]
  node [
    id 587
    label "220.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 220
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 220"
  ]
  node [
    id 588
    label "220.11.3.0&#10;the rock tumbler"
    text "the rock tumbler"
    shape "box"
    color "cyan"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7558324851970443, 6, 2, 197)]"
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['nc']&#10;corels: [(0.7558324851970443, 6, 2, 197)]"
  ]
  node [
    id 589
    label "220.00.2.0&#10;&#38;-um"
    text "&#38;-um"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "ent"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['ent']"
  ]
  node [
    id 590
    label "220.11.0.0&#10;"
    text ""
    shape "box"
    color "yellow"
    size 10
    syn_role "n/a"
    wrt_ROOT "n/a"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "!"
    title "syn_role: n/a&#10;wrt_ROOT: n/a&#10;cats: ['sfs']&#10;clause_type: !"
  ]
  node [
    id 591
    label "220.16.1.0&#10;me"
    text "me"
    shape "box"
    color "yellow"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "!"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: right&#10;cats: ['sfs']&#10;clause_type: !"
  ]
  node [
    id 592
    label "221.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 221
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 221"
  ]
  node [
    id 593
    label "221.00.1.0&#10;sure"
    text "sure"
    shape "box"
    color "pink"
    size 15
    syn_role "advmod (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.5
    title "syn_role: advmod (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['exp']&#10;polarity: 0.5"
  ]
  node [
    id 594
    label "222.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 222
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 222"
  ]
  node [
    id 595
    label "222.04.3.0&#10;my rock tumbler"
    text "my rock tumbler"
    shape "box"
    color "cyan"
    size 10
    syn_role "attr (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7803112885867443, 11, 3, 220)]"
    title "syn_role: attr (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.7803112885867443, 11, 3, 220)]"
  ]
  node [
    id 596
    label "223.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 223
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 223"
  ]
  node [
    id 597
    label "224.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 224
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 224"
  ]
  node [
    id 598
    label "225.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 225
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 225"
  ]
  node [
    id 599
    label "226.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 226
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 226"
  ]
  node [
    id 600
    label "227.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 227
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 227"
  ]
  node [
    id 601
    label "228.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 228
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 228"
  ]
  node [
    id 602
    label "229.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 229
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 229"
  ]
  node [
    id 603
    label "230.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 230
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 230"
  ]
  node [
    id 604
    label "231.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 231
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 231"
  ]
  node [
    id 605
    label "232.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 232
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 232"
  ]
  node [
    id 606
    label "232.10.2.0&#10;some slippers"
    text "some slippers"
    shape "box"
    color "cyan"
    size 10
    syn_role "dobj (head: conj)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7374209937059609, 4, 3, 222)]"
    title "syn_role: dobj (head: conj)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.7374209937059609, 4, 3, 222)]"
  ]
  node [
    id 607
    label "232.13.2.0&#10;my head"
    text "my head"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7416302160985657, 6, 3, 215)]"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.7416302160985657, 6, 3, 215)]"
  ]
  node [
    id 608
    label "232.16.2.0&#10;my head"
    text "my head"
    shape "box"
    color "cyan"
    size 10
    syn_role "nsubj (head: advcl)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.6903315745086769, 5, 1, 214)]"
    title "syn_role: nsubj (head: advcl)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.6903315745086769, 5, 1, 214)]"
  ]
  node [
    id 609
    label "232.00.1.0&#10;Brett"
    text "Brett"
    shape "box"
    color "cyan"
    size 10
    syn_role "npadvmod (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "ent"
    title "syn_role: npadvmod (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['ent']"
  ]
  node [
    id 610
    label "232.19.1.0&#10;warm"
    text "warm"
    shape "box"
    color "pink"
    size 16
    syn_role "acomp (head: advcl)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.6
    title "syn_role: acomp (head: advcl)&#10;wrt_ROOT: right&#10;cats: ['exp']&#10;polarity: 0.6"
  ]
  node [
    id 611
    label "232.02.1.0&#10;I"
    text "I"
    shape "box"
    color "yellow"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "."
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['sfs']&#10;clause_type: ."
  ]
  node [
    id 612
    label "232.13.2.1&#10;My Head? (music work rendition)"
    entity_name "My Head?"
    entity_type "music work rendition"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicWorkRendition&#34;, &#34;MyHeadFrankZappa::k2954&#34;]"
    comment "My Head? can be a anatomical structure, music work rendition or music work."
    source "MusicBrainz Database"
    certainty_type "quite_certain"
    title "Wolfram_expression: Entity[&#34;MusicWorkRendition&#34;, &#34;MyHeadFrankZappa::k2954&#34;]&#10;comment: My Head? can be a anatomical structure, music work rendition or music work.&#10;source: MusicBrainz Database"
  ]
  node [
    id 613
    label "232.13.2.2&#10;You Can't Do That On Stage Anymore, Vol. 5 (canonical album)"
    entity_name "You Can't Do That On Stage Anymore, Vol. 5"
    entity_type "canonical album"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicAlbum&#34;, &#34;YouCantDoThatonStageAnymoreVolume5::45ysg&#34;]"
    comment "According to MusicBrainz Database, the canonical album, relating to the music work rendition, is You Can't Do That On Stage Anymore, Vol. 5."
    source "MusicBrainz Database"
    certainty_type "absolutely_certain"
    title "Wolfram_expression: Entity[&#34;MusicAlbum&#34;, &#34;YouCantDoThatonStageAnymoreVolume5::45ysg&#34;]&#10;comment: According to MusicBrainz Database, the canonical album, relating to the music work rendition, is You Can't Do That On Stage Anymore, Vol. 5.&#10;source: MusicBrainz Database"
  ]
  node [
    id 614
    label "232.13.2.3&#10;My Head? (canonical recording)"
    entity_name "My Head?"
    entity_type "canonical recording"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicWorkRecording&#34;, &#34;MyHead::933k3&#34;]"
    comment "According to MusicBrainz Database, the canonical recording, relating to the music work rendition, is also named My Head?."
    source "MusicBrainz Database"
    certainty_type "absolutely_certain"
    title "Wolfram_expression: Entity[&#34;MusicWorkRecording&#34;, &#34;MyHead::933k3&#34;]&#10;comment: According to MusicBrainz Database, the canonical recording, relating to the music work rendition, is also named My Head?.&#10;source: MusicBrainz Database"
  ]
  node [
    id 615
    label "232.16.2.1&#10;My Head? (music work rendition)"
    entity_name "My Head?"
    entity_type "music work rendition"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicWorkRendition&#34;, &#34;MyHeadFrankZappa::k2954&#34;]"
    comment "My Head? can be a anatomical structure, music work rendition or music work."
    source "MusicBrainz Database"
    certainty_type "quite_certain"
    title "Wolfram_expression: Entity[&#34;MusicWorkRendition&#34;, &#34;MyHeadFrankZappa::k2954&#34;]&#10;comment: My Head? can be a anatomical structure, music work rendition or music work.&#10;source: MusicBrainz Database"
  ]
  node [
    id 616
    label "232.16.2.2&#10;You Can't Do That On Stage Anymore, Vol. 5 (canonical album)"
    entity_name "You Can't Do That On Stage Anymore, Vol. 5"
    entity_type "canonical album"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicAlbum&#34;, &#34;YouCantDoThatonStageAnymoreVolume5::45ysg&#34;]"
    comment "According to MusicBrainz Database, the canonical album, relating to the music work rendition, is You Can't Do That On Stage Anymore, Vol. 5."
    source "MusicBrainz Database"
    certainty_type "absolutely_certain"
    title "Wolfram_expression: Entity[&#34;MusicAlbum&#34;, &#34;YouCantDoThatonStageAnymoreVolume5::45ysg&#34;]&#10;comment: According to MusicBrainz Database, the canonical album, relating to the music work rendition, is You Can't Do That On Stage Anymore, Vol. 5.&#10;source: MusicBrainz Database"
  ]
  node [
    id 617
    label "232.16.2.3&#10;My Head? (canonical recording)"
    entity_name "My Head?"
    entity_type "canonical recording"
    shape "box"
    color "cyan"
    size 10
    Wolfram_expression "Entity[&#34;MusicWorkRecording&#34;, &#34;MyHead::933k3&#34;]"
    comment "According to MusicBrainz Database, the canonical recording, relating to the music work rendition, is also named My Head?."
    source "MusicBrainz Database"
    certainty_type "absolutely_certain"
    title "Wolfram_expression: Entity[&#34;MusicWorkRecording&#34;, &#34;MyHead::933k3&#34;]&#10;comment: According to MusicBrainz Database, the canonical recording, relating to the music work rendition, is also named My Head?.&#10;source: MusicBrainz Database"
  ]
  node [
    id 618
    label "233.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 233
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 233"
  ]
  node [
    id 619
    label "233.00.3.0&#10;why a slippers"
    text "why a slippers"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.999999990594579, 10, 2, 232)]"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']&#10;corels: [(0.999999990594579, 10, 2, 232)]"
  ]
  node [
    id 620
    label "234.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 234
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 234"
  ]
  node [
    id 621
    label "234.06.2.0&#10;my toothbrush"
    text "my toothbrush"
    shape "box"
    color "cyan"
    size 10
    syn_role "dobj (head: advcl)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.783857705574777, 0, 3, 233)]"
    title "syn_role: dobj (head: advcl)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.783857705574777, 0, 3, 233)]"
  ]
  node [
    id 622
    label "234.10.2.0&#10;my hair"
    text "my hair"
    shape "box"
    color "cyan"
    size 10
    syn_role "appos (head: dobj)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7635083280109909, 13, 2, 232)]"
    title "syn_role: appos (head: dobj)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.7635083280109909, 13, 2, 232)]"
  ]
  node [
    id 623
    label "234.00.1.0&#10;I"
    text "I"
    shape "box"
    color "yellow"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "."
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['sfs']&#10;clause_type: ."
  ]
  node [
    id 624
    label "235.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 235
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 235"
  ]
  node [
    id 625
    label "235.00.5.0&#10;oh , why a toothbrush"
    text "oh , why a toothbrush"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7339134470620996, 6, 2, 234)]"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']&#10;corels: [(0.7339134470620996, 6, 2, 234)]"
  ]
  node [
    id 626
    label "236.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 236
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 236"
  ]
  node [
    id 627
    label "237.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 237
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 237"
  ]
  node [
    id 628
    label "237.00.1.0&#10;Brett"
    text "Brett"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "nc"
    cats "ent"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc', 'ent']"
  ]
  node [
    id 629
    label "238.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 238
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 238"
  ]
  node [
    id 630
    label "238.00.0.0&#10;"
    text ""
    shape "box"
    color "yellow"
    size 10
    syn_role "n/a"
    wrt_ROOT "n/a"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "!"
    title "syn_role: n/a&#10;wrt_ROOT: n/a&#10;cats: ['sfs']&#10;clause_type: !"
  ]
  node [
    id 631
    label "239.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 239
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 239"
  ]
  node [
    id 632
    label "239.00.1.0&#10;Brett"
    text "Brett"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "nc"
    cats "ent"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc', 'ent']"
  ]
  node [
    id 633
    label "240.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 240
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 240"
  ]
  node [
    id 634
    label "240.02.2.0&#10;your boots"
    text "your boots"
    shape "box"
    color "cyan"
    size 10
    syn_role "dobj (head: advcl)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7658322569721702, 10, 2, 234)]"
    title "syn_role: dobj (head: advcl)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.7658322569721702, 10, 2, 234)]"
  ]
  node [
    id 635
    label "240.05.2.0&#10;your arms"
    text "your arms"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7434162955231911, 16, 2, 232)]"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.7434162955231911, 16, 2, 232)]"
  ]
  node [
    id 636
    label "240.10.1.0&#10;warm"
    text "warm"
    shape "box"
    color "pink"
    size 16
    syn_role "acomp (head: advcl)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.6
    title "syn_role: acomp (head: advcl)&#10;wrt_ROOT: right&#10;cats: ['exp']&#10;polarity: 0.6"
  ]
  node [
    id 637
    label "240.00.0.0&#10;"
    text ""
    shape "box"
    color "yellow"
    size 10
    syn_role "n/a"
    wrt_ROOT "n/a"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "!"
    title "syn_role: n/a&#10;wrt_ROOT: n/a&#10;cats: ['sfs']&#10;clause_type: !"
  ]
  node [
    id 638
    label "241.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 241
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 241"
  ]
  node [
    id 639
    label "241.04.2.0&#10;my boots"
    text "my boots"
    shape "box"
    color "cyan"
    size 10
    syn_role "dobj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(1.0000000573255683, 2, 2, 240)]"
    title "syn_role: dobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(1.0000000573255683, 2, 2, 240)]"
  ]
  node [
    id 640
    label "241.07.2.0&#10;my arm"
    text "my arm"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.8687678099653074, 5, 2, 240)]"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.8687678099653074, 5, 2, 240)]"
  ]
  node [
    id 641
    label "241.00.1.0&#10;I"
    text "I"
    shape "box"
    color "yellow"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "."
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['sfs']&#10;clause_type: ."
  ]
  node [
    id 642
    label "242.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 242
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 242"
  ]
  node [
    id 643
    label "242.05.1.0&#10;warm"
    text "warm"
    shape "box"
    color "pink"
    size 16
    syn_role "acomp (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.6
    title "syn_role: acomp (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['exp']&#10;polarity: 0.6"
  ]
  node [
    id 644
    label "243.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 243
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 243"
  ]
  node [
    id 645
    label "243.06.2.0&#10;your teeth"
    text "your teeth"
    shape "box"
    color "cyan"
    size 10
    syn_role "dobj (head: conj)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7330836214201605, 0, 5, 235)]"
    title "syn_role: dobj (head: conj)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.7330836214201605, 0, 5, 235)]"
  ]
  node [
    id 646
    label "244.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 244
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 244"
  ]
  node [
    id 647
    label "244.00.1.0&#10;I"
    text "I"
    shape "box"
    color "yellow"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "."
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['sfs']&#10;clause_type: ."
  ]
  node [
    id 648
    label "245.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 245
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 245"
  ]
  node [
    id 649
    label "246.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 246
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 246"
  ]
  node [
    id 650
    label "247.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 247
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 247"
  ]
  node [
    id 651
    label "247.01.1.0&#10;really"
    text "really"
    shape "box"
    color "pink"
    size 12
    syn_role "advmod (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.2
    title "syn_role: advmod (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['exp']&#10;polarity: 0.2"
  ]
  node [
    id 652
    label "247.00.1.0&#10;I"
    text "I"
    shape "box"
    color "yellow"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "."
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['sfs']&#10;clause_type: ."
  ]
  node [
    id 653
    label "248.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 248
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 248"
  ]
  node [
    id 654
    label "248.01.1.0&#10;I"
    text "I"
    shape "box"
    color "yellow"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "."
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['sfs']&#10;clause_type: ."
  ]
  node [
    id 655
    label "249.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 249
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 249"
  ]
  node [
    id 656
    label "250.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 250
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 250"
  ]
  node [
    id 657
    label "251.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 251
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 251"
  ]
  node [
    id 658
    label "251.00.1.0&#10;very"
    text "very"
    shape "box"
    color "pink"
    size 12
    syn_role "advmod (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.2
    title "syn_role: advmod (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['exp']&#10;polarity: 0.2"
  ]
  node [
    id 659
    label "252.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 252
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 252"
  ]
  node [
    id 660
    label "252.00.1.0&#10;sorry"
    text "sorry"
    shape "box"
    color "pink"
    size 15
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "exp"
    polarity -0.5
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['exp']&#10;polarity: -0.5"
  ]
  node [
    id 661
    label "253.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 253
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 253"
  ]
  node [
    id 662
    label "253.00.1.0&#10;very"
    text "very"
    shape "box"
    color "pink"
    size 12
    syn_role "advmod (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.2
    title "syn_role: advmod (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['exp']&#10;polarity: 0.2"
  ]
  node [
    id 663
    label "253.01.1.0&#10;silly"
    text "silly"
    shape "box"
    color "pink"
    size 15
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "exp"
    polarity -0.5
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['exp']&#10;polarity: -0.5"
  ]
  node [
    id 664
    label "254.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 254
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 254"
  ]
  node [
    id 665
    label "254.02.1.0&#10;silly"
    text "silly"
    shape "box"
    color "pink"
    size 15
    syn_role "acomp (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "exp"
    polarity -0.5
    title "syn_role: acomp (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['exp']&#10;polarity: -0.5"
  ]
  node [
    id 666
    label "255.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 255
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 255"
  ]
  node [
    id 667
    label "256.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 256
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 256"
  ]
  node [
    id 668
    label "256.04.1.0&#10;silly"
    text "silly"
    shape "box"
    color "pink"
    size 15
    syn_role "acomp (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "exp"
    polarity -0.5
    title "syn_role: acomp (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['exp']&#10;polarity: -0.5"
  ]
  node [
    id 669
    label "257.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 257
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 257"
  ]
  node [
    id 670
    label "258.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 258
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 258"
  ]
  node [
    id 671
    label "258.01.1.0&#10;silly"
    text "silly"
    shape "box"
    color "pink"
    size 15
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "exp"
    polarity -0.5
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['exp']&#10;polarity: -0.5"
  ]
  node [
    id 672
    label "259.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 259
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 259"
  ]
  node [
    id 673
    label "259.06.1.0&#10;Brett"
    text "Brett"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "nc"
    cats "ent"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc', 'ent']"
  ]
  node [
    id 674
    label "260.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 260
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 260"
  ]
  node [
    id 675
    label "260.00.1.0&#10;I"
    text "I"
    shape "box"
    color "yellow"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "."
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['sfs']&#10;clause_type: ."
  ]
  node [
    id 676
    label "261.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 261
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 261"
  ]
  node [
    id 677
    label "262.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 262
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 262"
  ]
  node [
    id 678
    label "262.00.1.0&#10;you"
    text "you"
    shape "box"
    color "yellow"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "?"
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['sfs']&#10;clause_type: ?"
  ]
  node [
    id 679
    label "263.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 263
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 263"
  ]
  node [
    id 680
    label "263.01.1.0&#10;wanna"
    text "wanna"
    shape "box"
    color "cyan"
    size 10
    syn_role "appos (head: nsubj)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "nc"
    title "syn_role: appos (head: nsubj)&#10;wrt_ROOT: left&#10;cats: ['nc']"
  ]
  node [
    id 681
    label "263.00.1.0&#10;you"
    text "you"
    shape "box"
    color "yellow"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "?"
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['sfs']&#10;clause_type: ?"
  ]
  node [
    id 682
    label "264.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 264
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 264"
  ]
  node [
    id 683
    label "265.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 265
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 265"
  ]
  node [
    id 684
    label "265.00.1.0&#10;finish"
    text "finish"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7420364365934291, 2, 3, 219)]"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']&#10;corels: [(0.7420364365934291, 2, 3, 219)]"
  ]
  node [
    id 685
    label "266.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 266
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 266"
  ]
  node [
    id 686
    label "266.03.1.0&#10;you"
    text "you"
    shape "box"
    color "yellow"
    size 10
    syn_role "nsubj (head: nsubj)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "?"
    title "syn_role: nsubj (head: nsubj)&#10;wrt_ROOT: right&#10;cats: ['sfs']&#10;clause_type: ?"
  ]
  node [
    id 687
    label "267.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 267
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 267"
  ]
  node [
    id 688
    label "268.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 268
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 268"
  ]
  node [
    id 689
    label "268.00.1.0&#10;okay"
    text "okay"
    shape "box"
    color "pink"
    size 15
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.5
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['exp']&#10;polarity: 0.5"
  ]
  node [
    id 690
    label "269.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 269
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 269"
  ]
  node [
    id 691
    label "270.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 270
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 270"
  ]
  node [
    id 692
    label "270.00.1.0&#10;okay"
    text "okay"
    shape "box"
    color "pink"
    size 15
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.5
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['exp']&#10;polarity: 0.5"
  ]
  node [
    id 693
    label "271.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 271
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 271"
  ]
  node [
    id 694
    label "271.02.1.0&#10;nice"
    text "nice"
    shape "box"
    color "pink"
    size 16
    syn_role "amod (head: attr)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.6
    title "syn_role: amod (head: attr)&#10;wrt_ROOT: right&#10;cats: ['exp']&#10;polarity: 0.6"
  ]
  node [
    id 695
    label "272.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 272
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 272"
  ]
  node [
    id 696
    label "272.05.2.0&#10;your birthday"
    text "your birthday"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.749783996482006, 0, 1, 265)]"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.749783996482006, 0, 1, 265)]"
  ]
  node [
    id 697
    label "272.02.1.0&#10;fun"
    text "fun"
    shape "box"
    color "pink"
    size 13
    syn_role "attr (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.3
    title "syn_role: attr (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['exp']&#10;polarity: 0.3"
  ]
  node [
    id 698
    label "273.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 273
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 273"
  ]
  node [
    id 699
    label "273.09.2.0&#10;what kind"
    text "what kind"
    shape "box"
    color "cyan"
    size 10
    syn_role "dobj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7903036876381072, 5, 2, 272)]"
    title "syn_role: dobj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['nc']&#10;corels: [(0.7903036876381072, 5, 2, 272)]"
  ]
  node [
    id 700
    label "273.12.2.0&#10;a birthday"
    text "a birthday"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7243171807466942, 7, 2, 241)]"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: left&#10;cats: ['nc']&#10;corels: [(0.7243171807466942, 7, 2, 241)]"
  ]
  node [
    id 701
    label "273.15.1.0&#10;you"
    text "you"
    shape "box"
    color "yellow"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "?"
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['sfs']&#10;clause_type: ?"
  ]
  node [
    id 702
    label "273.10.1.0&#10;kind"
    text "kind"
    shape "box"
    color "pink"
    size 16
    syn_role "dobj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.6
    title "syn_role: dobj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['exp']&#10;polarity: 0.6"
  ]
  node [
    id 703
    label "274.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 274
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 274"
  ]
  node [
    id 704
    label "274.00.3.0&#10;a fun birthday"
    text "a fun birthday"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(1.0000000264300513, 12, 2, 273)]"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']&#10;corels: [(1.0000000264300513, 12, 2, 273)]"
  ]
  node [
    id 705
    label "274.01.1.0&#10;fun"
    text "fun"
    shape "box"
    color "pink"
    size 13
    syn_role "amod (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.3
    title "syn_role: amod (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['exp']&#10;polarity: 0.3"
  ]
  node [
    id 706
    label "275.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 275
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 275"
  ]
  node [
    id 707
    label "275.00.3.0&#10;a fun birthday"
    text "a fun birthday"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(1.0000000264300513, 0, 3, 274)]"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']&#10;corels: [(1.0000000264300513, 0, 3, 274)]"
  ]
  node [
    id 708
    label "275.01.1.0&#10;fun"
    text "fun"
    shape "box"
    color "pink"
    size 13
    syn_role "amod (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.3
    title "syn_role: amod (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['exp']&#10;polarity: 0.3"
  ]
  node [
    id 709
    label "276.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 276
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 276"
  ]
  node [
    id 710
    label "276.00.2.0&#10;a fun"
    text "a fun"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7510341148367352, 9, 2, 273)]"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']&#10;corels: [(0.7510341148367352, 9, 2, 273)]"
  ]
  node [
    id 711
    label "276.06.2.0&#10;fun birthday"
    text "fun birthday"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(1.0000000264300513, 0, 3, 275)]"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']&#10;corels: [(1.0000000264300513, 0, 3, 275)]"
  ]
  node [
    id 712
    label "276.01.1.0&#10;fun"
    text "fun"
    shape "box"
    color "pink"
    size 13
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.3
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['exp']&#10;polarity: 0.3"
  ]
  node [
    id 713
    label "276.06.1.0&#10;fun"
    text "fun"
    shape "box"
    color "pink"
    size 13
    syn_role "amod (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.3
    title "syn_role: amod (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['exp']&#10;polarity: 0.3"
  ]
  node [
    id 714
    label "277.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 277
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 277"
  ]
  node [
    id 715
    label "277.07.2.0&#10;you guys"
    text "you guys"
    shape "box"
    color "cyan"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7920563411192785, 6, 2, 276)]"
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['nc']&#10;corels: [(0.7920563411192785, 6, 2, 276)]"
  ]
  node [
    id 716
    label "278.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 278
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 278"
  ]
  node [
    id 717
    label "278.22.3.0&#10;your big presents"
    text "your big presents"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7177977008418989, 0, 2, 276)]"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.7177977008418989, 0, 2, 276)]"
  ]
  node [
    id 718
    label "278.20.1.0&#10;one"
    text "one"
    shape "box"
    color "cyan"
    size 10
    syn_role "dobj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "ent"
    title "syn_role: dobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['ent']"
  ]
  node [
    id 719
    label "278.23.1.0&#10;big"
    text "big"
    shape "box"
    color "pink"
    size 10
    syn_role "amod (head: pobj)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.0
    title "syn_role: amod (head: pobj)&#10;wrt_ROOT: right&#10;cats: ['exp']&#10;polarity: 0.0"
  ]
  node [
    id 720
    label "279.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 279
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 279"
  ]
  node [
    id 721
    label "279.02.2.0&#10;your present"
    text "your present"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.999999921440576, 22, 3, 278)]"
    title "syn_role: pobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.999999921440576, 22, 3, 278)]"
  ]
  node [
    id 722
    label "279.07.2.0&#10;the pool"
    text "the pool"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.6448089374248851, 7, 2, 277)]"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.6448089374248851, 7, 2, 277)]"
  ]
  node [
    id 723
    label "279.03.1.0&#10;present"
    text "present"
    shape "box"
    color "pink"
    size 10
    syn_role "pobj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.0
    title "syn_role: pobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['exp']&#10;polarity: 0.0"
  ]
  node [
    id 724
    label "280.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 280
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 280"
  ]
  node [
    id 725
    label "280.00.2.0&#10;a boat"
    text "a boat"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7544122985919036, 2, 2, 279)]"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']&#10;corels: [(0.7544122985919036, 2, 2, 279)]"
  ]
  node [
    id 726
    label "281.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 281
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 281"
  ]
  node [
    id 727
    label "281.00.5.0&#10;yeah , a paddle boat"
    text "yeah , a paddle boat"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.9999999744495085, 0, 2, 280)]"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']&#10;corels: [(0.9999999744495085, 0, 2, 280)]"
  ]
  node [
    id 728
    label "282.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 282
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 282"
  ]
  node [
    id 729
    label "282.01.2.0&#10;a paddles"
    text "a paddles"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.8381533497054694, 0, 5, 281)]"
    title "syn_role: pobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.8381533497054694, 0, 5, 281)]"
  ]
  node [
    id 730
    label "282.04.2.0&#10;four blades"
    text "four blades"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.743463582065272, 4, 2, 241)]"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.743463582065272, 4, 2, 241)]"
  ]
  node [
    id 731
    label "282.04.1.0&#10;four"
    text "four"
    shape "box"
    color "cyan"
    size 10
    syn_role "nummod (head: pobj)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "ent"
    title "syn_role: nummod (head: pobj)&#10;wrt_ROOT: right&#10;cats: ['ent']"
  ]
  node [
    id 732
    label "283.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 283
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 283"
  ]
  node [
    id 733
    label "283.02.1.0&#10;right"
    text "right"
    shape "box"
    color "pink"
    size 13
    syn_role "acomp (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.2857142857142857
    title "syn_role: acomp (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['exp']&#10;polarity: 0.2857142857142857"
  ]
  node [
    id 734
    label "284.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 284
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 284"
  ]
  node [
    id 735
    label "284.01.2.0&#10;four paddles"
    text "four paddles"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.9999999997052595, 1, 2, 282)]"
    title "syn_role: pobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.9999999997052595, 1, 2, 282)]"
  ]
  node [
    id 736
    label "284.01.1.0&#10;four"
    text "four"
    shape "box"
    color "cyan"
    size 10
    syn_role "nummod (head: pobj)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "ent"
    title "syn_role: nummod (head: pobj)&#10;wrt_ROOT: right&#10;cats: ['ent']"
  ]
  node [
    id 737
    label "285.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 285
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 285"
  ]
  node [
    id 738
    label "285.02.1.0&#10;right"
    text "right"
    shape "box"
    color "pink"
    size 13
    syn_role "acomp (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.2857142857142857
    title "syn_role: acomp (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['exp']&#10;polarity: 0.2857142857142857"
  ]
  node [
    id 739
    label "286.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 286
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 286"
  ]
  node [
    id 740
    label "286.05.2.0&#10;that boat"
    text "that boat"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.8381533497054694, 1, 2, 284)]"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.8381533497054694, 1, 2, 284)]"
  ]
  node [
    id 741
    label "286.02.1.0&#10;you"
    text "you"
    shape "box"
    color "yellow"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "?"
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['sfs']&#10;clause_type: ?"
  ]
  node [
    id 742
    label "287.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 287
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 287"
  ]
  node [
    id 743
    label "287.00.1.0&#10;paddle"
    text "paddle"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.8347896756073537, 4, 2, 282)]"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']&#10;corels: [(0.8347896756073537, 4, 2, 282)]"
  ]
  node [
    id 744
    label "288.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 288
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 288"
  ]
  node [
    id 745
    label "288.01.2.0&#10;the paddle"
    text "the paddle"
    shape "box"
    color "cyan"
    size 10
    syn_role "dobj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(1.0000000003486598, 0, 1, 287)]"
    title "syn_role: dobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(1.0000000003486598, 0, 1, 287)]"
  ]
  node [
    id 746
    label "289.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 289
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 289"
  ]
  node [
    id 747
    label "290.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 290
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 290"
  ]
  node [
    id 748
    label "290.00.1.0&#10;play"
    text "play"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.6829053194308002, 5, 2, 286)]"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']&#10;corels: [(0.6829053194308002, 5, 2, 286)]"
  ]
  node [
    id 749
    label "291.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 291
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 291"
  ]
  node [
    id 750
    label "291.04.2.0&#10;the boat"
    text "the boat"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.8147316551287418, 1, 2, 288)]"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.8147316551287418, 1, 2, 288)]"
  ]
  node [
    id 751
    label "291.01.1.0&#10;you"
    text "you"
    shape "box"
    color "yellow"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "?"
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['sfs']&#10;clause_type: ?"
  ]
  node [
    id 752
    label "292.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 292
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 292"
  ]
  node [
    id 753
    label "293.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 293
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 293"
  ]
  node [
    id 754
    label "293.04.2.0&#10;the boat"
    text "the boat"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.9999999744495085, 4, 2, 291)]"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.9999999744495085, 4, 2, 291)]"
  ]
  node [
    id 755
    label "293.01.1.0&#10;you"
    text "you"
    shape "box"
    color "yellow"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "?"
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['sfs']&#10;clause_type: ?"
  ]
  node [
    id 756
    label "294.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 294
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 294"
  ]
  node [
    id 757
    label "295.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 295
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 295"
  ]
  node [
    id 758
    label "295.05.2.0&#10;the boat"
    text "the boat"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: prep)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.9999999744495085, 4, 2, 293)]"
    title "syn_role: pobj (head: prep)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.9999999744495085, 4, 2, 293)]"
  ]
  node [
    id 759
    label "295.02.1.0&#10;you"
    text "you"
    shape "box"
    color "yellow"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "?"
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['sfs']&#10;clause_type: ?"
  ]
  node [
    id 760
    label "296.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 296
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 296"
  ]
  node [
    id 761
    label "296.00.1.0&#10;paddle"
    text "paddle"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.8147316551287418, 5, 2, 295)]"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc']&#10;corels: [(0.8147316551287418, 5, 2, 295)]"
  ]
  node [
    id 762
    label "297.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 297
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 297"
  ]
  node [
    id 763
    label "297.00.1.0&#10;Brett"
    text "Brett"
    shape "box"
    color "cyan"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "nc"
    cats "ent"
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['nc', 'ent']"
  ]
  node [
    id 764
    label "298.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 298
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 298"
  ]
  node [
    id 765
    label "299.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 299
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 299"
  ]
  node [
    id 766
    label "299.03.1.0&#10;Brett"
    text "Brett"
    shape "box"
    color "cyan"
    size 10
    syn_role "npadvmod (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "ent"
    title "syn_role: npadvmod (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['ent']"
  ]
  node [
    id 767
    label "299.00.1.0&#10;good"
    text "good"
    shape "box"
    color "pink"
    size 17
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.7
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['exp']&#10;polarity: 0.7"
  ]
  node [
    id 768
    label "300.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 300
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 300"
  ]
  node [
    id 769
    label "301.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 301
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 301"
  ]
  node [
    id 770
    label "301.11.1.0&#10;paddles"
    text "paddles"
    shape "box"
    color "cyan"
    size 10
    syn_role "dobj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(1.0000000003486598, 0, 1, 296)]"
    title "syn_role: dobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(1.0000000003486598, 0, 1, 296)]"
  ]
  node [
    id 771
    label "301.02.1.0&#10;I"
    text "I"
    shape "box"
    color "yellow"
    size 10
    syn_role "conj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "."
    title "syn_role: conj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['sfs']&#10;clause_type: ."
  ]
  node [
    id 772
    label "301.09.1.0&#10;I"
    text "I"
    shape "box"
    color "yellow"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "."
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['sfs']&#10;clause_type: ."
  ]
  node [
    id 773
    label "301.13.1.0&#10;I"
    text "I"
    shape "box"
    color "yellow"
    size 10
    syn_role "nsubj (head: conj)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "."
    title "syn_role: nsubj (head: conj)&#10;wrt_ROOT: right&#10;cats: ['sfs']&#10;clause_type: ."
  ]
  node [
    id 774
    label "302.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 302
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 302"
  ]
  node [
    id 775
    label "302.04.3.0&#10;a smart guy"
    text "a smart guy"
    shape "box"
    color "cyan"
    size 10
    syn_role "attr (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.6835520303982937, 6, 2, 243)]"
    title "syn_role: attr (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.6835520303982937, 6, 2, 243)]"
  ]
  node [
    id 776
    label "302.05.1.0&#10;smart"
    text "smart"
    shape "box"
    color "pink"
    size 12
    syn_role "amod (head: attr)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.21428571428571427
    title "syn_role: amod (head: attr)&#10;wrt_ROOT: right&#10;cats: ['exp']&#10;polarity: 0.21428571428571427"
  ]
  node [
    id 777
    label "303.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 303
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 303"
  ]
  node [
    id 778
    label "303.04.3.0&#10;a good birthday"
    text "a good birthday"
    shape "box"
    color "cyan"
    size 10
    syn_role "dobj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7920563411192785, 4, 3, 302)]"
    title "syn_role: dobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.7920563411192785, 4, 3, 302)]"
  ]
  node [
    id 779
    label "303.05.1.0&#10;good"
    text "good"
    shape "box"
    color "pink"
    size 17
    syn_role "amod (head: dobj)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.7
    title "syn_role: amod (head: dobj)&#10;wrt_ROOT: right&#10;cats: ['exp']&#10;polarity: 0.7"
  ]
  node [
    id 780
    label "304.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 304
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 304"
  ]
  node [
    id 781
    label "304.07.3.0&#10;all you guys"
    text "all you guys"
    shape "box"
    color "cyan"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7920563411192785, 4, 3, 303)]"
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['nc']&#10;corels: [(0.7920563411192785, 4, 3, 303)]"
  ]
  node [
    id 782
    label "304.19.2.0&#10;your cake"
    text "your cake"
    shape "box"
    color "cyan"
    size 10
    syn_role "dobj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7171710795678963, 11, 1, 301)]"
    title "syn_role: dobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.7171710795678963, 11, 1, 301)]"
  ]
  node [
    id 783
    label "304.23.2.0&#10;your presents"
    text "your presents"
    shape "box"
    color "cyan"
    size 10
    syn_role "dobj (head: conj)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.6653880489886711, 7, 3, 304)]"
    title "syn_role: dobj (head: conj)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.6653880489886711, 7, 3, 304)]"
  ]
  node [
    id 784
    label "304.17.1.0&#10;you"
    text "you"
    shape "box"
    color "yellow"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "?"
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['sfs']&#10;clause_type: ?"
  ]
  node [
    id 785
    label "305.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 305
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 305"
  ]
  node [
    id 786
    label "305.02.2.0&#10;you guys"
    text "you guys"
    shape "box"
    color "cyan"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.6653880489886711, 23, 2, 304)]"
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['nc']&#10;corels: [(0.6653880489886711, 23, 2, 304)]"
  ]
  node [
    id 787
    label "305.02.1.0&#10;you"
    text "you"
    shape "box"
    color "yellow"
    size 10
    syn_role "nmod (head: nsubj)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "?"
    title "syn_role: nmod (head: nsubj)&#10;wrt_ROOT: left&#10;cats: ['sfs']&#10;clause_type: ?"
  ]
  node [
    id 788
    label "306.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 306
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 306"
  ]
  node [
    id 789
    label "306.01.2.0&#10;the store"
    text "the store"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7099841994727741, 0, 1, 290)]"
    title "syn_role: pobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.7099841994727741, 0, 1, 290)]"
  ]
  node [
    id 790
    label "307.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 307
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 307"
  ]
  node [
    id 791
    label "307.04.2.0&#10;you guys"
    text "you guys"
    shape "box"
    color "cyan"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(1.0000000382888155, 2, 2, 305)]"
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['nc']&#10;corels: [(1.0000000382888155, 2, 2, 305)]"
  ]
  node [
    id 792
    label "308.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 308
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 308"
  ]
  node [
    id 793
    label "308.08.3.0&#10;all you guys"
    text "all you guys"
    shape "box"
    color "cyan"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(1.0000000382888155, 4, 2, 307)]"
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['nc']&#10;corels: [(1.0000000382888155, 4, 2, 307)]"
  ]
  node [
    id 794
    label "308.12.1.0&#10;outside"
    text "outside"
    shape "box"
    color "pink"
    size 10
    syn_role "advmod (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.0
    title "syn_role: advmod (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['exp']&#10;polarity: 0.0"
  ]
  node [
    id 795
    label "309.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 309
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 309"
  ]
  node [
    id 796
    label "309.02.1.0&#10;you"
    text "you"
    shape "box"
    color "yellow"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "?"
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['sfs']&#10;clause_type: ?"
  ]
  node [
    id 797
    label "310.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 310
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 310"
  ]
  node [
    id 798
    label "310.01.2.0&#10;the pool"
    text "the pool"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7068687845377777, 19, 2, 304)]"
    title "syn_role: pobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.7068687845377777, 19, 2, 304)]"
  ]
  node [
    id 799
    label "311.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 311
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 311"
  ]
  node [
    id 800
    label "312.-1.0.0&#10;MOT"
    speaker "MOT"
    utterance_id 312
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 312"
  ]
  node [
    id 801
    label "313.-1.0.0&#10;CHI"
    speaker "CHI"
    utterance_id 313
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 313"
  ]
  node [
    id 802
    label "313.02.1.0&#10;love"
    text "love"
    shape "box"
    color "pink"
    size 15
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.5
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['exp']&#10;polarity: 0.5"
  ]
  node [
    id 803
    label "313.01.1.0&#10;I"
    text "I"
    shape "box"
    color "yellow"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "."
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['sfs']&#10;clause_type: ."
  ]
  edge [
    source 0
    target 1
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 0
    target 2
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 0
    target 4
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 1
    target 1
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 2
    target 3
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 4
    target 5
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 4
    target 6
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 4
    target 7
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 7
    target 8
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 7
    target 9
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 8
    target 10
    key 0
    label "corel"
    value 6
    weight 0.6736481470665314
    title "weight: 0.6736481470665314"
  ]
  edge [
    source 9
    target 10
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 9
    target 12
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 10
    target 11
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 10
    target 11
    key 1
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 10
    target 21
    key 0
    label "corel"
    value 9
    weight 0.9999999130441076
    title "weight: 0.9999999130441076"
  ]
  edge [
    source 12
    target 13
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 13
    target 14
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 13
    target 16
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 13
    target 17
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 14
    target 15
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 17
    target 18
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 17
    target 19
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 19
    target 20
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 19
    target 22
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 19
    target 24
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 20
    target 20
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 20
    target 21
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 21
    target 23
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 21
    target 23
    key 1
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 21
    target 25
    key 0
    label "corel"
    value 7
    weight 0.7559426266191352
    title "weight: 0.7559426266191352"
  ]
  edge [
    source 24
    target 25
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 24
    target 26
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 24
    target 28
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 25
    target 26
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 25
    target 27
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 25
    target 29
    key 0
    label "corel"
    value 6
    weight 0.6379101919364686
    title "weight: 0.6379101919364686"
  ]
  edge [
    source 26
    target 27
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 28
    target 29
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 28
    target 30
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 29
    target 32
    key 0
    label "corel"
    value 6
    weight 0.6736481470665314
    title "weight: 0.6736481470665314"
  ]
  edge [
    source 30
    target 31
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 30
    target 33
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 30
    target 35
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 31
    target 31
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 31
    target 32
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 32
    target 34
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 32
    target 34
    key 1
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 32
    target 37
    key 0
    label "corel"
    value 7
    weight 0.7559426266191352
    title "weight: 0.7559426266191352"
  ]
  edge [
    source 35
    target 36
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 36
    target 37
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 36
    target 39
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 36
    target 38
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 36
    target 43
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 37
    target 39
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 37
    target 40
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 37
    target 40
    key 1
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 37
    target 41
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 37
    target 42
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 37
    target 44
    key 0
    label "corel"
    value 7
    weight 0.7559426266191352
    title "weight: 0.7559426266191352"
  ]
  edge [
    source 40
    target 41
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 41
    target 42
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 43
    target 44
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 43
    target 45
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 43
    target 47
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 44
    target 46
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 44
    target 46
    key 1
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 44
    target 48
    key 0
    label "corel"
    value 9
    weight 0.9999999130441076
    title "weight: 0.9999999130441076"
  ]
  edge [
    source 47
    target 48
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 47
    target 49
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 47
    target 51
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 48
    target 50
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 48
    target 50
    key 1
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 48
    target 52
    key 0
    label "corel"
    value 9
    weight 0.9999999130441076
    title "weight: 0.9999999130441076"
  ]
  edge [
    source 51
    target 52
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 51
    target 54
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 51
    target 57
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 52
    target 56
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 52
    target 56
    key 1
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 52
    target 59
    key 0
    label "corel"
    value 7
    weight 0.7574771986438121
    title "weight: 0.7574771986438121"
  ]
  edge [
    source 54
    target 55
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 56
    target 53
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 57
    target 58
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 58
    target 59
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 58
    target 60
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 58
    target 61
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 59
    target 63
    key 0
    label "corel"
    value 6
    weight 0.609895437266936
    title "weight: 0.609895437266936"
  ]
  edge [
    source 61
    target 62
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 62
    target 63
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 62
    target 64
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 62
    target 65
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 63
    target 67
    key 0
    label "corel"
    value 6
    weight 0.6612148075920553
    title "weight: 0.6612148075920553"
  ]
  edge [
    source 65
    target 66
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 66
    target 67
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 66
    target 68
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 66
    target 69
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 67
    target 73
    key 0
    label "corel"
    value 6
    weight 0.6612148075920553
    title "weight: 0.6612148075920553"
  ]
  edge [
    source 69
    target 70
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 70
    target 71
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 70
    target 72
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 72
    target 74
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 72
    target 75
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 73
    target 80
    key 0
    label "corel"
    value 6
    weight 0.6673593283774537
    title "weight: 0.6673593283774537"
  ]
  edge [
    source 74
    target 73
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 75
    target 76
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 76
    target 77
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 77
    target 78
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 78
    target 79
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 78
    target 81
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 79
    target 80
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 79
    target 146
    key 0
    label "corel"
    value 6
    weight 0.6405473590565586
    title "weight: 0.6405473590565586"
  ]
  edge [
    source 80
    target 83
    key 0
    label "corel"
    value 6
    weight 0.622096043686025
    title "weight: 0.622096043686025"
  ]
  edge [
    source 81
    target 82
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 82
    target 83
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 82
    target 84
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 82
    target 85
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 83
    target 84
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 83
    target 88
    key 0
    label "corel"
    value 7
    weight 0.7034903316345432
    title "weight: 0.7034903316345432"
  ]
  edge [
    source 85
    target 86
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 86
    target 87
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 87
    target 88
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 87
    target 89
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 88
    target 91
    key 0
    label "corel"
    value 6
    weight 0.6620885460992072
    title "weight: 0.6620885460992072"
  ]
  edge [
    source 89
    target 90
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 89
    target 92
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 89
    target 93
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 90
    target 91
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 91
    target 94
    key 0
    label "corel"
    value 7
    weight 0.7183236082220523
    title "weight: 0.7183236082220523"
  ]
  edge [
    source 93
    target 94
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 93
    target 95
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 94
    target 96
    key 0
    label "corel"
    value 9
    weight 0.9999999882960363
    title "weight: 0.9999999882960363"
  ]
  edge [
    source 95
    target 96
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 95
    target 98
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 95
    target 99
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 96
    target 97
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 96
    target 107
    key 0
    label "corel"
    value 6
    weight 0.6236684595321675
    title "weight: 0.6236684595321675"
  ]
  edge [
    source 99
    target 100
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 100
    target 101
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 100
    target 102
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 102
    target 103
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 103
    target 104
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 104
    target 105
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 104
    target 106
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 106
    target 107
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 106
    target 108
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 107
    target 109
    key 0
    label "corel"
    value 8
    weight 0.8344216858801877
    title "weight: 0.8344216858801877"
  ]
  edge [
    source 108
    target 109
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 108
    target 110
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 109
    target 113
    key 0
    label "corel"
    value 7
    weight 0.7311073075431102
    title "weight: 0.7311073075431102"
  ]
  edge [
    source 110
    target 111
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 110
    target 112
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 112
    target 113
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 112
    target 117
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 113
    target 114
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 113
    target 114
    key 1
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 113
    target 115
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 113
    target 116
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 113
    target 119
    key 0
    label "corel"
    value 7
    weight 0.7746215455607665
    title "weight: 0.7746215455607665"
  ]
  edge [
    source 114
    target 115
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 115
    target 116
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 117
    target 118
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 118
    target 119
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 118
    target 120
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 119
    target 121
    key 0
    label "corel"
    value 7
    weight 0.7191380942504905
    title "weight: 0.7191380942504905"
  ]
  edge [
    source 120
    target 121
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 120
    target 125
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 121
    target 122
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 121
    target 122
    key 1
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 121
    target 123
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 121
    target 124
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 121
    target 126
    key 0
    label "corel"
    value 8
    weight 0.8485926386983647
    title "weight: 0.8485926386983647"
  ]
  edge [
    source 122
    target 123
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 123
    target 124
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 125
    target 127
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 125
    target 128
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 125
    target 132
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 126
    target 129
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 126
    target 129
    key 1
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 126
    target 130
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 126
    target 131
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 126
    target 136
    key 0
    label "corel"
    value 6
    weight 0.6368795676771811
    title "weight: 0.6368795676771811"
  ]
  edge [
    source 127
    target 126
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 129
    target 130
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 130
    target 131
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 132
    target 133
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 132
    target 138
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 133
    target 133
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 133
    target 134
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 134
    target 134
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 134
    target 135
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 135
    target 137
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 135
    target 137
    key 1
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 136
    target 136
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 136
    target 142
    key 0
    label "corel"
    value 6
    weight 0.6200081108404467
    title "weight: 0.6200081108404467"
  ]
  edge [
    source 137
    target 136
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 138
    target 139
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 138
    target 140
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 140
    target 141
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 141
    target 142
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 141
    target 143
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 141
    target 144
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 142
    target 145
    key 0
    label "corel"
    value 6
    weight 0.6716958699588885
    title "weight: 0.6716958699588885"
  ]
  edge [
    source 144
    target 145
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 144
    target 147
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 144
    target 151
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 145
    target 147
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 145
    target 146
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 145
    target 155
    key 0
    label "corel"
    value 7
    weight 0.7947118469379837
    title "weight: 0.7947118469379837"
  ]
  edge [
    source 146
    target 148
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 146
    target 148
    key 1
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 146
    target 149
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 146
    target 150
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 146
    target 152
    key 0
    label "corel"
    value 6
    weight 0.6900641941051103
    title "weight: 0.6900641941051103"
  ]
  edge [
    source 148
    target 149
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 149
    target 150
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 151
    target 152
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 151
    target 153
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 151
    target 154
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 152
    target 158
    key 0
    label "corel"
    value 10
    weight 1.0000000108908578
    title "weight: 1.0000000108908578"
  ]
  edge [
    source 154
    target 155
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 154
    target 156
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 154
    target 157
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 155
    target 162
    key 0
    label "corel"
    value 6
    weight 0.6521466064818977
    title "weight: 0.6521466064818977"
  ]
  edge [
    source 157
    target 158
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 157
    target 159
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 157
    target 160
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 158
    target 163
    key 0
    label "corel"
    value 6
    weight 0.6539257798908481
    title "weight: 0.6539257798908481"
  ]
  edge [
    source 160
    target 161
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 161
    target 164
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 161
    target 168
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 162
    target 163
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 162
    target 169
    key 0
    label "corel"
    value 10
    weight 1.0000000355020076
    title "weight: 1.0000000355020076"
  ]
  edge [
    source 163
    target 165
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 163
    target 165
    key 1
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 163
    target 166
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 163
    target 167
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 163
    target 170
    key 0
    label "corel"
    value 6
    weight 0.687523613856338
    title "weight: 0.687523613856338"
  ]
  edge [
    source 164
    target 162
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 165
    target 166
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 166
    target 167
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 168
    target 169
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 168
    target 171
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 169
    target 170
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 169
    target 186
    key 0
    label "corel"
    value 10
    weight 1.0000000355020076
    title "weight: 1.0000000355020076"
  ]
  edge [
    source 170
    target 174
    key 0
    label "corel"
    value 6
    weight 0.6585163387160425
    title "weight: 0.6585163387160425"
  ]
  edge [
    source 171
    target 172
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 171
    target 173
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 173
    target 174
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 173
    target 175
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 173
    target 176
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 174
    target 175
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 174
    target 187
    key 0
    label "corel"
    value 6
    weight 0.6770380472070083
    title "weight: 0.6770380472070083"
  ]
  edge [
    source 176
    target 177
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 176
    target 178
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 178
    target 179
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 178
    target 180
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 178
    target 181
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 178
    target 182
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 182
    target 183
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 182
    target 184
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 182
    target 185
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 185
    target 188
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 185
    target 189
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 185
    target 193
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 186
    target 187
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 186
    target 201
    key 0
    label "corel"
    value 6
    weight 0.6968035413370409
    title "weight: 0.6968035413370409"
  ]
  edge [
    source 187
    target 190
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 187
    target 190
    key 1
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 187
    target 191
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 187
    target 192
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 187
    target 196
    key 0
    label "corel"
    value 6
    weight 0.6854492724560142
    title "weight: 0.6854492724560142"
  ]
  edge [
    source 188
    target 186
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 190
    target 191
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 191
    target 192
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 193
    target 194
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 194
    target 195
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 195
    target 198
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 195
    target 200
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 196
    target 199
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 196
    target 199
    key 1
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 196
    target 202
    key 0
    label "corel"
    value 6
    weight 0.6854492724560142
    title "weight: 0.6854492724560142"
  ]
  edge [
    source 197
    target 197
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 198
    target 196
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 199
    target 197
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 200
    target 201
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 200
    target 207
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 201
    target 203
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 201
    target 267
    key 0
    label "corel"
    value 10
    weight 1.0000000065261785
    title "weight: 1.0000000065261785"
  ]
  edge [
    source 202
    target 204
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 202
    target 204
    key 1
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 202
    target 205
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 202
    target 206
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 202
    target 210
    key 0
    label "corel"
    value 7
    weight 0.7286955108086063
    title "weight: 0.7286955108086063"
  ]
  edge [
    source 203
    target 202
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 204
    target 205
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 205
    target 206
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 207
    target 208
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 208
    target 209
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 209
    target 210
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 209
    target 211
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 210
    target 213
    key 0
    label "corel"
    value 7
    weight 0.7067322081305525
    title "weight: 0.7067322081305525"
  ]
  edge [
    source 211
    target 212
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 212
    target 213
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 212
    target 215
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 213
    target 214
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 213
    target 214
    key 1
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 213
    target 219
    key 0
    label "corel"
    value 7
    weight 0.7937302592388699
    title "weight: 0.7937302592388699"
  ]
  edge [
    source 215
    target 216
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 216
    target 217
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 216
    target 218
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 218
    target 219
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 218
    target 223
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 219
    target 220
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 219
    target 220
    key 1
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 219
    target 221
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 219
    target 222
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 219
    target 225
    key 0
    label "corel"
    value 8
    weight 0.8328143959866809
    title "weight: 0.8328143959866809"
  ]
  edge [
    source 220
    target 221
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 221
    target 222
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 223
    target 224
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 224
    target 225
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 224
    target 227
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 224
    target 231
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 225
    target 227
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 225
    target 226
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 225
    target 252
    key 0
    label "corel"
    value 10
    weight 1.0000000039903203
    title "weight: 1.0000000039903203"
  ]
  edge [
    source 226
    target 228
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 226
    target 228
    key 1
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 226
    target 229
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 226
    target 230
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 226
    target 261
    key 0
    label "corel"
    value 10
    weight 1.0000000408776748
    title "weight: 1.0000000408776748"
  ]
  edge [
    source 228
    target 229
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 229
    target 230
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 231
    target 232
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 231
    target 233
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 233
    target 234
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 233
    target 236
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 233
    target 237
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 234
    target 235
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 237
    target 238
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 237
    target 240
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 238
    target 239
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 240
    target 241
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 240
    target 243
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 240
    target 244
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 241
    target 242
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 244
    target 245
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 244
    target 246
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 246
    target 247
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 247
    target 248
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 247
    target 249
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 248
    target 248
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 249
    target 250
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 249
    target 251
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 251
    target 252
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 251
    target 253
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 251
    target 254
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 252
    target 253
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 252
    target 255
    key 0
    label "corel"
    value 10
    weight 1.0000000039903203
    title "weight: 1.0000000039903203"
  ]
  edge [
    source 254
    target 255
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 254
    target 256
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 254
    target 257
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 255
    target 256
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 255
    target 260
    key 0
    label "corel"
    value 10
    weight 1.0000000039903203
    title "weight: 1.0000000039903203"
  ]
  edge [
    source 257
    target 258
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 257
    target 259
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 259
    target 260
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 259
    target 265
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 260
    target 261
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 260
    target 269
    key 0
    label "corel"
    value 10
    weight 1.0000000039903203
    title "weight: 1.0000000039903203"
  ]
  edge [
    source 261
    target 262
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 261
    target 262
    key 1
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 261
    target 263
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 261
    target 264
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 261
    target 270
    key 0
    label "corel"
    value 10
    weight 1.0000000408776748
    title "weight: 1.0000000408776748"
  ]
  edge [
    source 262
    target 263
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 263
    target 264
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 265
    target 266
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 266
    target 267
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 266
    target 272
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 266
    target 276
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 267
    target 271
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 267
    target 271
    key 1
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 267
    target 268
    key 0
    label "corel"
    value 10
    weight 1.0000000065261785
    title "weight: 1.0000000065261785"
  ]
  edge [
    source 268
    target 268
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 268
    target 269
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 268
    target 312
    key 0
    label "corel"
    value 10
    weight 1.0000000065261785
    title "weight: 1.0000000065261785"
  ]
  edge [
    source 269
    target 272
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 269
    target 270
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 269
    target 278
    key 0
    label "corel"
    value 7
    weight 0.7791815191548143
    title "weight: 0.7791815191548143"
  ]
  edge [
    source 270
    target 273
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 270
    target 273
    key 1
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 270
    target 274
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 270
    target 275
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 270
    target 279
    key 0
    label "corel"
    value 8
    weight 0.8328143959866809
    title "weight: 0.8328143959866809"
  ]
  edge [
    source 271
    target 268
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 273
    target 274
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 274
    target 275
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 276
    target 277
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 276
    target 280
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 276
    target 282
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 277
    target 277
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 277
    target 278
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 278
    target 279
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 278
    target 289
    key 0
    label "corel"
    value 7
    weight 0.7690394412912005
    title "weight: 0.7690394412912005"
  ]
  edge [
    source 279
    target 281
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 279
    target 288
    key 0
    label "corel"
    value 10
    weight 1.0000000039903203
    title "weight: 1.0000000039903203"
  ]
  edge [
    source 280
    target 281
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 282
    target 283
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 283
    target 284
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 284
    target 285
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 285
    target 286
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 285
    target 287
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 287
    target 288
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 287
    target 290
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 287
    target 294
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 288
    target 290
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 288
    target 289
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 288
    target 299
    key 0
    label "corel"
    value 8
    weight 0.8038481359541441
    title "weight: 0.8038481359541441"
  ]
  edge [
    source 289
    target 291
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 289
    target 291
    key 1
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 289
    target 292
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 289
    target 293
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 289
    target 304
    key 0
    label "corel"
    value 7
    weight 0.7501978936483702
    title "weight: 0.7501978936483702"
  ]
  edge [
    source 291
    target 292
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 292
    target 293
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 294
    target 295
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 294
    target 296
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 296
    target 297
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 297
    target 298
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 298
    target 299
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 298
    target 303
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 299
    target 300
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 299
    target 300
    key 1
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 299
    target 301
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 299
    target 302
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 299
    target 305
    key 0
    label "corel"
    value 7
    weight 0.7286955108086063
    title "weight: 0.7286955108086063"
  ]
  edge [
    source 300
    target 301
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 301
    target 302
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 303
    target 304
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 303
    target 306
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 304
    target 304
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 304
    target 305
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 304
    target 329
    key 0
    label "corel"
    value 7
    weight 0.7532067960964989
    title "weight: 0.7532067960964989"
  ]
  edge [
    source 305
    target 336
    key 0
    label "corel"
    value 7
    weight 0.7227535240283695
    title "weight: 0.7227535240283695"
  ]
  edge [
    source 306
    target 308
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 306
    target 309
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 307
    target 307
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 308
    target 307
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 309
    target 310
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 310
    target 311
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 311
    target 312
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 311
    target 313
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 311
    target 314
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 311
    target 315
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 312
    target 417
    key 0
    label "corel"
    value 6
    weight 0.6144812652109073
    title "weight: 0.6144812652109073"
  ]
  edge [
    source 315
    target 316
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 315
    target 318
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 316
    target 317
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 316
    target 317
    key 1
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 318
    target 319
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 318
    target 321
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 319
    target 320
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 319
    target 320
    key 1
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 321
    target 322
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 321
    target 323
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 323
    target 324
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 324
    target 325
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 324
    target 326
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 326
    target 327
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 327
    target 328
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 327
    target 330
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 328
    target 328
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 328
    target 329
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 329
    target 332
    key 0
    label "corel"
    value 8
    weight 0.8338516962585786
    title "weight: 0.8338516962585786"
  ]
  edge [
    source 330
    target 331
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 331
    target 332
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 331
    target 333
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 332
    target 340
    key 0
    label "corel"
    value 8
    weight 0.8338516962585786
    title "weight: 0.8338516962585786"
  ]
  edge [
    source 333
    target 334
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 333
    target 337
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 334
    target 334
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 334
    target 335
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 335
    target 335
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 335
    target 336
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 336
    target 353
    key 0
    label "corel"
    value 7
    weight 0.745925671392336
    title "weight: 0.745925671392336"
  ]
  edge [
    source 337
    target 338
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 338
    target 339
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 339
    target 340
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 339
    target 341
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 340
    target 342
    key 0
    label "corel"
    value 10
    weight 1.0000000228299875
    title "weight: 1.0000000228299875"
  ]
  edge [
    source 341
    target 342
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 341
    target 343
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 342
    target 362
    key 0
    label "corel"
    value 6
    weight 0.6644786263819696
    title "weight: 0.6644786263819696"
  ]
  edge [
    source 343
    target 344
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 343
    target 345
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 345
    target 346
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 346
    target 347
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 346
    target 348
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 348
    target 349
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 348
    target 350
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 349
    target 349
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 349
    target 407
    key 0
    label "corel"
    value 7
    weight 0.75726330990877
    title "weight: 0.75726330990877"
  ]
  edge [
    source 350
    target 351
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 351
    target 352
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 351
    target 354
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 351
    target 355
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 352
    target 352
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 352
    target 353
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 353
    target 359
    key 0
    label "corel"
    value 7
    weight 0.7462066792039541
    title "weight: 0.7462066792039541"
  ]
  edge [
    source 355
    target 356
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 356
    target 357
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 356
    target 358
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 358
    target 360
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 358
    target 361
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 359
    target 365
    key 0
    label "corel"
    value 7
    weight 0.7881016684269865
    title "weight: 0.7881016684269865"
  ]
  edge [
    source 360
    target 359
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 361
    target 362
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 361
    target 363
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 362
    target 364
    key 0
    label "corel"
    value 7
    weight 0.7120459235817346
    title "weight: 0.7120459235817346"
  ]
  edge [
    source 363
    target 364
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 363
    target 366
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 363
    target 370
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 364
    target 365
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 364
    target 385
    key 0
    label "corel"
    value 7
    weight 0.7181777270575451
    title "weight: 0.7181777270575451"
  ]
  edge [
    source 365
    target 367
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 365
    target 367
    key 1
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 365
    target 368
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 365
    target 369
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 365
    target 371
    key 0
    label "corel"
    value 6
    weight 0.6985503623814711
    title "weight: 0.6985503623814711"
  ]
  edge [
    source 367
    target 368
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 368
    target 369
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 370
    target 371
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 370
    target 372
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 370
    target 373
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 371
    target 387
    key 0
    label "corel"
    value 7
    weight 0.79462309361351
    title "weight: 0.79462309361351"
  ]
  edge [
    source 373
    target 374
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 373
    target 375
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 375
    target 376
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 375
    target 377
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 377
    target 378
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 377
    target 380
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 377
    target 381
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 378
    target 379
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 378
    target 379
    key 1
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 378
    target 380
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 381
    target 382
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 382
    target 383
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 382
    target 384
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 383
    target 383
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 384
    target 385
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 384
    target 386
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 385
    target 404
    key 0
    label "corel"
    value 7
    weight 0.7181777270575451
    title "weight: 0.7181777270575451"
  ]
  edge [
    source 386
    target 387
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 386
    target 388
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 386
    target 389
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 387
    target 390
    key 0
    label "corel"
    value 10
    weight 1.0000000108908578
    title "weight: 1.0000000108908578"
  ]
  edge [
    source 389
    target 390
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 389
    target 391
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 389
    target 392
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 390
    target 394
    key 0
    label "corel"
    value 6
    weight 0.6878199499499585
    title "weight: 0.6878199499499585"
  ]
  edge [
    source 392
    target 393
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 393
    target 394
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 393
    target 395
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 394
    target 399
    key 0
    label "corel"
    value 6
    weight 0.6937727092056006
    title "weight: 0.6937727092056006"
  ]
  edge [
    source 395
    target 396
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 396
    target 397
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 396
    target 398
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 397
    target 397
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 398
    target 399
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 398
    target 400
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 398
    target 401
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 399
    target 400
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 399
    target 415
    key 0
    label "corel"
    value 7
    weight 0.7249968713714251
    title "weight: 0.7249968713714251"
  ]
  edge [
    source 401
    target 402
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 401
    target 403
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 402
    target 402
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 403
    target 404
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 403
    target 405
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 403
    target 406
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 404
    target 411
    key 0
    label "corel"
    value 9
    weight 0.9999999503230271
    title "weight: 0.9999999503230271"
  ]
  edge [
    source 406
    target 408
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 406
    target 409
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 407
    target 416
    key 0
    label "corel"
    value 6
    weight 0.6343954541710531
    title "weight: 0.6343954541710531"
  ]
  edge [
    source 408
    target 407
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 409
    target 410
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 410
    target 411
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 410
    target 412
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 411
    target 414
    key 0
    label "corel"
    value 9
    weight 0.9999999503230271
    title "weight: 0.9999999503230271"
  ]
  edge [
    source 412
    target 413
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 413
    target 414
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 413
    target 418
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 413
    target 425
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 414
    target 415
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 414
    target 519
    key 0
    label "corel"
    value 6
    weight 0.6669985743584493
    title "weight: 0.6669985743584493"
  ]
  edge [
    source 415
    target 419
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 415
    target 419
    key 1
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 415
    target 420
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 415
    target 421
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 415
    target 428
    key 0
    label "corel"
    value 7
    weight 0.7301424255687371
    title "weight: 0.7301424255687371"
  ]
  edge [
    source 416
    target 417
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 416
    target 426
    key 0
    label "corel"
    value 7
    weight 0.7899236587012705
    title "weight: 0.7899236587012705"
  ]
  edge [
    source 417
    target 418
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 417
    target 422
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 417
    target 422
    key 1
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 417
    target 423
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 417
    target 424
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 417
    target 518
    key 0
    label "corel"
    value 7
    weight 0.7293779545329983
    title "weight: 0.7293779545329983"
  ]
  edge [
    source 419
    target 420
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 420
    target 421
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 421
    target 416
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 422
    target 423
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 423
    target 424
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 425
    target 426
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 425
    target 427
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 426
    target 430
    key 0
    label "corel"
    value 7
    weight 0.7364813133955704
    title "weight: 0.7364813133955704"
  ]
  edge [
    source 427
    target 428
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 427
    target 429
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 428
    target 508
    key 0
    label "corel"
    value 10
    weight 1.0000000634262258
    title "weight: 1.0000000634262258"
  ]
  edge [
    source 429
    target 430
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 429
    target 431
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 430
    target 432
    key 0
    label "corel"
    value 7
    weight 0.73761591270814
    title "weight: 0.73761591270814"
  ]
  edge [
    source 431
    target 432
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 431
    target 433
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 432
    target 444
    key 0
    label "corel"
    value 9
    weight 0.9999999254605505
    title "weight: 0.9999999254605505"
  ]
  edge [
    source 433
    target 434
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 433
    target 435
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 435
    target 436
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 435
    target 437
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 436
    target 436
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 437
    target 438
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 437
    target 439
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 437
    target 440
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 440
    target 441
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 440
    target 442
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 442
    target 443
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 442
    target 445
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 443
    target 444
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 444
    target 458
    key 0
    label "corel"
    value 7
    weight 0.747925663873226
    title "weight: 0.747925663873226"
  ]
  edge [
    source 445
    target 446
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 445
    target 447
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 447
    target 448
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 447
    target 451
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 448
    target 450
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 448
    target 450
    key 1
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 450
    target 449
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 451
    target 452
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 451
    target 453
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 453
    target 454
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 453
    target 455
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 455
    target 456
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 455
    target 457
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 457
    target 458
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 457
    target 459
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 457
    target 460
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 458
    target 458
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 458
    target 459
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 458
    target 461
    key 0
    label "corel"
    value 7
    weight 0.747925663873226
    title "weight: 0.747925663873226"
  ]
  edge [
    source 460
    target 461
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 460
    target 463
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 461
    target 462
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 461
    target 462
    key 1
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 461
    target 464
    key 0
    label "corel"
    value 7
    weight 0.747925663873226
    title "weight: 0.747925663873226"
  ]
  edge [
    source 463
    target 464
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 463
    target 465
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 463
    target 466
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 464
    target 464
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 464
    target 465
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 464
    target 483
    key 0
    label "corel"
    value 7
    weight 0.7054817713468109
    title "weight: 0.7054817713468109"
  ]
  edge [
    source 466
    target 467
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 466
    target 468
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 468
    target 469
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 468
    target 470
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 470
    target 471
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 470
    target 472
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 472
    target 473
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 472
    target 476
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 473
    target 475
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 473
    target 475
    key 1
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 475
    target 474
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 476
    target 477
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 476
    target 478
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 477
    target 477
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 478
    target 481
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 478
    target 482
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 479
    target 481
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 479
    target 480
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 481
    target 479
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 482
    target 483
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 482
    target 484
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 483
    target 485
    key 0
    label "corel"
    value 9
    weight 0.999999921440576
    title "weight: 0.999999921440576"
  ]
  edge [
    source 484
    target 485
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 484
    target 486
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 485
    target 487
    key 0
    label "corel"
    value 9
    weight 0.999999921440576
    title "weight: 0.999999921440576"
  ]
  edge [
    source 486
    target 487
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 486
    target 488
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 487
    target 490
    key 0
    label "corel"
    value 7
    weight 0.7388969951104069
    title "weight: 0.7388969951104069"
  ]
  edge [
    source 488
    target 491
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 488
    target 492
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 488
    target 493
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 489
    target 491
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 489
    target 490
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 490
    target 492
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 490
    target 498
    key 0
    label "corel"
    value 7
    weight 0.7388969951104069
    title "weight: 0.7388969951104069"
  ]
  edge [
    source 491
    target 489
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 493
    target 494
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 493
    target 497
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 494
    target 496
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 494
    target 496
    key 1
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 496
    target 495
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 497
    target 498
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 497
    target 499
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 498
    target 500
    key 0
    label "corel"
    value 9
    weight 0.999999921440576
    title "weight: 0.999999921440576"
  ]
  edge [
    source 499
    target 500
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 499
    target 501
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 500
    target 502
    key 0
    label "corel"
    value 9
    weight 0.999999921440576
    title "weight: 0.999999921440576"
  ]
  edge [
    source 501
    target 502
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 501
    target 503
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 502
    target 504
    key 0
    label "corel"
    value 9
    weight 0.999999921440576
    title "weight: 0.999999921440576"
  ]
  edge [
    source 503
    target 504
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 503
    target 505
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 504
    target 513
    key 0
    label "corel"
    value 6
    weight 0.6659714575585227
    title "weight: 0.6659714575585227"
  ]
  edge [
    source 505
    target 506
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 506
    target 507
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 507
    target 508
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 507
    target 509
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 507
    target 510
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 508
    target 515
    key 0
    label "corel"
    value 10
    weight 1.0000000634262258
    title "weight: 1.0000000634262258"
  ]
  edge [
    source 510
    target 511
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 511
    target 512
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 512
    target 513
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 512
    target 514
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 513
    target 523
    key 0
    label "corel"
    value 7
    weight 0.7714392218124476
    title "weight: 0.7714392218124476"
  ]
  edge [
    source 514
    target 515
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 514
    target 516
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 515
    target 530
    key 0
    label "corel"
    value 10
    weight 1.0000000634262258
    title "weight: 1.0000000634262258"
  ]
  edge [
    source 516
    target 517
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 517
    target 518
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 517
    target 521
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 517
    target 520
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 517
    target 522
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 518
    target 521
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 518
    target 519
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 518
    target 546
    key 0
    label "corel"
    value 7
    weight 0.7665873673805427
    title "weight: 0.7665873673805427"
  ]
  edge [
    source 519
    target 526
    key 0
    label "corel"
    value 7
    weight 0.7879314387847862
    title "weight: 0.7879314387847862"
  ]
  edge [
    source 522
    target 524
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 522
    target 525
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 523
    target 588
    key 0
    label "corel"
    value 7
    weight 0.7558324851970443
    title "weight: 0.7558324851970443"
  ]
  edge [
    source 524
    target 523
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 525
    target 526
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 525
    target 528
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 525
    target 527
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 525
    target 529
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 526
    target 528
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 526
    target 534
    key 0
    label "corel"
    value 7
    weight 0.7879314387847862
    title "weight: 0.7879314387847862"
  ]
  edge [
    source 529
    target 530
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 529
    target 531
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 529
    target 532
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 529
    target 533
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 530
    target 552
    key 0
    label "corel"
    value 6
    weight 0.6908767432737982
    title "weight: 0.6908767432737982"
  ]
  edge [
    source 533
    target 534
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 533
    target 536
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 533
    target 535
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 533
    target 537
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 534
    target 536
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 534
    target 538
    key 0
    label "corel"
    value 7
    weight 0.7141621038665686
    title "weight: 0.7141621038665686"
  ]
  edge [
    source 537
    target 539
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 537
    target 540
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 537
    target 544
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 538
    target 541
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 538
    target 542
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 538
    target 542
    key 1
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 538
    target 543
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 538
    target 563
    key 0
    label "corel"
    value 7
    weight 0.7259285242755035
    title "weight: 0.7259285242755035"
  ]
  edge [
    source 539
    target 538
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 542
    target 543
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 543
    target 541
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 544
    target 545
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 545
    target 546
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 545
    target 547
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 545
    target 548
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 546
    target 547
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 546
    target 549
    key 0
    label "corel"
    value 9
    weight 0.999999921440576
    title "weight: 0.999999921440576"
  ]
  edge [
    source 548
    target 549
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 548
    target 550
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 548
    target 551
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 549
    target 550
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 549
    target 566
    key 0
    label "corel"
    value 7
    weight 0.7203428179501424
    title "weight: 0.7203428179501424"
  ]
  edge [
    source 551
    target 552
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 551
    target 553
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 551
    target 554
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 552
    target 553
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 552
    target 555
    key 0
    label "corel"
    value 7
    weight 0.7188526397901721
    title "weight: 0.7188526397901721"
  ]
  edge [
    source 554
    target 556
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 554
    target 557
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 554
    target 561
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 555
    target 558
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 555
    target 558
    key 1
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 555
    target 559
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 555
    target 560
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 555
    target 568
    key 0
    label "corel"
    value 6
    weight 0.6883309052864174
    title "weight: 0.6883309052864174"
  ]
  edge [
    source 556
    target 555
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 558
    target 559
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 559
    target 560
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 561
    target 562
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 562
    target 563
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 562
    target 564
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 563
    target 574
    key 0
    label "corel"
    value 9
    weight 0.9999999890893256
    title "weight: 0.9999999890893256"
  ]
  edge [
    source 564
    target 565
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 565
    target 566
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 565
    target 567
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 566
    target 579
    key 0
    label "corel"
    value 7
    weight 0.7721263892863034
    title "weight: 0.7721263892863034"
  ]
  edge [
    source 567
    target 568
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 567
    target 569
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 568
    target 570
    key 0
    label "corel"
    value 8
    weight 0.8708349174028114
    title "weight: 0.8708349174028114"
  ]
  edge [
    source 569
    target 570
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 569
    target 571
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 570
    target 575
    key 0
    label "corel"
    value 7
    weight 0.7586774426944894
    title "weight: 0.7586774426944894"
  ]
  edge [
    source 571
    target 572
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 571
    target 573
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 573
    target 574
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 573
    target 576
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 574
    target 575
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 574
    target 608
    key 0
    label "corel"
    value 6
    weight 0.6903315745086769
    title "weight: 0.6903315745086769"
  ]
  edge [
    source 575
    target 577
    key 0
    label "corel"
    value 9
    weight 0.9999999485566696
    title "weight: 0.9999999485566696"
  ]
  edge [
    source 576
    target 577
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 576
    target 578
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 577
    target 607
    key 0
    label "corel"
    value 7
    weight 0.7416302160985657
    title "weight: 0.7416302160985657"
  ]
  edge [
    source 578
    target 579
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 578
    target 580
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 578
    target 581
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 579
    target 580
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 579
    target 585
    key 0
    label "corel"
    value 7
    weight 0.7665873673805427
    title "weight: 0.7665873673805427"
  ]
  edge [
    source 581
    target 582
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 581
    target 583
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 583
    target 584
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 584
    target 585
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 584
    target 586
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 584
    target 587
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 585
    target 586
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 585
    target 684
    key 0
    label "corel"
    value 7
    weight 0.7420364365934291
    title "weight: 0.7420364365934291"
  ]
  edge [
    source 587
    target 589
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 587
    target 590
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 587
    target 592
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 588
    target 595
    key 0
    label "corel"
    value 7
    weight 0.7803112885867443
    title "weight: 0.7803112885867443"
  ]
  edge [
    source 589
    target 588
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 590
    target 591
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 592
    target 593
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 592
    target 594
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 594
    target 595
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 594
    target 596
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 595
    target 606
    key 0
    label "corel"
    value 7
    weight 0.7374209937059609
    title "weight: 0.7374209937059609"
  ]
  edge [
    source 596
    target 597
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 597
    target 598
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 598
    target 599
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 599
    target 600
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 600
    target 601
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 601
    target 602
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 602
    target 603
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 603
    target 604
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 604
    target 605
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 605
    target 609
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 605
    target 610
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 605
    target 611
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 605
    target 618
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 606
    target 607
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 606
    target 619
    key 0
    label "corel"
    value 9
    weight 0.999999990594579
    title "weight: 0.999999990594579"
  ]
  edge [
    source 607
    target 612
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 607
    target 612
    key 1
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 607
    target 613
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 607
    target 614
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 607
    target 622
    key 0
    label "corel"
    value 7
    weight 0.7635083280109909
    title "weight: 0.7635083280109909"
  ]
  edge [
    source 608
    target 615
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 608
    target 615
    key 1
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 608
    target 616
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 608
    target 617
    key 0
    label "kb_world"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 608
    target 635
    key 0
    label "corel"
    value 7
    weight 0.7434162955231911
    title "weight: 0.7434162955231911"
  ]
  edge [
    source 609
    target 606
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 612
    target 613
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 613
    target 614
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 614
    target 608
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 615
    target 616
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 616
    target 617
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 618
    target 619
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 618
    target 620
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 619
    target 621
    key 0
    label "corel"
    value 7
    weight 0.783857705574777
    title "weight: 0.783857705574777"
  ]
  edge [
    source 620
    target 621
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 620
    target 623
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 620
    target 624
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 621
    target 622
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 621
    target 625
    key 0
    label "corel"
    value 7
    weight 0.7339134470620996
    title "weight: 0.7339134470620996"
  ]
  edge [
    source 622
    target 634
    key 0
    label "corel"
    value 7
    weight 0.7658322569721702
    title "weight: 0.7658322569721702"
  ]
  edge [
    source 624
    target 625
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 624
    target 626
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 625
    target 645
    key 0
    label "corel"
    value 7
    weight 0.7330836214201605
    title "weight: 0.7330836214201605"
  ]
  edge [
    source 626
    target 627
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 627
    target 628
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 627
    target 629
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 628
    target 628
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 629
    target 630
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 629
    target 631
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 631
    target 632
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 631
    target 633
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 632
    target 632
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 633
    target 634
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 633
    target 636
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 633
    target 637
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 633
    target 638
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 634
    target 635
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 634
    target 639
    key 0
    label "corel"
    value 10
    weight 1.0000000573255683
    title "weight: 1.0000000573255683"
  ]
  edge [
    source 635
    target 640
    key 0
    label "corel"
    value 8
    weight 0.8687678099653074
    title "weight: 0.8687678099653074"
  ]
  edge [
    source 638
    target 639
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 638
    target 641
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 638
    target 642
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 639
    target 640
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 639
    target 730
    key 0
    label "corel"
    value 7
    weight 0.743463582065272
    title "weight: 0.743463582065272"
  ]
  edge [
    source 640
    target 700
    key 0
    label "corel"
    value 7
    weight 0.7243171807466942
    title "weight: 0.7243171807466942"
  ]
  edge [
    source 642
    target 643
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 642
    target 644
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 644
    target 645
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 644
    target 646
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 645
    target 775
    key 0
    label "corel"
    value 6
    weight 0.6835520303982937
    title "weight: 0.6835520303982937"
  ]
  edge [
    source 646
    target 647
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 646
    target 648
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 648
    target 649
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 649
    target 650
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 650
    target 651
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 650
    target 652
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 650
    target 653
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 653
    target 654
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 653
    target 655
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 655
    target 656
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 656
    target 657
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 657
    target 658
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 657
    target 659
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 659
    target 660
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 659
    target 661
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 661
    target 662
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 661
    target 664
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 662
    target 663
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 664
    target 665
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 664
    target 666
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 666
    target 667
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 667
    target 668
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 667
    target 669
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 669
    target 670
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 670
    target 671
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 670
    target 672
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 672
    target 673
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 672
    target 674
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 673
    target 673
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 674
    target 675
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 674
    target 676
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 676
    target 677
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 677
    target 678
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 677
    target 679
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 679
    target 680
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 679
    target 681
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 679
    target 682
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 682
    target 683
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 683
    target 684
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 683
    target 685
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 684
    target 696
    key 0
    label "corel"
    value 7
    weight 0.749783996482006
    title "weight: 0.749783996482006"
  ]
  edge [
    source 685
    target 686
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 685
    target 687
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 687
    target 688
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 688
    target 689
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 688
    target 690
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 690
    target 691
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 691
    target 692
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 691
    target 693
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 693
    target 694
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 693
    target 695
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 695
    target 696
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 695
    target 697
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 695
    target 698
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 696
    target 699
    key 0
    label "corel"
    value 7
    weight 0.7903036876381072
    title "weight: 0.7903036876381072"
  ]
  edge [
    source 698
    target 699
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 698
    target 702
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 698
    target 701
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 698
    target 703
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 699
    target 702
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 699
    target 700
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 699
    target 710
    key 0
    label "corel"
    value 7
    weight 0.7510341148367352
    title "weight: 0.7510341148367352"
  ]
  edge [
    source 700
    target 704
    key 0
    label "corel"
    value 10
    weight 1.0000000264300513
    title "weight: 1.0000000264300513"
  ]
  edge [
    source 703
    target 704
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 703
    target 705
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 703
    target 706
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 704
    target 705
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 704
    target 707
    key 0
    label "corel"
    value 10
    weight 1.0000000264300513
    title "weight: 1.0000000264300513"
  ]
  edge [
    source 706
    target 707
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 706
    target 708
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 706
    target 709
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 707
    target 708
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 707
    target 711
    key 0
    label "corel"
    value 10
    weight 1.0000000264300513
    title "weight: 1.0000000264300513"
  ]
  edge [
    source 709
    target 710
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 709
    target 712
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 709
    target 714
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 710
    target 712
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 710
    target 711
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 710
    target 717
    key 0
    label "corel"
    value 7
    weight 0.7177977008418989
    title "weight: 0.7177977008418989"
  ]
  edge [
    source 711
    target 713
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 711
    target 715
    key 0
    label "corel"
    value 7
    weight 0.7920563411192785
    title "weight: 0.7920563411192785"
  ]
  edge [
    source 712
    target 713
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 714
    target 715
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 714
    target 716
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 715
    target 722
    key 0
    label "corel"
    value 6
    weight 0.6448089374248851
    title "weight: 0.6448089374248851"
  ]
  edge [
    source 716
    target 718
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 716
    target 719
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 716
    target 720
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 717
    target 719
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 717
    target 721
    key 0
    label "corel"
    value 9
    weight 0.999999921440576
    title "weight: 0.999999921440576"
  ]
  edge [
    source 718
    target 717
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 720
    target 721
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 720
    target 723
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 720
    target 724
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 721
    target 723
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 721
    target 722
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 721
    target 725
    key 0
    label "corel"
    value 7
    weight 0.7544122985919036
    title "weight: 0.7544122985919036"
  ]
  edge [
    source 724
    target 725
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 724
    target 726
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 725
    target 727
    key 0
    label "corel"
    value 9
    weight 0.9999999744495085
    title "weight: 0.9999999744495085"
  ]
  edge [
    source 726
    target 727
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 726
    target 728
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 727
    target 729
    key 0
    label "corel"
    value 8
    weight 0.8381533497054694
    title "weight: 0.8381533497054694"
  ]
  edge [
    source 728
    target 729
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 728
    target 732
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 729
    target 731
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 729
    target 735
    key 0
    label "corel"
    value 9
    weight 0.9999999997052595
    title "weight: 0.9999999997052595"
  ]
  edge [
    source 730
    target 731
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 730
    target 743
    key 0
    label "corel"
    value 8
    weight 0.8347896756073537
    title "weight: 0.8347896756073537"
  ]
  edge [
    source 731
    target 730
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 732
    target 733
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 732
    target 734
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 734
    target 736
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 734
    target 737
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 735
    target 736
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 735
    target 740
    key 0
    label "corel"
    value 8
    weight 0.8381533497054694
    title "weight: 0.8381533497054694"
  ]
  edge [
    source 736
    target 735
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 737
    target 738
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 737
    target 739
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 739
    target 740
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 739
    target 741
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 739
    target 742
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 740
    target 748
    key 0
    label "corel"
    value 6
    weight 0.6829053194308002
    title "weight: 0.6829053194308002"
  ]
  edge [
    source 742
    target 743
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 742
    target 744
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 743
    target 745
    key 0
    label "corel"
    value 10
    weight 1.0000000003486598
    title "weight: 1.0000000003486598"
  ]
  edge [
    source 744
    target 745
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 744
    target 746
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 745
    target 750
    key 0
    label "corel"
    value 8
    weight 0.8147316551287418
    title "weight: 0.8147316551287418"
  ]
  edge [
    source 746
    target 747
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 747
    target 748
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 747
    target 749
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 748
    target 789
    key 0
    label "corel"
    value 7
    weight 0.7099841994727741
    title "weight: 0.7099841994727741"
  ]
  edge [
    source 749
    target 750
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 749
    target 751
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 749
    target 752
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 750
    target 754
    key 0
    label "corel"
    value 9
    weight 0.9999999744495085
    title "weight: 0.9999999744495085"
  ]
  edge [
    source 752
    target 753
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 753
    target 754
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 753
    target 755
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 753
    target 756
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 754
    target 758
    key 0
    label "corel"
    value 9
    weight 0.9999999744495085
    title "weight: 0.9999999744495085"
  ]
  edge [
    source 756
    target 757
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 757
    target 758
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 757
    target 759
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 757
    target 760
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 758
    target 761
    key 0
    label "corel"
    value 8
    weight 0.8147316551287418
    title "weight: 0.8147316551287418"
  ]
  edge [
    source 760
    target 761
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 760
    target 762
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 761
    target 770
    key 0
    label "corel"
    value 10
    weight 1.0000000003486598
    title "weight: 1.0000000003486598"
  ]
  edge [
    source 762
    target 763
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 762
    target 764
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 763
    target 763
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 764
    target 765
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 765
    target 766
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 765
    target 767
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 765
    target 768
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 768
    target 769
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 769
    target 770
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 769
    target 771
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 769
    target 774
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 770
    target 782
    key 0
    label "corel"
    value 7
    weight 0.7171710795678963
    title "weight: 0.7171710795678963"
  ]
  edge [
    source 771
    target 772
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 772
    target 773
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 774
    target 775
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 774
    target 776
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 774
    target 777
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 775
    target 776
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 775
    target 778
    key 0
    label "corel"
    value 7
    weight 0.7920563411192785
    title "weight: 0.7920563411192785"
  ]
  edge [
    source 777
    target 778
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 777
    target 779
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 777
    target 780
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 778
    target 779
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 778
    target 781
    key 0
    label "corel"
    value 7
    weight 0.7920563411192785
    title "weight: 0.7920563411192785"
  ]
  edge [
    source 780
    target 781
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 780
    target 784
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 780
    target 785
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 781
    target 782
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 781
    target 783
    key 0
    label "corel"
    value 6
    weight 0.6653880489886711
    title "weight: 0.6653880489886711"
  ]
  edge [
    source 782
    target 783
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 782
    target 798
    key 0
    label "corel"
    value 7
    weight 0.7068687845377777
    title "weight: 0.7068687845377777"
  ]
  edge [
    source 783
    target 786
    key 0
    label "corel"
    value 6
    weight 0.6653880489886711
    title "weight: 0.6653880489886711"
  ]
  edge [
    source 785
    target 786
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 785
    target 787
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 785
    target 788
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 786
    target 791
    key 0
    label "corel"
    value 10
    weight 1.0000000382888155
    title "weight: 1.0000000382888155"
  ]
  edge [
    source 788
    target 789
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 788
    target 790
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 790
    target 791
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 790
    target 792
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 791
    target 793
    key 0
    label "corel"
    value 10
    weight 1.0000000382888155
    title "weight: 1.0000000382888155"
  ]
  edge [
    source 792
    target 793
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 792
    target 794
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 792
    target 795
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 795
    target 796
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 795
    target 797
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 797
    target 798
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 797
    target 799
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 799
    target 800
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 800
    target 801
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 801
    target 802
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 801
    target 803
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
]
