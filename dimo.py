print('Dimo')

"""
reference: https://github.com/wadetb/eliza

repository: https://gitlab.com/alexluu/dimo
"""


#%%
import logging
import structlog
import orjson
# import random
from collections import defaultdict
from dimo_spacy import *
from dimo_networkx import *
from dimo_salience import *
# from wolframclient.language import wl
from wolframclient.evaluation import WolframLanguageSession
from time import gmtime, strftime

time_format = "%Y-%m-%d-%H-%M-%S"

# obsidian://open?vault=Obsidian%20Vault&file=dissertation%2FYAML
import yaml

# with open('dialog_system.yml',encoding='utf8') as f:
#     yaml_content = list(yaml.safe_load_all(f))

# https://github.com/luutuntin/Cinderella/blob/master/semaland_utils.py
# https://docs.python.org/3.7/library/pickle.html
# https://stackoverflow.com/questions/56562929/modulenotfounderror-no-module-named-cpickle-on-python-3-7-3
# from pickle import dump, load, HIGHEST_PROTOCOL
import json

# def save_data(data, pklfile_name):
#     """ save data to a pickle file """        
#     with open(pklfile_name,'wb') as f:
#         dump(data,f,HIGHEST_PROTOCOL)

# def load_data(pklfile_name):
#     """ load data from a pickle file """        
#     with open(pklfile_name,'rb') as f:
#         data = load(f)
#     return data

setup_spacy_ds()

core_model = "en_core_web_sm"
transformer = "en_trf_distilbertbaseuncased_lg"

nlp, nlpt = setup_spacy_models(core_model, transformer)


# https://www.structlog.org/en/stable/performance.html
# https://bitestreams.com/blog/structured_logging/
# https://www.structlog.org/en/stable/logging-best-practices.html
# https://www.structlog.org/en/stable/api.html
def configure_structlog(f):
    structlog.configure(
        cache_logger_on_first_use=True,
        wrapper_class=structlog.make_filtering_bound_logger(logging.DEBUG), # development
        # wrapper_class=structlog.make_filtering_bound_logger(logging.INFO), # production
        processors=[
            structlog.contextvars.merge_contextvars,
            structlog.processors.add_log_level,
            structlog.processors.format_exc_info,
            structlog.processors.TimeStamper(fmt="iso", utc=True),
            structlog.processors.JSONRenderer(serializer=orjson.dumps),
        ],
        logger_factory=structlog.BytesLoggerFactory(f),
    )


# handle TypeError: keys must be str, int, float, bool or None, not tuple)
# https://stackoverflow.com/questions/7001606/json-serialize-a-dictionary-with-tuples-as-key
def make_memory_compatible_with_json(m):
    """
    m: memory of an instance of Dimo
    """
    for k in m:
        m[k] = {str(k): v for k, v in m[k].items()}

#%%
class Dimo:
    # def __init__(self, log, script_path, nlp, nlpt, mode):
    def __init__(self, log, script_path, mode):
        # dialog system's convenient name
        # hardcode
        self.name = "Dimo" # Di(alog) mo(del)

        # synchronous Wolfram session
        self.wolfram = None

        # log
        self.log = log

        # script
        self.script_path = script_path
        self.hyperparameters = dict()
        self.insight = dict()
        self.focus = dict()
        self.action = dict()

        # NLP
        self.nlp = nlp
        self.nlpt = nlpt

        # mode: 'listen'/'talk'
        self.mode = mode              

        # conversational context
        self.spacy = defaultdict(list) # keys: cached, cached_corel, docs
        self.networkx = nx.MultiDiGraph(last_utterance=None)
        # https://dl.acm.org/doi/pdf/10.1145/365153.365168 - page 41, columns 1-2
        # https://aclanthology.org/2022.acl-srw.14.pdf - epistemically/informationally relevant
        self.memory = defaultdict(dict) # keys: kb_world_unused, kb_world_used, kb_ling_unused, kb_ling_used

        # natural language output
        self.utterances = list() # of dicts whose keys are 'speaker', 'action' (obligatory), 'insight', 'focus', 'creativity' (optional)

        self.n_sents = int() # number of cached preceding sents
        self.threshold_mod = float() # threshold for modified content
        self.threshold_cor = float() # threshold for corels
        self.top_k = int() # top-k corels
        self.threshold_aff = float()
        self.syn_role_weights = dict()
        self.edge_weights = dict()
        self.node_indexes = dict()


    # YAML
    def _load(self):
        """
        ...
        """
        # obsidian://open?vault=Obsidian%20Vault&file=dissertation%2FYAML
        with open(self.script_path,encoding='utf8') as f:
            script = list(yaml.safe_load_all(f))
            self.hyperparameters = script[0]
            self.n_sents = self.hyperparameters['cache_size']
            self.threshold_cor = self.hyperparameters['informational']
            # HARDCODE
            self.threshold_mod = self.threshold_cor/1.2
            self.top_k = self.hyperparameters['max_num_info']
            self.threshold_aff = self.hyperparameters['affective']
            self.syn_role_weights = self.hyperparameters['salience_weights']['syn_role']
            self.edge_weights = self.hyperparameters['salience_weights']['edge']
            self.node_indexes = self.hyperparameters['node_indexes']
            self.insight = script[1]
            self.focus = script[2]
            # self.creativity = script[3]
            # self.action = script[4]
            self.action = script[3]

    def _clear(self):
        """
        ...
        """
        # conversational context
        self.spacy = defaultdict(list) # keys: cached, cached_corel, docs
        # self.networkx = None
        self.networkx = nx.MultiDiGraph(last_utterance=None)
        self.memory = defaultdict(dict)

        # natural language output
        self.utterances = list() # of dicts whose keys are 'speaker', 'action' (obligatory), 'insight', 'focus', 'creativity' (optional)


    def _insight(self, n, u, self_mode, s):
        """
        n: str of speaker name
        u: str of speaker utterance
        self_mode: True/False
        s: a Wolfram session
        """
        self.utterances.append(
            {
                'speaker': n,
                'action': u
            }
        )

        # update self.spacy
        doc = get_spacy_utterance(len(self.utterances)-1, n, u, self.nlp, self.nlpt)

        for s in doc.sents:
            # list of noun chunks in cached preceding sents
            pncs = get_preceding_ncs(self.spacy['cached'])
            
            for w in s:
                if w._.wn_polysemous != 'n/a':
                    if w._.wn_polysemous == 'y' and pncs:
                        # modify vectors of polysemous word (according to WordNet)
                        # HARDCODE
                        modified_content = get_modified_content(w, pncs, self.threshold_mod, self.nlpt)
                        if modified_content:
                            w._.modified_content = modified_content
                            self.log.debug(f'Modified: {w}, {w.i}, {w.sent}')
            
                    # get contextual WordNet sense
                    v = get_vector(w)
                    w._.wn_sense = get_wordnet_sense(v, w._.wn_senses)

            for nc in s.noun_chunks:
                if nc.root._.wn_polysemous != 'n/a':
                    # set corels
                    set_corels(nc.root, self.threshold_cor, self.top_k, self.spacy['cached_corel'])

                if nc._.bare_np:
                    bare_np = doc[nc._.bare_np[0]:nc._.bare_np[1]]
                    if bare_np._.wn_polysemous != 'n/a': # if bare_np is wn_np
                        if bare_np._.wn_polysemous == 'y' and pncs:
                            # modify vectors of polysemous wn_np (according to WordNet)
                            # HARDCODE
                            modified_content = get_modified_content(bare_np, pncs, self.threshold_mod, self.nlpt)
                            if modified_content:
                                bare_np._.modified_content = modified_content
                                self.log.debug(f'Modified: {bare_np}, ({bare_np.start}, {bare_np.end}), {bare_np.sent}')

                        # get contextual WordNet sense 
                        v = get_vector(bare_np)
                        bare_np._.wn_sense = get_wordnet_sense(v, bare_np._.wn_senses)
            

            # cached preceding sents
            temp = get_preceding_sent(s)
            get_preceding_sents(self.n_sents, self.spacy['cached'], temp)

            # candidate noun chunks for forward-looking corels
            get_notyetforwardlooking_nc(self.spacy['cached_corel'], s)


        self.spacy['docs'].append(doc)

        # update self.networkx
        self.networkx, insight = update_contextual_graph(self.networkx, doc, self.spacy['docs'], self.wolfram, self.nlpt, self.log, self.memory, self.edge_weights, self.insight, self.node_indexes)


        if self_mode: # computer is Self
            uid = len(self.utterances)

            self.utterances.append(
                {
                    'speaker': self.name,
                    'insight': insight
                }
            )
            self.log.debug(f'Insight: {insight}')
            
            return uid # utterance index in self.utterances
        
        self.utterances[-1]['insight'] = insight

        return None

    def _focus(self, uid):
        """
        uid: int of utterance index in self.utterances
        """
        # dimo_salience.py, self.threshold_aff
        self.utterances[uid]['focus'] = get_focus(self.utterances[uid]['insight'], self.networkx, self.threshold_aff, self.syn_role_weights, self.focus)
        self.log.debug(f"Focus: {self.utterances[uid]['focus']}")
        return uid

    def _action(self, uid):
        """
        uid: int of utterance index in self.utterances
        """
        action = get_action(self.utterances[uid]['focus'], self.spacy['docs'][uid-1], self.networkx, self.action, self.memory, self.node_indexes)

        action['id'] = uid
        action['speaker'] = self.name

        self.utterances[uid]['action'] = action

        # update self.spacy, self.networkx
        # reference: get_spacy_utterance() in dimo_spacy.py
        self.spacy['docs'].append(action) # skip 'cached' and 'cached_corel'
        self.networkx = update_contextual_graph(self.networkx, action, self.spacy['docs'], self.wolfram, self.nlpt, self.log, self.memory, self.edge_weights, self.insight, self.node_indexes)
        
        self.log.debug(f"Action: {self.utterances[uid]['action']}")

        return uid

    def _meaning_modeling(self, n, u, self_mode, s):
        """
        meaning modeling includes insight, focus, creativity, action
        n: str of speaker name
        u: str of speaker utterance
        self_mode: True if computer is self
        s: a Wolfram session
        """

        # insight
        uid = self._insight(n, u, self_mode, s)
        

        if uid: # create response
            # focus
            self._focus(uid)

            # action
            self._action(uid)

            return uid
        
        return uid


    def _respond(self, n, u, e, s):
        """
        n: str of user name
        u: str of user utterance
        e: str of explanation mode ("y"/"n")
        s: a Wolfram session
        """
        if u.lower().strip() in self.hyperparameters['quit']:
            return 0

        if u.lower().strip() in self.hyperparameters['clear']:
            self._finish(n, self.name)
            self._clear()
            return 1

        if u.strip() == self.hyperparameters['update_this_script']:
            self._load()
            return 2
        
        # computer is self
        self_mode = True

        uid = self._meaning_modeling(n, u, self_mode, s)
        

        if e.lower().strip() == 'y': # with explicit explanation
            output = ' '.join(f"{v['text']}" for k, v in self.utterances[uid].items() if k != 'speaker')
        else:
            output = self.utterances[uid]['action']['text']        
        
        return output

    def _finish(self, speaker1, speaker2):
        """
        speaker1/speaker2: speaker name (str)
        """
        # end time (str) - TODO: take into account structlog's time
        t = strftime(time_format, gmtime())

        # file prefix (str)
        p = f"{speaker1}_{speaker2}_{t}"

        # https://stackoverflow.com/questions/4617034/how-can-i-open-multiple-files-using-with-open-in-python
        # https://stackoverflow.com/questions/32957708/python-pickle-error-unicodedecodeerror
        with open(f'{p}_utterances.json','w',encoding='utf8') as f1, open(f'{p}_memory.json','w',encoding='utf8') as f2:
        # open(f'{p}_spacy','wb') as f3        
            # save to JSON
            # https://docs.python.org/3.7/library/json.html
            # https://stackoverflow.com/questions/18337407/saving-utf-8-texts-with-json-dumps-as-utf-8-not-as-a-u-escape-sequence
            json.dump(self.utterances, f1, ensure_ascii=False)
            make_memory_compatible_with_json(self.memory)
            json.dump(self.memory, f2, ensure_ascii=False)
            # https://v2.spacy.io/usage/saving-loading#pickle
            # dump(self.spacy['docs'], f3, HIGHEST_PROTOCOL) # This doesn't work
            # https://networkx.org/documentation/networkx-2.6.2/reference/readwrite/index.html
            # https://networkx.org/documentation/networkx-2.6.2/reference/readwrite/gpickle.html
            # https://stackoverflow.com/questions/64846222/store-networkx-graph-object
            # dump(self.networkx, f3, HIGHEST_PROTOCOL) # this is not human-readable
        
        spacy2folia(self.spacy['docs'], f'{p}_spacy')

        # https://networkx.org/documentation/networkx-2.6.2/reference/readwrite/generated/networkx.readwrite.gml.write_gml.html
        # print(self.networkx)
        if self.networkx:
            nx.write_gml(self.networkx, f'{p}_networkx.gml')
            visualize(self.networkx, f'{p}_networkx')

        # # https://networkx.org/documentation/networkx-2.6.2/reference/readwrite/generated/networkx.readwrite.graphml.write_graphml.html
        # if self.networkx:
        #     # nx.write_graphml(self.networkx, f'{p}_networkx.graphml')
        #     nx.write_graphml_lxml(self.networkx, f'{p}_networkx.graphml') # faster (see c:\Users\luutuntin\Anaconda3\envs\dimo\lib\site-packages\networkx\readwrite\graphml.py) -> KeyError line 762 - due to restricted types (lines 388-427), right?

        print('Your dialog is all set.')

    def _get_transcript(self, file_name):
        """
        file_name: name of transcript file (.txt file - format of each line: [speaker]\t[utterance])
        """
        with open(file_name, encoding='utf-8') as f:
            transcript = [l[:-1].split('\t') for l in f.readlines()]
            speakers = list(set(u[0] for u in transcript))
        
        return transcript, speakers

    def _update(self, u, s):
        """
        u: transcript utterance - tuple of (speaker, utterance text)
        s: a Wolfram session
        """
        # reference: _respond()
        self_mode = False

        self._meaning_modeling(u[0], u[1], self_mode, s)

    
    # https://github.com/luutuntin/Cinderella/blob/master/semaland_dialog_system.py - dialog_manager()
    def run(self):
        print("Please be patient as the system setup takes time. Thank you.")
        with WolframLanguageSession() as s:
            print(s)
            self.wolfram = s
            self._load()
            if mode == 'talk':
                user_name = input("What's your name? " )
                explanation = input("Do you want to see explicit explanation of the computer's responses (y/n)? ")

                speaker1 = user_name
                speaker2 = self.name
                
                while True:

                    user_utterance = input(f'{user_name}: ')
                    # print(f'{user_name}: {user_utterance}')

                    output = self._respond(user_name, user_utterance, explanation, s)

                    if output == 0: # user quits
                        break

                    if output == 1: # user renews conversation
                        explanation = input("Do you want to see explicit explanation of the computer's responses (y/n)? ")
                        continue

                    if output == 2: # user reloads script
                        continue

                    print(f'{self.name}: {output}')

            else: # mode = 'listen'
                transcript_file = input("Please provide the name of the transcript file: ") # .txt file - format of each line: [speaker]\t[utterance_text]

                transcript, speakers = self._get_transcript(transcript_file)

                if len(speakers) != 2:
                    print('This is not a dialog between two speakers.')
                
                else:
                    print('This is a dialog between two speakers.')
                    speaker1 = speakers[0]
                    speaker2 = speakers[1]

                    for u in transcript:
                        output = self._update(u, s)

            self.wolfram = None
        
        self._finish(speaker1, speaker2)

        return (self.utterances, self.spacy, self.networkx, self.memory)


# def main(log_path, script_path, nlp, nlpt, mode):
def main(log_path, script_path, mode):
    """
    log_path: name path of the log file, e.g. "dimo.log"
    """
    # https://docs.python.org/3/library/io.html#binary-i-o
    # https://docs.python.org/3.7/tutorial/inputoutput.html#reading-and-writing-files
    # https://dev.to/importox/how-to-write-in-an-existing-file-in-python-af4
    # https://www.geeksforgeeks.org/how-to-keep-old-content-when-writing-to-files-in-python/
    # with open(logfile, "wb") as f:
    with open(log_path, "ab") as f:
        configure_structlog(f)
        # the meaning of "__name__" is explained in:
        # https://rollbar.com/blog/logging-in-python/
        # https://stackoverflow.com/questions/50714316/how-to-use-logging-getlogger-name-in-multiple-modules
        # -> use the name of the Python module which the logger belongs to
        # https://www.structlog.org/en/stable/performance.html
        log = structlog.getLogger()
        # dimo = Dimo(log, script_path, nlp, nlpt, mode)
        dimo = Dimo(log, script_path, mode)        
        output = dimo.run()
    return output

#%%
if __name__ == '__main__':
    log_path = 'dimo_20230620.log'
    script_path = 'dialog_system.yml'
    mode = 'listen'
    # mode = 'talk'
    # output = main(log_path, script_path, nlp, nlpt, mode)
    output = main(log_path, script_path, mode)
    # print(output)
