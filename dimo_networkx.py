print('Dimo - NetworkX')

"""
References:
+ https://github.com/luutuntin/Cinderella/blob/master/semaland_amr_graph.py (permanent link: https://github.com/luutuntin/Cinderella/blob/c3730f464c7c7f7deea6bf28d04702c711cb474a/semaland_amr_graph.py)
AMRGraph()

+ https://github.com/luutuntin/Cinderella/blob/master/semaland_linking_concept_definition.py (permanent link: https://github.com/luutuntin/Cinderella/blob/c3730f464c7c7f7deea6bf28d04702c711cb474a/semaland_linking_concept_definition.py)
add_sent_id()

+ https://github.com/luutuntin/Cinderella/blob/master/semaland_anaphora_resolution.py (permanent link: https://github.com/luutuntin/Cinderella/blob/c3730f464c7c7f7deea6bf28d04702c711cb474a/semaland_anaphora_resolution.py)
nx.shortest_path_length()

+ https://github.com/luutuntin/Cinderella/blob/master/semaland_sent_graph_merging.py (permanent link: https://github.com/luutuntin/Cinderella/blob/c3730f464c7c7f7deea6bf28d04702c711cb474a/semaland_sent_graph_merging.py)
merge_id_nodes()
draw_graph()
"""

#%%
import networkx as nx
from dimo_wolfram import *
import spacy


# HARDCODE
# # length of index str of utterance
# # utterance is spaCy Doc
# len_idx_u = 2
# # length of index str of 1st word token
# # word token is spaCy Token
# len_idx_w = 2
# # length of index str represent phrase length
# # phrase is spaCy Span
# len_idx_p = 1
# # length of index str of related concepts in KB
# len_idx_n = 1
# parameters_idx = (2,2,1,1) # min of the second parameter is 2 (to accommodate utterance root node whose first word token index is -1)
def get_parameters_idx(x):
    """
    x: nodes_indexes of an instance of Dimo
    """
    return (x['utterance'], x['start'], x['length'], x['extra'])

def get_node_idx(i0, i1, i2, i3, parameters_idx):
    """
    i0, i1, i2, i3: idx of utterance, 1st word token, phrase length, related concepts respectively (int)
    parameters_idx: tuple of predefined lengths of these indexes
    -> str of node idx of NetWorkX graph
    """
    return '.'.join([str(i).zfill(l) for i,l in zip((i0, i1, i2, i3), parameters_idx)])


# https://www.w3schools.com/tags/ref_colornames.asp
color_map = {
    # 'ut': 'gray',
    'ut': 'lightgray',
    'inf': 'cyan',
    'aff': 'pink',
    'nor': 'yellow'
}

# https://visjs.github.io/vis-network/docs/network/nodes.html
# https://www.youtube.com/watch?v=-8GEpZrpcEU&list=PL2VXyKi-KpYu7djT-8bDxtylvxznz3WLR&index=6
shape = 'box'

def get_node(x, nt, parameters_idx):
    """
    TO CONSIDER: add utterance id as another parameter of this function for simplification
    x: depends on nt (node type) 
    nt: node type
    - 'ut' (utterance root - speaker info)
    - 'inf' (informational) - cyan
    - 'aff' (affective) - pink
    - 'nor' (normative) - yellow

    -> tuple of (node label, dict of node attributes)
    """
    if nt=='ut':
        # x: utterance - spaCy Doc / dict (action of Self)
        if isinstance(x, spacy.tokens.Doc):
            uid = x._.id
            speaker = x._.speaker
        else:
            uid = x['id']
            speaker = x['speaker']

        id = get_node_idx(uid, -1, 0, 0, parameters_idx)

        return (
            # node label
            f'{id}\n{speaker}',
            {
                'id': id,
                'speaker': speaker,
                'utterance_id': uid, # unique str for utterance root node
                'shape': shape,
                # https://www.w3schools.com/tags/ref_colornames.asp
                'color': color_map[nt],
                # HARDCODE - default value of pyvis
                'size': 10
            }
        )
    
    else:
        # x: tuple of (spaCy Span (noun chunks, entities, WordNet noun phrase) or Token (sentiment analysis), syn-role, info-status, cat)
        if isinstance(x[0], spacy.tokens.Span):
            i0 = x[0].doc._.id
            i1 = x[0].start
            i2 = len(x[0])
            text = x[0].text
        elif isinstance(x[0], spacy.tokens.Token):
            i0 = x[0].doc._.id
            i1 = x[0].i
            i2 = 1
            text = x[0].text
        else: # emphasized alignment in imperative
            # hardcode
            i0 = x[0][0]
            i1 = x[0][1]
            i2 = x[0][2]
            text = str()

        id = get_node_idx(i0, i1, i2, 0, parameters_idx)

        attributes = {
            'id': id,
            'text': text,
            'shape': shape,
            'color': color_map[nt],
            'size': x[4],
            # 'syn-role': x[1], # NetworkXError: 'syn-role' is not a valid key (https://networkx.org/documentation/networkx-2.6.2/tutorial.html#adding-attributes-to-graphs-nodes-and-edges)
            'syn_role': x[1],
            # 'info-status': x[2],
            'wrt_ROOT': x[2],
            'cats': [x[3]] # list
            # 'title' will be added when other attrs are ready
        }

        if nt=='aff':
            attributes['polarity'] = x[5]
        elif nt=='nor':
            attributes['clause_type'] = x[5]
        elif 'wn_np' in attributes['cats']:
            attributes['wn_sense'] = x[0]._.wn_sense
            attributes['definition'] = x[0]._.wn_senses[attributes['wn_sense']][0]
        
        return (
            # node label
            f'{id}\n{text}',
            attributes
        )


def add_horizontal_edges(d, nodes, g):
    """
    d: str of socio-cultural dimension (e.g. 'INF'/'AFF'/'NOR')
    nodes: list of nodes belonging to d
    g: NetWorkX graph
    -> add edges to g
    """
    if nodes: # not an empty list
        g.add_edge(g.graph['root'], nodes[0], label=d, value=1, weight=0.1, title='weight: 0.1')
        if len(nodes)>1:
            edges = [(nodes[i], nodes[i+1]) for i in range(len(nodes)-1)]
            # HARDCODE
            g.add_edges_from(edges, label='hor', value=1, weight=0.1, title='weight: 0.1')


def add_children(p, nodes, g, w):
    """
    p: label of parent node
    nodes: list of children nodes of p (tuples of (node label, dict of node attributes)
    g: NetWorkX graph
    w: Dimo.hyperparameters['salience_weights']['edge']['child']
    -> add children nodes to g and 'child' edges from p to children nodes
    """
    ### handle cases in which the label of a child node is the same as p
    cats = g.nodes[p]['cats']

    for i in range(len(nodes)):
        # nodes[i][0]: label of node nodes[i]
        if nodes[i][0]==p:            
            # last = i
            cats.extend(nodes[i][1]['cats'])

    # https://networkx.org/documentation/networkx-2.6.2/tutorial.html
    g.add_nodes_from(nodes)

    if len(cats)>1:        
        g.nodes[p]['cats'] = cats
    edges = [(p, n[0]) for n in nodes]
    # HARDCODE
    g.add_edges_from(edges, label='child', value=2, weight=w, title=f'weight: {w}')


def get_corel_info(s):
    """
    s: spaCy Span
    -> list of tuples of (weight, antecedent's start index, antecedent's length, index of doc of antecedent)
    """
    if s.root._.corel_backward:
        # assumption: 'span' in s.root._.corel_backward
        return s.root._.corel_backward['span']


def check_parent_span_plus(x, type, node, u, nodes, nodes_children, parameters_idx):
    """
    x: WordNet noun phrase/entity/sentiment word
    type: 'wn_np'/'ent'/'exp'
    node: output of get_node(x, ...)
    u: utterance - spaCy Doc
    nodes/nodes_children: dict of nodes/nodes_children (see get_utterance_graph)
    -> check parent span of x and more
    """
    if x._.parent_span:
        parent = u[x._.parent_span[0]: x._.parent_span[1]]

        parent_label = f'{get_node_idx(u._.id, parent.start, len(parent), 0, parameters_idx)}\n{parent.text}'

        if parent_label in nodes:
            if parent_label not in nodes_children:
                nodes_children[parent_label] = {
                    node[0]: node[1]
                }
            else:
                if node[0] not in nodes_children[parent_label]:
                    nodes_children[parent_label][node[0]] = node[1]
                else:
                    nodes_children[parent_label][node[0]]['cats'].append(type)

                    for k in node[1]: # attributes
                        if k not in nodes_children[parent_label][node[0]]:
                            nodes_children[parent_label][node[0]][k] = node[1][k]
    else:
        if node[0] not in nodes:
            nodes[node[0]] = node[1]
        else:
            nodes[node[0]]['cats'].append(type)


excluded_attrs = {'id', 'speaker', 'text', 'entity_name', 'entity_type', 'shape', 'color', 'size', 'certainty_type'}
def get_node_title(n, g):
    """
    n: node label
    g: graph
    """
    # https://networkx.org/documentation/networkx-2.6.2/tutorial.html#node-attributes
    title = '\n'.join([
        f"{k}: {v}" for (k, v) in g.nodes.data()[n].items() if k not in excluded_attrs
    ])
    
    g.nodes[n]['title'] = title


def check_kb_world(s, u, nc, nc_node_label, nodes_kb_world, nlpt, parameters_idx):
    """
    s: synchronous Wolfram session
    u: spaCy Doc
    nc: spaCy noun chunk
    nc_node_label: the first element of output of get_node(nc, ...)
    nodes_kb_world: dict of nodes of kb_world (key: nc node label, value: list of nodes of kb_world - tuples of (node label, dict of node attributes))
    -> list of tuples of (node label, dict of node attributes)
    """
    # get related Wolfram entities
    ent_info = get_ent_info(s, nc.text, u._.transformer[nc.start: nc.end], nlpt)
    # example of ent_info:
    # {'Wolfram expression': 'Entity["Shipwreck", "RMSTitanic::5d778"]',
    #  'entity name': 'RMS Titanic',
    #  'selected entity type': 'shipwreck',
    #  'entity type comment': 'Actually, RMS Titanic can be a historical event, shipwreck, ship, movie or word.',
    #  'source': 'Wikipedia',
    #  'related entities with source': [{'property': 'ship',
    #    'value': 'RMS Titanic',
    #    'source': 'Wikipedia',
    #    'entity': 'Entity["Ship", "RMSTitanic::5d778"]'}]}

    if ent_info:
        i0 = u._.id
        i1 = nc.start
        i2 = len(nc)

        id = get_node_idx(i0, i1, i2, 1, parameters_idx)
        entity_name = ent_info['entity name']
        entity_type = ent_info['selected entity type']

        nodes_kb_world[nc_node_label] = [(
            f"{id}\n{entity_name} ({entity_type})",
            {
                'id': id,
                'entity_name': entity_name,
                'entity_type': entity_type,
                'shape': shape,
                # HARDCODE - informational dimension
                'color': color_map['inf'],
                # hardcode - default value o pyvis
                'size': 10,
                'Wolfram_expression': ent_info['Wolfram expression'],
                'comment': ent_info['entity type comment'],
                'source': ent_info['source'],
                # HARDCODE
                'certainty_type': 'quite_certain' if 'can be a' in ent_info['entity type comment'] else 'highly_certain'
            }
        )]

        for i in range(len(ent_info['related entities with source'])):
            e = ent_info['related entities with source'][i]

            id = get_node_idx(i0, i1, i2, i+2, parameters_idx)
            entity_name = e['value']
            entity_type = e['property']

            nodes_kb_world[nc_node_label].append((
                f"{id}\n{entity_name} ({entity_type})",
                {
                    'id': id,
                    'entity_name': entity_name,
                    'entity_type': entity_type,
                    'shape': shape,
                    # HARDCODE - informational dimension
                    'color': color_map['inf'],
                    # hardcode - default value o pyvis
                    'size': 10,
                    'Wolfram_expression': e['entity'],
                    'comment': e['related entity comment'],
                    'source': e['source'],
                    'certainty_type': 'absolutely_certain'
                }
            ))

def add_kb_world(nc_node_label, nodes, g, m, w):
    """    
    nc_node_label: label of nc node
    nodes: list of kb_world nodes of nc (tuples of (node label, dict of node attributes))
    g: NetWorkX graph of an utterance
    m: memory of an instance of Dimo
    w: Dimo.hyperparameters['salience_weights']['edge']['kb_world']
    -> add kb_world nodes to g and 'kb_world' edges from nc_node_label to kb_world nodes
    """    
    g.add_nodes_from(nodes)
    edges = [(nc_node_label, n[0]) for n in nodes]
    # HARDCODE
    g.add_edges_from(edges, label='kb_world', value=2, weight=w, title=f'weight: {w}')

    # update memory
    for n in nodes:
        # n[1]: dict of node attributes
        temp = (n[1]['entity_name'], n[1]['entity_type'])
        if temp in m['kb_world_used']:
            m['kb_world_used'][temp].append(n[1]['id'])
        elif temp not in m['kb_world_unused']:
            m['kb_world_unused'][temp] = [n[1]['id']]
        else:
            m['kb_world_unused'][temp].append(n[1]['id'])


def get_position_wrt_sent_root(w):
    """
    w: spaCy Token
    -> 'left'/'right'/'same' wrt the dependency root of w.sent
    """
    # dependency root of w.sent (sent root)
    sr = w.sent.root # sr.dep_ is "ROOT"
    if w.i < sr.i:
        return 'left'
    elif w.i == sr.i:
        return 'same'
    else:
        return 'right'


def get_insight_text(i, s):
    """
    i: insight dict
    s: insight script dict
    """
    temp = list()
    if i['informational']:
        temp.append(s['informational'].format(', '.join([f"'{x[1]}'" for x in i['informational']])))
    if i['affective']:
        temp.append(s['affective'].format(', '.join([f"'{x[1]}'" for x in i['affective']])))
    if i['normative']:
        temp.append(s['normative'].format(', '.join([f"'{x[1]}' ('{x[2]}')" for x in i['normative']])))

    return f"[{' '.join(temp)}]"


def get_utterance_graph(u, s, nlpt, memory, edge_weights, insight_script, parameters_idx):
    """
    u: spaCy Doc
    s: synchronous Wolfram session
    nlpt: spacy-transformers model
    memory: memory of an instance of Dimo (dict)
    edge_weights: edge_weights of an instance of Dimo (dict)
    insight_script: YAML insight script (self.insight of Dimo class)

    https://docs.python.org/3.7/tutorial/inputoutput.html#formatted-string-literals
    https://docs.python.org/3.7/reference/lexical_analysis.html#f-strings
    https://docs.python.org/3.7/library/stdtypes.html#str.zfill

    Node label: f'{node index}: {node repr}'
    - node index: get_node_idx
    - node repr:
        - utterance root: speaker
        - others: text

    Edge label types:
    # three socio-cultural dimensions
    - INF
    - AFF
    - NOR
    # between linguistic units within an utterance
    - linear: horizonal (intra-utterance) vs vertical (inter-utterance)
    # coherence relation
    - corel
    # knowledge bases
    - kb_ling (WordNet)
    - kb_world (Wolfram)

    Node attributes: label, title (hover - including all attributes except for label and color), color (cyan: INF, pink: AFF, yellow: NOR, root: black), syn-role (based on dependency structure), info-status (given, new), cat (speaker, nc, wn_np, ent, exp (instance of expressive language)), sfs (social focus shift))
    sfs:
        – emphasized Self positioning: the target utterance is a declarative or imperative which involves the first person pronoun “I” or “me”
        – emphasized alignment: the target utterance is an interrogative which involves second person pronoun “you” or an imperative
    
    Edge attributes: label, value (normalized weight for repr purpose), title (hover - including all attributes except for label and value), weight (e.g. for corel edges)
    
    """
    # add utterance root node - speaker
    ut = get_node(u,'ut', parameters_idx)
    # label of this node
    l = ut[0]
    # initiate 'nodes' dict (key: node label, value: dict of node attributes)
    nodes = {l: ut[1]}

    # non-str attributes will be converted into str after their updating
    g = nx.MultiDiGraph(root=l, corel_nodes=list()) # multi: different socio-cultural dimensions, Di: forward-looking (backward-looking)

    nodes_kb_world = dict() # see check_kb_world()

    nodes_children = dict() # see check_paren_span_plus()
    
    # reference: jupyter_dimo_spacy.py - spacy2folia
    # noun chunks whose root has wn sense(s)
    # REPEATED CONDITIONS (see dimo_spacy.py)
    selected_ncs = [nc for nc in u.noun_chunks if nc.root._.wn_polysemous != 'n/a' or nc.root.pos_ in {'PROPN'}] # not including pronouns

    # for nc in u.noun_chunks:
    for nc in selected_ncs:
        # HARDCODE - node size: 10 (the default value of pyvis)
        temp = get_node((nc, f"{nc.root.dep_} (head: {nc.root.head.dep_})", get_position_wrt_sent_root(nc.root), 'nc', 10), 'inf', parameters_idx)
        nodes[temp[0]] = temp[1]
        corel_info = get_corel_info(nc)
        if corel_info:
            # add attribute 'corels' to this node
            # nodes[temp[0]]['corels'] = corel_info
            nodes[temp[0]]['corels'] = str(corel_info) # NetworkXError: (0.8916567935541966, 6, 3, 0) is not a string (c:\Users\luutuntin\Anaconda3\envs\dimo\lib\site-packages\networkx\readwrite\gml.py "Without specifying a `stringizer`/`destringizer`, the code is capable of writing `int`/`float`/`str`/`dict`/`list` data as required by the GML specification.  For writing other data types, and for reading data other than `str` you need to explicitly supply a `stringizer`/`destringizer`.")
            # add label of this node to attribute 'corel_nodes' of g
            g.graph['corel_nodes'].append(temp[0])

        # add related Wolfram entities to nodes_kb_world
        check_kb_world(s, u, nc, temp[0], nodes_kb_world, nlpt, parameters_idx)

    # key: corel_nodes, value: []
    # c:\Users\luutuntin\Anaconda3\envs\dimo\lib\site-packages\networkx\readwrite\gml.py in stringize(key, value, ignored_keys, indent, in_list)
    #     744                 if not isinstance(value, str):
    # --> 745                     raise NetworkXError(f"{value!r} is not a string")
    # NetworkXError: [] is not a string
    # print(f"graph attribute 'corel_nodes': {g.graph['corel_nodes']}")
    g.graph['corel_nodes'] = str(g.graph['corel_nodes'])

    for wn_np in u._.wn_nps:
        # wn_np: tuple of (index of the corresponding noun chunk, index of the first token, len of compound) 
        span =u[wn_np[1]: wn_np[2]]

        # HARDCODE - node size: 10 (the default value of pyvis)
        node = get_node((span, f"{span.root.dep_} (head: {span.root.head.dep_})", get_position_wrt_sent_root(span.root), 'wn_np', 10), 'inf', parameters_idx)

        check_parent_span_plus(span, 'wn_np', node, u, nodes, nodes_children, parameters_idx)

        # add wn_np into memory['kb_ling_unused']
        # node[1]: dict of node attributes
        temp = (span.text, span._.wn_sense)
        if temp in memory['kb_ling_used']:
            memory['kb_ling_used'][temp].append(node[1]['id'])
        elif temp not in memory['kb_ling_unused']:
            memory['kb_ling_unused'][temp] = [node[1]['id']]
        else:
            memory['kb_ling_unused'][temp].append(node[1]['id'])
    
    for e in u.ents:
        # HARDCODE - node size: 10 (the default value of pyvis)
        node = get_node((e, f"{e.root.dep_} (head: {e.root.head.dep_})", get_position_wrt_sent_root(e.root), 'ent', 10), 'inf', parameters_idx)
        check_parent_span_plus(e, 'ent', node, u, nodes, nodes_children, parameters_idx)

    for w in u:
        sentiment_assessments = w._.sentiment.assessments
        if sentiment_assessments:
            # https://textblob.readthedocs.io/en/latest/quickstart.html#sentiment-analysis
            polarity = w._.sentiment.polarity

            # HARDCODE
            node = get_node((w, f"{w.dep_} (head: {w.head.dep_})", get_position_wrt_sent_root(w), "exp", 10 + round(abs(w._.sentiment.polarity)*10), polarity), 'aff', parameters_idx)

            check_parent_span_plus(w, 'exp', node, u, nodes, nodes_children, parameters_idx)

    # NOR
    for s in u.sents:
        if s._.social_foci:
            for i in s._.social_foci:
                if i==-1: # imperative
                    # hardcode
                    # # HARDCODE - node size: 10 (the default value of pyvis)
                    node = get_node(((u._.id, s.start, 0), 'n/a', 'n/a', 'sfs', 10, '!'), 'nor', parameters_idx)
                else:
                    # HARDCODE - node size: 10 (the default value of pyvis)
                    node = get_node((u[i], f"{u[i].dep_} (head: {u[i].head.dep_})", get_position_wrt_sent_root(u[i]), 'sfs', 10, s._.clause_type), 'nor', parameters_idx)
                nodes[node[0]] = node[1]


    g.add_nodes_from(nodes.items())

    insight = {
        'informational': [(k, v['text']) for (k, v) in sorted(nodes.items()) if v['color']=='cyan'],
        'affective': [(k, v['text'], v['polarity']) for (k, v) in sorted(nodes.items()) if v['color']=='pink'],
        'normative': [(k, v['text'], v['clause_type']) for (k, v) in sorted(nodes.items()) if v['color']=='yellow']
    }
    insight['text'] = get_insight_text(insight, insight_script)

    for k in nodes_children:
        add_children(k, list(nodes_children[k].items()), g, edge_weights['child'])

    for k in nodes_kb_world:
        add_kb_world(k, nodes_kb_world[k], g, memory, edge_weights['kb_world'])    

    # https://networkx.org/documentation/networkx-2.6.2/reference/classes/multidigraph.html
    # https://networkx.org/documentation/networkx-2.6.2/reference/introduction.html#graph-reporting
    # https://networkx.org/documentation/networkx-2.6.2/tutorial.html#adding-attributes-to-graphs-nodes-and-edges
    inf = sorted([n for n in g if g.nodes[n]['color']=='cyan'])
    aff = sorted([n for n in g if g.nodes[n]['color']=='pink'])
    nor = sorted([n for n in g if g.nodes[n]['color']=='yellow'])
    add_horizontal_edges('INF', inf, g)
    add_horizontal_edges('AFF', aff, g)
    add_horizontal_edges('NOR', nor, g)

    # add attribute 'title' to nodes
    for n in g:
        get_node_title(n, g)

    return g, insight

def get_utterance_graph_self(u, parameters_idx):
    """
    u: Self's utterance (dict)
    """
    # reference: get_utterance_graph()
    ut = get_node(u, 'ut', parameters_idx)
    l = ut[0]
    nodes = {l: ut[1]}

    g = nx.MultiDiGraph(root=l)

    id_base = ut[1]['id'][:-parameters_idx[-1]]
    
    # socio-cultural dimensions
    ds = u['strategy'][0].split('_')

    temp = dict()

    for d in ds:
        if d == 'informational':
            label = f"{id_base}{str(1).zfill(parameters_idx[-1])}\n{u['informational'][1]}"
            nodes[label] = {
                'reference': u['informational'][0],
                'shape': shape,
                'color': color_map['inf'],
                'size': 10
            }
            temp['INF'] = [label]

        
        if d == 'affective':
            label = f"{id_base}{str(2).zfill(parameters_idx[-1])}"
            nodes[label] = {
                'reference': u['affective'][0],
                'shape': shape,
                'color': color_map['aff'],
                'size': 10
            }
            temp['AFF'] = [label]

        if d == 'normative':
            label = f"{id_base}{str(3).zfill(parameters_idx[-1])}"
            nodes[label] = {
                'reference': u['normative'][0],
                'shape': shape,
                'color': color_map['nor'],
                'size': 10
            }
            temp['NOR'] = [label]

    g.add_nodes_from(nodes.items())

    for k in temp:
        add_horizontal_edges(k, temp[k], g)

    # add attribute 'title' to nodes
    for n in g:
        get_node_title(n, g)

    # print(list(g.nodes(data=True)))

    return g

def update_contextual_graph(g, u, us, s, nlpt, log, memory, edge_weights, insight_script, node_indexes):
    """
    g (contextual graph): an instance of mx.MultiDiGraph()
    u (utterance): a spaCy Doc/ dict (action of Self)
    us: list of all utterances (spaCy Docs)
    s: synchronous Wolfram session
    (is_self: if computer is self - handled at the spaCy level)
    log: log of an instance of Dimo
    memory: memory of an instance of Dimo (dict)
    edge_weights: edge_weights of an instance of Dimo (dict)
    insight_script: YAML insight script (self.insight of Dimo class)
    node_indexes: nodes_indexes of an instance of Dimo 
    -> updated g (and u)
    """
    parameters_idx = get_parameters_idx(node_indexes)


    # utterance graph
    if isinstance(u, spacy.tokens.Doc):
        # ug = get_utterance_graph(u, s, nlpt)
        ug, insight = get_utterance_graph(u, s, nlpt, memory, edge_weights, insight_script, parameters_idx)
    else:
        ug = get_utterance_graph_self(u, parameters_idx)

    log.debug(f"Utterance graph: {ug}")

    # last utterance in g
    lu = g.graph['last_utterance'] # label of utterance root node

    # https://github.com/luutuntin/Cinderella/blob/c3730f464c7c7f7deea6bf28d04702c711cb474a/semaland_sent_graph_merging.py
    g = nx.union(g, ug)

    if lu:
        # connect the root node of u and the root node of the last utterance in g before union
        # HARDCODE
        g.add_edge(lu, ug.graph['root'], label='ver', value=1, weight=0.1, title='weight: 0.1')

    g.graph['last_utterance'] = ug.graph['root']

    if isinstance(u, spacy.tokens.Doc):
        # corel handling
        for n in eval(ug.graph['corel_nodes']):
            # list of tuples of (weight, start index, length, utterance id)
            corels = eval(ug.nodes[n]['corels']) # see get_utterance_graph(u, s, nlpt), relating to nx.write_gml()

            for c in corels:
                # label of corel node
                # i0, i1, i2, i3: idx of utterance, 1st word token, phrase length, related concepts respectively (int)
                # HARDCODE
                l = f'{get_node_idx(c[3], c[1], c[2], 0, parameters_idx)}\n{us[c[3]][c[1]: c[1]+c[2]].text}'

                # HARDCODE
                g.add_edge(l, n, label='corel', value=int(c[0]*10), weight=c[0], title=f'weight: {c[0]}')
    else:        
        # reference handling
        for n in ug:
            if n != ug.graph['root']:
                g.add_edge(n, g.nodes[n]['reference'], label='reference', value=1, weight=0.1, title='weight: 0.1')
    
    log.debug(f"Updated dialog graph: {g}")   

    if isinstance(u, spacy.tokens.Doc):
        return g, insight
    return g


#%%
# https://pyvis.readthedocs.io/en/latest/
from pyvis.network import Network


def visualize(input, output):
    """
    input: dialogic NetWorkX graph
    output: name/path of html file of input visualization
    """
    # selected configuration - output is shown in on browser
    # https://pyvis.readthedocs.io/en/latest/documentation.html
    # neighborhood_highlight: turn off the rest (e.g. their labels, colors)
    # layout: hierarchical
    # select_menu: menu to select a node
    g = Network(height='630px', width='100%', directed=True, filter_menu=True)
    g.from_nx(input)

    # reference: https://www.youtube.com/watch?v=jNfkbkHzZ5s
    # "hover": visualization of nodes and edges is changed/highlighted during hovering
    # ""navigationButtons": green button at the bottom corners
    g.set_options("""
    const options = {
        "interaction": {
            "hover": true,
            "navigationButtons": true
        }
    }
    """)



    g.show(output + '.html', notebook=False) # g.save_graph("...html")

# #%%
# # https://networkx.org/documentation/networkx-2.6.2/reference/readwrite/generated/networkx.readwrite.graphml.read_graphml.html
# output = 'CHI_MOT_2023-06-21-03-23-25_networkx'
# input = nx.read_gml(output + '.gml')
# visualize(input, output)

def analyze_contextual_graph(graph_id):
    """
    graph_id: GML file name (without extension) of NetWorkX contextual graph
    """
    #HARDCODE
    g = nx.read_gml(graph_id + '.gml')
    visualize(g, graph_id)

    # https://networkx.org/documentation/networkx-2.6.2/reference/functions.html
    # https://networkx.org/documentation/networkx-2.6.2/reference/classes/multidigraph.html
    return {
        'overview': nx.info(g), # str(g),
        'number of nodes': nx.number_of_nodes(g), # len(g.nodes), 
        'number of edges': nx.number_of_edges(g), # len(g.edges),
        'number of utterance nodes': len([n for n in g if g.nodes[n]['color']=='lightgray']),
        'number of informational nodes': len([n for n in g if g.nodes[n]['color']=='cyan']),
        'number of affective nodes': len([n for n in g if g.nodes[n]['color']=='pink']),
        'number of normative nodes': len([n for n in g if g.nodes[n]['color']=='yellow']),
        'number of informational coherence relations': len([e for e in g.edges if g.edges[e]['label'] == 'corel']),
        'number of world knowledge relations': len([e for e in g.edges if g.edges[e]['label'] == 'kb_world']),
    }
# %%
# graph_id = 'CHI_MOT_2023-06-21-03-23-25_networkx'
# graph_statistics = analyze_contextual_graph(graph_id)

# {'overview': 'MultiDiGraph with 804 nodes and 1124 edges',
#  'number of nodes': 804,
#  'number of edges': 1124,
#  'number of utterance nodes': 314,
#  'number of informational nodes': 333,
#  'number of affective nodes': 87,
#  'number of normative nodes': 70,
#  'number of informational coherence relations': 174,
#  'number of world knowledge relations': 64}
# %%
