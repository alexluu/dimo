print('Dimo - Wolfram')

"""
Reference:
+ wolfram/jupyterwolfram101.py

+ https://alexluu.flowlu.com/_module/knowledgebase/view/article/261--wolfram-engine

+ https://writings.stephenwolfram.com/2019/05/launching-today-free-wolfram-engine-for-developers/

+ https://blog.wolfram.com/2019/05/16/announcing-the-wolfram-client-library-for-python/

+ https://reference.wolfram.com/language/WolframClientForPython/

+ obsidian://open?vault=Obsidian%20Vault&file=dissertation%2FWolfram
"""

# %%
# from wolframclient.evaluation import WolframLanguageSession
from wolframclient.language import wl, wlexpr
from wolframclient.language.expression import WLFunction # vs WLSymbol

from operator import itemgetter
import re


# https://stackoverflow.com/questions/29916065/how-to-do-camelcase-split-in-python
def camel_case_split(identifier):
    matches = re.finditer('.+?(?:(?<=[a-z])(?=[A-Z])|(?<=[A-Z])(?=[A-Z][a-z])|$)', identifier)
    return [m.group(0) for m in matches]


def normalize_ent_type_token(ett):
    """
    ett: str of entity type token (element of output list of camel_case_split())
    """
    return ett if ett.isupper() else ett.lower()


def get_ent_type_plus(ent_types, v, nlpt):
    """
    ent_types: set of Wolfram ent types (str)
    v: spaCy-transformers embedding of the interpreted text string
    nlpt: spacy-transformers model
    -> (selected ent_type, selected ent_type_normalized), ent_types_normalized
    """
    # print(ent_types)
    ent_types_normalized = [
        ' '.join(
            [normalize_ent_type_token(ett) for ett in camel_case_split(et)]
        )
        for et in ent_types
    ]
    
    if len(ent_types)>1:        
        # reference: jupyter_dimo_spacy.py - get_wordnet_sense()
        similarity_scores = {(et, etn): nlpt(etn).similarity(v) for (et, etn) in zip(ent_types, ent_types_normalized)}

        return max(similarity_scores.items(), key=itemgetter(1))[0], ent_types_normalized

    return (ent_types[0], ent_types_normalized[0]), ent_types_normalized

def get_entity_plus(s, ec, v, nlpt):
    """
    s: synchronous Wolfram session
    ec: str of entity candidate
    v: spaCy-transformers embedding of the interpreted text string
    nlpt: spacy-transformers model
    -> disambiguated Wolfram entity and all possible ent types
    """
    # https://reference.wolfram.com/language/guide/KnowledgeRepresentationAndAccess.html
    # https://www.wolfram.com/knowledgebase/source-information/
    # https://reference.wolfram.com/language/ref/SemanticInterpretation.html
    # https://reference.wolfram.com/language/ref/AmbiguityList.html
    # https://www.wolfram.com/language/fast-introduction-for-programmers/en/lists/
    # https://reference.wolfram.com/language/WolframClientForPython/docpages/advanced_usages.html#
    output = s.evaluate(wl.System.SemanticInterpretation(ec, wl.System.Rule(wl.System.AmbiguityFunction, wl.System.All)))
    
    # https://reference.wolfram.com/language/ref/Head.html?q=Head
    if isinstance(output, WLFunction) and output[0]: # tuple of possible semantic interpretation, handle TypeError: 'WLSymbol' object is not iterable
        try:
            ents = [(x, s.evaluate(wl.System.EntityTypeName(x)))  for x in output[0] if str(s.evaluate(wl.System.Head(x)))=="Entity"]

            if ents:    
                ent_types = list(set(et for _, et in ents))
                (ent_type, ent_type_normalized), ent_types_normalized = get_ent_type_plus(ent_types, v, nlpt)
                for e,  et in ents:
                    if et==ent_type:
                        # the first entity in ents whose type is ent_type
                        ent = e
                        break    
                
                # if output.messages:
                #     for m in output.messages:
                #         print(m)

                return (ent, ent_type, ent_type_normalized, ent_types, ent_types_normalized)
        except Exception as err:
            print(f"Exception: {err}")
            print(type(output[0]))

def get_ent_properties(s, e):
    """
    s: Wolfram synchronous session
    e: Wolfram entity
    -> return Wolfram immutable dict of e's properties and their values
    ...
    """
    # https://reference.wolfram.com/language/ref/Normal.html
    return s.evaluate(wl.Normal(e("Dataset")))
    
def get_ent_info(s, ec, v, nlpt):
    """
    reference: get_entity_plus()
    s: synchronous Wolfram session
    ec: str of entity candidate
    v: spaCy-transformers embedding of the interpreted text string
    nlpt: spacy-transformers model
    -> ...
    """
    output = dict()

    temp = get_entity_plus(s, ec, v, nlpt)
    if temp: # if there is at least one entity
        e, e_type, e_type_normalized, ent_types, ent_types_normalized = temp

        # if e is a named entity
        # https://reference.wolfram.com/language/ref/CommonName.html
        if 'name' in s.evaluate(wl.System.CommonName(e("Properties"))):
            # https://reference.wolfram.com/language/ref/entity/Source.html - "Source" is a Wolfram named entity (having the "Name" property)
            # list of Wolfram "Source" entities of "Name" property - assumption: the first one is sufficient
            e_name_source = s.evaluate(e("Name", "Source"))    
            if e_name_source:
                # https://reference.wolfram.com/language/ref/ToString.html
                # https://reference.wolfram.com/language/ref/InputForm.html
                output["Wolfram expression"] = s.evaluate(wl.System.ToString(e,wl.System.InputForm))                
                e_name = s.evaluate(e("Name"))
                output["entity name"] = e_name
                output["selected entity type"] = e_type_normalized

                if len(ent_types)>1:
                    temp = ", ".join(ent_types_normalized[:-1]) + f" or {ent_types_normalized[-1]}"
                    output["entity type comment"] = f"{e_name} can be a {temp}."
                else:
                    output["entity type comment"] = f"{e_name} is a {e_type_normalized}."

                e_name_source_str = s.evaluate(e_name_source[0]("Name"))
                output["source"] = e_name_source_str

                output["related entities with source"] = list()

                ps = get_ent_properties(s, e)
                for p in ps:
                    # value = s.evaluate(wl.System.EntityValue(e,p))
                    value = s.evaluate(e(p))
                    # list of Wolfram "Source" entities - assumption: the first one is sufficient
                    source = s.evaluate(e(p, "Source"))

                    # if source of p is not empty and value of p is a named entity
                    if source and str(s.evaluate(wl.System.Head(value)))=="Entity" and 'name' in s.evaluate(wl.System.CommonName(value("Properties"))):
                        source_str = s.evaluate(source[0]("Name"))
                        # https://reference.wolfram.com/language/ref/CommonName.html
                        property_str = s.evaluate(wl.System.CommonName(p))
                        ent_type_str = e_type_normalized                
                        value_str = s.evaluate(value("Name"))
                        value_entity_str = s.evaluate(wl.System.ToString(value,wl.System.InputForm))
                        property_info = {
                            "property": property_str,
                            "value": value_str,
                            "source": source_str,
                            "entity": value_entity_str 
                        }

                        if e_name!=value_str:
                            property_info["related entity comment"] = f"According to {source_str}, the {property_str}, relating to the {ent_type_str}, is {value_str}."

                        else:
                            property_info["related entity comment"] = f"According to {source_str}, the {property_str}, relating to the {ent_type_str}, is also named {value_str}."

                        output["related entities with source"].append(property_info)

    return output

# example of output:
# {'Wolfram expression': 'Entity["Shipwreck", "RMSTitanic::5d778"]',
#  'entity name': 'RMS Titanic',
#  'selected entity type': 'shipwreck',
#  'entity type comment': 'RMS Titanic can be a historical event, shipwreck, ship, movie or word.',
#  'source': 'Wikipedia',
#  'related entities with source': [{'property': 'ship',
#    'value': 'RMS Titanic',
#    'source': 'Wikipedia',
#    'entity': 'Entity["Ship", "RMSTitanic::5d778"]'}]}

# import spacy
# nlp = spacy.load("en_core_web_sm")
# nlpt = spacy.load("en_trf_distilbertbaseuncased_lg")
# text = "From Titanic, I can see the iceberg in the ocean."
# d = nlp(text)
# dt = nlpt(text)
# v = dt[1]
# with WolframLanguageSession() as s:    
#     print(s)
#     output = get_ent_info(s, 'Titanic', v, nlpt)