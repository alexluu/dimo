print('Dimo - Salience')

"""
reference: obsidian://open?vault=Obsidian%20Vault&file=dissertation%2Fsalience
"""


#%%
import random
from collections import defaultdict
from operator import itemgetter
from dimo_networkx import get_parameters_idx, get_node_idx


# FOCUS
excluded_edge_labels = {'INF', 'AFF', 'NOR', 'hor', 'ver'}


syn_role_groups = {
    'subject': {'nsubj', 'nsubjpass'},
    'object': {'dobj', 'dative', 'ccomp', 'xcomp', 'acomp', 'attr'},
}

syn_role2group = {vv: k for k in syn_role_groups for vv in syn_role_groups[k]}

def get_syn_role_weight(g, n, syn_role_weights):
    """
    g: NetWorkX contextual graph
    n: node (label)
    """
    if g.nodes[n]['syn_role'] in syn_role2group:
        return syn_role_weights[syn_role2group[g.nodes[n]['syn_role']]]
    
    return syn_role_weights['other']

# https://networkx.org/documentation/networkx-2.6.2/reference/classes/multidigraph.html#reporting-nodes-edges-and-neighbors
# https://github.com/luutuntin/Cinderella/blob/c3730f464c7c7f7deea6bf28d04702c711cb474a/semaland_amr_graph.py#L43
def sum_weights_in(g, n):
    """
    g: NetWorkX contextual graph
    n: node (label)
    -> sum of weights of edges coming in to n
    """
    return sum(x[2]['weight'] for x in g.in_edges(n, data=True) if x[2]['label'] not in excluded_edge_labels)

def sum_weights_out(g, n):
    """
    g: NetWorkX contextual graph
    n: node (label)
    -> sum of weights of edges going out from n
    """
    return sum(x[2]['weight'] for x in g.out_edges(n, data=True) if x[2]['label'] not in excluded_edge_labels)

# TODO (?): https://networkx.org/documentation/networkx-2.6.2/reference/algorithms/generated/networkx.algorithms.chains.chain_decomposition.html#networkx.algorithms.chains.chain_decomposition
def rank_inf(insight_inf, g, syn_role_weights):
    """
    g: NetWorkX contextual graph

    """
    if insight_inf:
        temp = sorted([n for n in insight_inf], reverse=True) # sorted based on recentness/recency

        return sorted([(*n, g.nodes[n[0]]['wrt_ROOT'], sum([sum_weights_out(g, n[0]), sum_weights_in(g, n[0]), get_syn_role_weight(g, n[0], syn_role_weights)])) for n in temp], key=itemgetter(-1), reverse=True)
    
    return list() # handle TypeError: 'NoneType' object is not iterable (in get_action())    

def get_polarity_type(p):
    """
    p: polarity (float)
    -> 'positive'/'negative'
    """
    if p > 0:
        return 'positive'
    return 'negative'

def rank_aff(insight_aff, g, threshold_aff):
    """
    g: NetWorkX contextual graph
    -> sorted list of affective instances above threshold_aff (reverse=True)

    """
    temp = sorted([n for n in insight_aff if abs(n[-1]) >= threshold_aff], reverse=True) # sorted based on recentness/recency
    
    if temp:
        return sorted([(*n, g.nodes[n[0]]['wrt_ROOT'], get_polarity_type(n[-1]), abs(n[-1])) for n in temp], key=itemgetter(-1), reverse=True)
    
    return list() # handle TypeError: 'NoneType' object is not iterable (in get_action())

def get_sfs_type(p, c): # sfs: social focus shift
    """
    reference: get_social_focus() in ...dimo_spacy.py

    """
    if p.lower() in {'i', 'me'}:
        if c == '.':
            return ('Self_emphasis', 'declarative')
        else: # c =='!':
            return ('Self_emphasis', 'imperative')
    else: # p.lower() in {'you'}
        if c == '?':
            return ('alignment_emphasis', 'interrogative')
        else: # c =='!':
            return ('alignment_emphasis', 'imperative')

def rank_nor(insight_nor, g):
    """
    g: NetWorkX contextual graph

    """
    if insight_nor:
        return sorted([(*n, get_sfs_type(n[1], n[2])) for n in insight_nor], reverse=True) # sorted based on recentness/recency
    
    return list() # handle TypeError: 'NoneType' object is not iterable (in get_action())


def get_focus_text(f, s):
    """
    f: focus dict
    s: insight script dict
    """
    temp = list()
    if f['informational']:
        temp.append(s['informational'].format(f"'{f['informational'][0][1]}'"))
    if f['affective']:
        temp.append(s['affective'].format(f"'{f['affective'][0][1]}'"))
    if f['normative']:
        temp.append(s['normative'].format(f"'{f['normative'][0][1]}' ('{f['normative'][0][2]}')"))

    return f"[{' '.join(temp)}]"    

def get_focus(insight, g, threshold_aff,syn_role_weights, focus_script):
    """
    insight: actual insight values (vs insight script)
    g: NetWorkX contextual graph
    threshold_aff: from YAML script (self.threshold_aff of Dimo class)
    syn_role_weights: from YAML script (self.syn_role_weights of Dimo class) 
    focus_script: from YAML script (self.focus of Dimo class) 
    """
    focus =  {
        'informational': rank_inf(insight['informational'], g, syn_role_weights),
        'affective': rank_aff(insight['affective'], g, threshold_aff),
        'normative': rank_nor(insight['normative'], g)
    }
    focus['text'] = get_focus_text(focus, focus_script)

    return focus

# ACTION

def get_inf(g, n, m):
    """
    g: NetWorkX contextual graph
    n: node (label)
    m: memory of an instance of Dimo
    -> informational content for filling action template
    """
    candidates = defaultdict(list)
    for s in g.successors(n):
        attrs = g.nodes[s]
        if 'Wolfram_expression' in attrs: # kb_world
            candidate = ((attrs['entity_name'], attrs['entity_type']), attrs['id'])
            if candidate[0] in m['kb_world_unused']:
                candidates['kb_world'].append(candidate)
        elif 'cats' in attrs and 'wn_np' in attrs['cats']:
            candidate = ((attrs['text'], attrs['wn_sense']), attrs['id'])
            if candidate[0] in m['kb_ling_unused']:
                candidates['kb_ling'].append(candidate)

    if candidates['kb_world']:
        selected = random.choice(candidates['kb_world'])
        # concepts share the same text representation
        homonyms = [k for k in m['kb_world_unused'] if k[0]==selected[0][0]]
        for h in homonyms:
            temp = m['kb_world_unused'].pop(h)
            m['kb_world_used'][h] = temp # m['kb_world_used'].update({h: temp})
        return (selected, 'kb_world')
    
    if candidates['kb_ling']:
        selected = random.choice(candidates['kb_ling'])
        temp = m['kb_ling_unused'].pop(selected[0])
        m['kb_ling_used'][selected[0]] = temp # m['kb_world_used'].update({selected[0]: temp})
        return (selected, 'kb_ling')

def get_inf_memory(g, m):
    """
    g: NetWorkX contextual graph
    m: memory of an instance of Dimo
    """ 
    if m['kb_world_unused']:
        selected = random.choice(list(m['kb_world_unused'].keys()))
        temp = m['kb_world_unused'].pop(selected)
        m['kb_world_used'][selected] = temp # m['kb_world_used'].update({selected: temp})

        # get parent
        # node label
        n = f"{temp[-1]}\n{selected[0]} ({selected[1]})"
        # parent label
        parent = list(g.predecessors(n))[0]

        # concepts share the same text representation
        homonyms = [k for k in m['kb_world_unused'] if k[0]==selected[0]]
        for h in homonyms:
            temp = m['kb_world_unused'].pop(h)
            m['kb_world_used'][h] = temp # m['kb_world_used'].update({h: temp})

        return (g.nodes[parent]['text'], (n, g.nodes[n]['comment']), 'kb_world')
    
    if m['kb_ling_unused']:
        selected = random.choice(list(m['kb_ling_unused'].keys()))
        temp = m['kb_ling_unused'].pop(selected)
        m['kb_ling_used'][selected] = temp # m['kb_world_used'].update({selected: temp})

        # node label
        n = f"{temp[-1]}\n{selected[0]}"

        return (selected[0], (n, g.nodes[n]['definition']), 'kb_ling')


def get_utterance_template(x):
    """
    x: value in YAML script
    """
    if isinstance(x, list):
        return random.choice(x)
    return x

# https://towardsdatascience.com/how-to-use-variable-number-of-arguments-in-python-functions-d3a49a9b7db6
def get_utterance(t, *slot_values):
    """
    t: utterance template (str)
    slot_values: str values to complate the template
    """
    return t.format(*slot_values)


def get_action(focus, u, g, templates, memory, node_indexes):
    """
    u: spaCy utterance
    g: NetWorkX contextual graph
    templates: action of an instance of Dimo
    """
    # last sent
    s = list(u.sents)[-1]
    sid = get_node_idx(u._.id, s.start, 0, 0, get_parameters_idx(node_indexes))

    salience_inf = [x for x in focus['informational'] if x[0].partition('\n')[0]>=sid] # list of tuples of (node label, text, info-packaging position, salience score)

    salience_aff = [x for x in focus['affective'] if x[0].partition('\n')[0]>=sid] # list of tuples of (node label, text, polarity, info-packaging position, YAML polarity type, absolute value of polarity)

    salience_nor = [x for x in focus['normative'] if x[0].partition('\n')[0]>=sid] # list of tuples of (node label, text, clause type, (sfs type, YAML text of clause type))

    # 'FACTUAL'
    if s._.clause_type == '?':# interrogative is very salient in that it strongly requires certain backward-looking salent content in the response
        if not s._.social_foci:
            return {
                'text': get_utterance_template(templates['FACTUAL']),
                'strategy': ['FACTUAL'] 
            }
    
    # normative
    if salience_nor: 
        if salience_inf: # 'informational_normative'
            return {
                'text': get_utterance(get_utterance_template(templates['informational_normative'][salience_nor[0][-1][0]][salience_nor[0][-1][1]]), salience_inf[0][1]),
                'strategy': ['informational_normative', salience_nor[0][-1][0], salience_nor[0][-1][1]],
                'normative': salience_nor[0],
                'informational': salience_inf[0],
            }        
        
        if salience_aff: # 'affective_normative'
            return {
                'text': get_utterance_template(templates['affective_normative'][salience_nor[0][-1][0]][salience_aff[0][-2]][salience_nor[0][-1][1]]),
                'strategy': ['affective_normative', salience_nor[0][-1][0], salience_aff[0][-2], salience_nor[0][-1][1]],
                'normative': salience_nor[0],
                'affective': salience_aff[0],
            }
        
        return {
            'text': get_utterance_template(templates['normative'][salience_nor[0][-1][0]][salience_nor[0][-1][1]]),
            'strategy': ['normative', salience_nor[0][-1][0], salience_nor[0][-1][1]],
            'normative': salience_nor[0],
        }


    # forward-looking informational salience
    if salience_inf:
        if salience_inf[0][2] == 'right': # 'informational'
            inf = get_inf(g, salience_inf[0][0], memory)
            if inf:
                if inf[-1] == 'kb_world': # inf: (((entity name, entity type), node id), kb type)
                    attrs = g.nodes[f"{inf[0][-1]}\n{inf[0][0][0]} ({inf[0][0][1]})"]
                    return {
                        'text': get_utterance(get_utterance_template(templates['informational']['kb_world'][attrs['certainty_type']]), attrs['comment']),
                        'strategy': ['informational', 'kb_world', attrs['certainty_type']],
                        # 'informational': (salience_inf[0], inf)
                        'informational': salience_inf[0],
                        'knowledge': inf
                    }
                
                if inf[-1] == 'kb_ling': # inf: (((text, wn_sense), node id), kb type)
                    attrs = g.nodes[f"{inf[0][-1]}\n{inf[0][0][0]}"]
                    return {
                        'text': get_utterance(get_utterance_template(templates['informational']['kb_ling']), attrs['text'], attrs['definition']),
                        'strategy': ['informational', 'kb_ling'],
                        # 'informational': (salience_inf[0], inf)
                        'informational': salience_inf[0],
                        'knowledge': inf
                    }

        if salience_aff: # 'informational_affective'
            return {
                'text': get_utterance(get_utterance_template(templates['informational_affective'][salience_aff[0][-2]]), salience_inf[0][1]),
                'strategy': ['informational_affective', salience_aff[0][-2]],
                'informational': salience_inf[0],
                'affective': salience_aff[0],
            }
        
    if salience_aff: # 'affective'
        return {
            'text': get_utterance_template(templates['affective'][salience_aff[0][-2]]),
            'strategy': ['affective', salience_aff[0][-2]],
            'affective': salience_aff[0],
        }
    
# inf: (node label, text, info-packaging position, salience score)
# aff: (node label, text, polarity, info-packaging position, absolute value of polarity)
# nor: (node label, text, clause type, (sfs type, YAML text of clause type))    

    inf_m = get_inf_memory(g, memory)
    if inf_m:
        return {
            'text': get_utterance(get_utterance_template(templates['MEMORY'][inf_m[-1]]), inf_m[0], inf_m[1][1]),
            'strategy': ['MEMORY', inf_m[-1]],
            'informational': (inf_m[1][0], inf_m[0]),
            'knowledge': inf_m
        }    

    return {
        'text': get_utterance_template(templates['NONE']),
        'strategy': ['NONE']
    } 