print('Dimo - spaCy')

#%%
# https://v2.spacy.io
import spacy
from spacy.tokens import Doc, Span, Token

# https://github.com/argilla-io/spacy-wordnet
from spacy_wordnet.wordnet_annotator import WordnetAnnotator
from nltk.corpus import wordnet as wn

# https://pypi.org/project/spacytextblob/0.1.7/ - sentiment analysis
from spacytextblob.spacytextblob import SpacyTextBlob

from operator import itemgetter
from collections import defaultdict


def setup_spacy_models(core_model, transformer):
    """
    core_model: str of spaCy core model (e.g. "en_core_web_sm")
    transformer: str of spaCy transformer (e.g. "en_trf_distilbertbaseuncased_lg")
    """
    nlp = spacy.load(core_model)

    nlp.add_pipe(WordnetAnnotator(nlp.lang), after='tagger')
    # token._.wordnet.[...]
    
    nlp.add_pipe(SpacyTextBlob())
    # doc._.sentiment.[...]

    nlpt = spacy.load(transformer)

    return nlp, nlpt


# UPOS of function word
# according to our annotation guidelines (https://docs.google.com/document/d/1AWxEkc72fctQzceeARi5chg9Mf5zCQUWueo5CtLt-k0/edit#)
upos_f = {
    'INTJ': 'interjection',
    'ADP': 'adposition',
    'AUX': 'auxiliary',
    'CCONJ': 'coordinating conjunction',
    'DET': 'determiner',
    'NUM': 'numeral',
    'PART': 'particle',
    'PRON': 'pronoun',
    'SCONJ': 'subordinating conjunction',
    'PUNCT': 'punctuation',
    'SYM': 'symbol',
    'X': 'other',
}

# UPOS of content words
# according to our annotation guidelines (https://docs.google.com/document/d/1AWxEkc72fctQzceeARi5chg9Mf5zCQUWueo5CtLt-k0/edit#)
upos_c = {
    "ADJ": "adjective",
    "ADV": "adverb",
    "NOUN": "noun",
    "PROPN": "proper noun",
    "VERB": "verb",
}

# https://linguistics.stackexchange.com/questions/6508/which-part-of-speech-are-s-and-r-in-wordnet
# https://stackoverflow.com/questions/18817396/what-part-of-speech-does-s-stand-for-in-wordnet-synsets
# https://wordnet.princeton.edu/documentation/wndb5wn
# https://wordnet.princeton.edu/documentation/wngloss7wn
# https://github.com/luutuntin/Cinderella/blob/master/semaland_semantic_features.py
upos2wn = {
    "ADJ": {'a', 's'},
    "ADV": {'r'},
    "NOUN": {'n'},
    "PROPN": {'n'},
    "VERB": {'v'},
}


def setup_spacy_ds(): # ds: data structure
    """
    ...
    """
    # https://course.spacy.io/en/chapter3
    # Doc is an annotated utterance
    # id is int
    Doc.set_extension("id", default=None, force=True)
    Doc.set_extension("speaker", default="unknown", force=True)
    
    # output of a spaCy-transformers model
    Doc.set_extension("transformer", default=None, force=True)

    # dict of morphological features
    Token.set_extension("morph", default=None, force=True)

    # str of social focus label ("Self"/"the Other")
    Token.set_extension("social_focus", default=None, force=True)
    # list of indexes of social focus tokens
    Span.set_extension("social_foci", default=list(), force=True)

    # bare NP in noun chunks - tuple of (start index, end index)
    Span.set_extension("bare_np", default=None, force=True)    
    # list of tuples of (index of the corresponding noun chunk,start index, end index) of WordNet noun phrases (which are bare noun phrases of noun chunks)
    Doc.set_extension("wn_nps", default=list(), force=True)

    # WordNet sense dictionary - key: sense name, value: tuple(sense definition, the corresponding vector)
    Token.set_extension("wn_senses", default=None, force=True)
    Span.set_extension("wn_senses", default=None, force=True)
    # WordNet-based polysemous? 'y'/'n'
    Token.set_extension("wn_polysemous", default='n/a', force=True)
    Span.set_extension("wn_polysemous", default='n/a', force=True)
    # WordNet sense dictionary with only one key - the most appropriate sense
    Token.set_extension("wn_sense", default=None, force=True)
    Span.set_extension("wn_sense", default=None, force=True)

    # convenient for getting cached_corel    
    # forward-looking coherence relation dictionary - key: corel label (e.g. 'token' vs 'span'), value: tuple of (weight, anaphor's start index, anaphor's length, index of doc of anaphor)
    # currently: at most one forward-looking coherence relation per token (root of a noun chunk) - top_k = 1
    Token.set_extension("corel_forward", default=None, force=True)
    # Span.set_extension("corel_forward", default=None, force=True)
    
    # convenient for updating NetWorkX graph
    # back-looking coherence relation dictionary - key: corel label (e.g. 'token' vs 'span'), value: list of tuples of (weight, antecedent's start index, antecedent's length, index of doc of antecedent)
    # currently: only apply to root of a noun chunk
    Token.set_extension("corel_backward", default=None, force=True)
    # Span.set_extension("corel_backward", default=None, force=True)
    
    # tuple of (string which includes the examined word/phrase and most salient entities/phrases which share coherence relations with that word/phrase, index of the examined word/(start index, end index) of the examined phrase in that string, spacy-transformers doc of that string)
    Token.set_extension("modified_content", default=None, force=True)
    Span.set_extension("modified_content", default=None, force=True)

    # target: WordNet noun phrases, entities, sentiment words
    # parent span can be noun chunk the target belongs to
    # tuple of (start index, end index)
    Token.set_extension("parent_span", default=None, force=True)
    Span.set_extension("parent_span", default=None, force=True)

    # str
    Span.set_extension("clause_type", default="n/a", force=True)
    

# https://www.nltk.org/book/ch02.html
# reference: generate_auto_annotation.py - get_senses(), get_baseline_sense()
def get_wordnet_senses(w, nlpt):
    """
    w: spaCy token
    -> dictionary:
        - key: NLTK name of a sense (e.g. "bank.n.01")
        - value: WordNet definition of that sense (e.g. "sloping land (especially the slope beside a body of water)"), corresponding vector
    """
    temp =  {synset.name():(synset.definition(), nlpt(synset.definition())) for synset in w._.wordnet.synsets() if synset.pos() in upos2wn[w.pos_]}
    if temp:
        w._.wn_senses = temp
        set_wn_polysemous(w, len(temp))

def get_wordnet_sense(v,senses):
    """
    v: spaCy-transformers vector of spaCy token/span
    senses: dictionary of WordNet senses (e.g. output of get_wordnet_senses())
    nlpt: spacy-transformer
    -> NLTK name of the most appropriate WordNet sense
    """
    if len(senses)>1:    
        # sn: synset name, sdv: vector of synset definition
        similarity_scores = {sn:sdv.similarity(v) for sn,(_,sdv) in senses.items()}
        # https://stackoverflow.com/questions/268272/getting-key-with-maximum-value-in-dictionary
        return max(similarity_scores.items(), key=itemgetter(1))[0]
    
    return list(senses.keys())[0]

def set_wn_polysemous(x, n):
    """
    x: spaCy token/span
    n: number of WordNet senses
    -> x._.wn_polysemous = 'y'/'n'
    """
    if n==1:
        x._.wn_polysemous = 'n'
    else:
        x._.wn_polysemous = 'y'


# https://aclanthology.org/2022.mwe-1.8/ "Multi-word Lexical Units Recognition in WordNet"
# https://stackoverflow.com/questions/32097226/how-to-determine-multiword-a-is-a-b-in-wordnet
# https://github.com/MarekMaziarz/Multi-word-lexical-units
# https://docs.google.com/spreadsheets/d/1fJINFxaui11w7HrUdR2o7RUFvW1U2uNsJDTdx3slylc/edit#gid=1910166996
def get_bare_np(nc):
    """
    nc: spaCy noun chunk
    -> tuple of (start index, end index) # end index = start index + length of the bare NP; these indexes are within Doc, not Span
    """
    # "That is my brother's black and white bear of computer interaction with the light speed and a sampling rate."
    # https://link.springer.com/chapter/10.1007/978-94-007-3002-1_2#:~:text=Bare%20Noun%20Phrases%20(bare%20NPs,preceded%20by%20an%20overt%20determiner.
    # https://en.wikipedia.org/wiki/Bare_nouns
    # my definition of bare NP: NP not preceded by determiner, numeric modifier, possession modifiers (and of course pre-determiner, e.g. "all" in "all the students")
    # https://spacy.io/models/en#en_core_web_sm (assumption: the same for spaCy v.2 - https://v2.spacy.io/models/en#en_core_web_sm)
    # https://github.com/clir/clearnlp-guidelines/blob/master/md/specifications/dependency_labels.md#nominals
    # https://universaldependencies.org/u/overview/nominal-syntax.html
    # https://universaldependencies.org/u/dep/all.html
    # https://universaldependencies.org/u/dep/all.html#al-u-dep/obl

    # relevant dependency labels: det, nummod, poss (nmod doesn't seem to be captured by spaCy's parser)
    # noun chunk's head (data type: spacy's Token)
    h = nc.root
    # start index
    i_s = h.i
    # end index
    i_e = i_s + 1
    for i in range(i_s-1, nc.start-1, -1):
        if nc.doc[i].head != h or \
           nc.doc[i].dep_ in {'det','nummod','poss'}:
           break
        else:
            i_s = i
    
    nc._.bare_np = (i_s,i_e)
    
    return (i_s,i_e)

def get_wn_np(nc, nlpt):
    """
    nc: spaCy noun chunk
    # -> tuple of (index of 1st token, len of compound)/None
    -> tuple of (start index, end index)/None
    """
    # https://aclanthology.org/2022.mwe-1.8/
    # https://github.com/MarekMaziarz/Multi-word-lexical-units
    # https://drive.google.com/drive/folders/1f8KNFJ0yY5wTPyETEyNM5uaPN8kXSI7m
    # https://www.d.umn.edu/~tpederse/wordnet.html
    # https://stackoverflow.com/questions/32097226/how-to-determine-multiword-a-is-a-b-in-wordnet
    # http://wordnetweb.princeton.edu/perl/webwn?s=black+bears&sub=Search+WordNet&o2=&o0=1&o8=1&o1=1&o7=&o5=&o9=&o6=&o3=&o4=&h=00
    # https://www.wolframalpha.com/input?i=black+and+white+bear

    # start and end indexes of the bare NP belonging to the spaCy noun chunk nc
    i_s, i_e = get_bare_np(nc)
    # spaCy span of the bare NP
    bare_np = nc.doc[i_s:i_e]
    if len(bare_np)>1: # bare_np is a compound
        wn_senses = wn.synsets(bare_np.text.replace(' ', '_'))
        if wn_senses:
            temp = {
                s.name(): (s.definition(), nlpt(s.definition())) for s in wn_senses if s.pos() in upos2wn[bare_np.root.pos_]
            }
            if temp:
                bare_np._.wn_senses = temp
                set_wn_polysemous(bare_np, len(temp))
                bare_np._.parent_span = (nc.start, nc.end)

                return (i_s, i_e)


def get_parent_spans(u, ncs):
    """
    u: utterance - spaCy Doc
    ncs: list of noun chunks in u
    -> update ._.parent_span of each target (entities, sentiment words) (WordNet noun phrases are handled separately)
    """
    # dict - key: spaCy (root) Token index, value: list of targets (spaCy Token/Span)
    input = defaultdict(list)
    for e in u.ents:
        input[e.root.i].append(e)

    for w in u:
        if w._.sentiment.assessments:
            input[w.i].append(w)

    input_keys = sorted(input.keys())
    # index of sorted input keys
    k = 0
    # index of noun chunk in ncs
    i = 0

    while k<len(input_keys) and i<len(ncs):
        if input_keys[k]<ncs[i].end:
            if ncs[i].start<=input_keys[k]:
                for x in input[input_keys[k]]:
                    x._.parent_span = (ncs[i].start, ncs[i].end)
            k += 1
        # elif ncs[i].end<=input_keys[k]:
        else:
            i += 1


def get_clause_type(s):
    """
    s: spaCy sent
    -> str of clause type of s (TODO: take into account info from dependency tree and morphological features of main verb - actually, this is not reliable)
    """
    if s[-1].text=='?':
        s._.clause_type = '?'
    elif s[-1].text=='!':
        s._.clause_type = '!'
        s._.social_foci.append(-1)
    else:
        s._.clause_type = '.'


def get_morph_features(w, nlp):
    """
    Assumption: t already has its PTB tag
    w: spaCy token
    -> dict whose keys are categories of morphological features
    """
    # example of morph_raw: {74: 100, 'Tense_past': True, 'VerbForm_fin': True}
    morph = dict()
    morph_raw = nlp.vocab.morphology.tag_map[w.tag_]
    for k in morph_raw:
        if morph_raw[k]==True:
            k, _, v = k.partition('_') 
            morph[k] = v
    # return morph
    if morph:
        w._.morph = morph


def get_social_focus(w):
    """
    w: spaCy token
    -> 'Self'/'the Other'/None
    """
    # https://universaldependencies.org/u/feat/PronType.html
    if w._.morph and 'PronType' in w._.morph and w._.morph['PronType']=='prs':
        if w.text.lower() in {'i', 'me'}:
            w._.social_focus = "Self"
            if w.sent._.clause_type in {'.', '!'}:
                w.sent._.social_foci.append(w.i)
        elif w.text.lower() in {'you'}:
            w._.social_focus = "the Other"
            if w.sent._.clause_type in {'?'}:
                w.sent._.social_foci.append(w.i)


def get_spacy_utterance(id, speaker, text, nlp, nlpt):
    """
    id: int 
    speaker, text: string objects
    nlp: spaCy core model
    nlpt: spacy-transformers model
    # is_self: if computer is Self
    -> spaCy doc
    """
    doc = nlp(text)
    doc._.id = id
    doc._.speaker = speaker

    doc._.transformer = nlpt(text)

    for s in doc.sents:
        get_clause_type(s)

    for w in doc:
        # it seems that WordNet doesn't support modal verbs, which is mapped to UPOS 'VERB' (instead of 'AUX' - why?)
        # https://v2.spacy.io/api/annotation#pos-en
        if w.pos_ in upos_c and w.tag_ not in {'MD'}:
            if w._.wordnet.synsets():
                get_wordnet_senses(w, nlpt)

        get_morph_features(w,nlp)
        get_social_focus(w)

    # index of noun chunk
    id_nc = 0

    ncs = list(doc.noun_chunks)

    for nc in ncs:
        wn_np = get_wn_np(nc, nlpt) # Span._.wn_senses/wn_polysemous
        if wn_np:
            doc._.wn_nps.append((id_nc, *wn_np))
        id_nc =+ 1

    get_parent_spans(doc, ncs)
    
    return doc


def get_preceding_sents(n, cached, s):
    """
    n: int - number of sents
    cached: list of n preceding sents of the previous sent
    s: relevant info of current sent
    -> newly updated cached    
    """
    if len(cached)>=n: # "being equal" is enough
        cached.pop(0)
    cached.append(s)

def get_preceding_sent(x):
    """
    -> object 's' as input of get_preceding_sents()
    """
    output = dict()
    if isinstance(x, spacy.tokens.Span):
        # REPEATED CONDITIONS
        output['noun_chunks'] = [nc for nc in x.noun_chunks if nc.root._.wn_polysemous != 'n/a' or nc.root.pos_ in {'PROPN'}]
    else: # utterance generated by Dimo
        # if x['informational']:
        #     output['noun_chunks'] = x['informational']
        # else:
        #     output['noun_chunks'] = list()
        output['noun_chunks'] = list()
    return output

def get_preceding_ncs(pss):
    """
    pss: list of precedings sents
    -> list of noun chunks in pss
    """
    # return [nc for s in pss for nc in s.noun_chunks]
    # restricted to WordNet vocab
    # return [nc for s in pss for nc in s['noun_chunks'] if nc.root._.wn_polysemous != 'n/a']
    # print(pss)
    # return [nc for s in pss for nc in s['noun_chunks']]
    return [nc for s in pss for nc in s['noun_chunks'] if nc.root._.wn_senses]


def get_vector(x):
    """
    x: spaCy (word) token/span
    -> vector of x
    """
    if isinstance(x, Token):
        if x._.modified_content:
            return x._.modified_content[-1][x._.modified_content[1]]
        return x.doc._.transformer[x.i]
    
    # x is spaCy Span    
    if x._.modified_content:
        return x._.modified_content[-1][x._.modified_content[1][0]:x._.modified_content[1][1]]
    return x.doc._.transformer[x.start:x.end]

def get_wn_sense_vector(x):
    """
    x: spaCy (word) token/span
    -> vector of WordNet sense of x/ ...
    """
    # based on contextual vectors of x
    if x._.wn_senses:
        return x._.wn_senses[x._.wn_sense][1]
    
    # backup variant
    return get_vector(x)


def get_closest_nc(pncs, x):
    """
    pncs: list of preceding noun chunks (Assumption: pncs is not empty)
    x: a spaCy token/span of a WordNet polysemous word/phrase (whose vector has not been modified yet)
    -> noun chunk closest to x, corresponding similarity score
    """    
    # based on vectors of contextual wn senses of roots of pncs
    similarity_scores = {nc: get_wn_sense_vector(nc.root).similarity(get_vector(x)) for nc in pncs}
    return max(similarity_scores.items(), key=itemgetter(1))


def get_modified_content(x, pncs, threshold, nlpt):
    """
    x: a spaCy token/span of a WordNet polysemous word/phrase
    pncs: list of noun chunks in precedings sents
    threshold: float - similarity score
    -> ((str of modified context, index/(start index, end index) of x in modified context), spacy-transformers doc of modified context)
    """
    # https://stackoverflow.com/questions/62367044/is-it-better-to-join-or-format-strings
    nc, ss = get_closest_nc(pncs, x)
    if ss >= threshold:
        modified_text = f"{nc}, {x.sent}"
        
        if isinstance(x, Token):
            return (modified_text, sum([len(nc), 1, x.i, -x.sent.start]), nlpt(modified_text))
        # x is spaCy Span
        modified_start_index = sum([len(nc), 1, x.start, -x.sent.start])
        return (modified_text, (modified_start_index, modified_start_index + len(x)), nlpt(modified_text))

# TODOs: handle utterance generated by computer - skip it
def get_notyetforwardlooking_nc(cached_corel, s):
    """
    cached_corel: list of noun chunks that haven't had any forward-looking corel yet ||implication: each noun chunk has no more than one forward-looking corel
    s: current sent
    """
    for nc in s.noun_chunks:
        # REPEATED CONDITIONS
        if (nc.root._.wn_polysemous != 'n/a' or nc.root.pos_ in {'PROPN'}) and not nc.root._.corel_forward:
            cached_corel.append(nc)


def set_corels(w, threshold, top_k, cached_corel):
    """
    w: spaCy token (e.g. noun chunk's root)
    threshold: minimal value of similarity
    top-k: w's k most similar noun chunks in cached_corel
    cached_corel: see get_notyetforwardlooking_nc()
    """
    # based on the vector of contextual WordNet sense of w
    similarity_scores = {nc: get_wn_sense_vector(nc.root).similarity(get_wn_sense_vector(w)) for nc in cached_corel}

    w._.corel_backward = {'span': list()}
    for item in sorted(similarity_scores.items(), key=itemgetter(1), reverse=True)[:top_k]:
        if item[1]>=threshold:
            # forward-looking corel
            # item[0].root._.corel_forward = {'token': (item[1], w)}
            # to deal with spaCy pickle error
            # [E111] Pickling a token is not supported, because tokens are only views of the parent Doc and can't exist on their own. A pickled token would always have to include its Doc and Vocab, which has practically no advantage over pickling the parent Doc directly. So instead of pickling the token, pickle the Doc it belongs to.
            # HARDCODE
            item[0].root._.corel_forward = {'token': (item[1], w.i, 1, w.doc._.id)}
            cached_corel.remove(item[0])
            # backward-looking corel
            w._.corel_backward['span'].append((item[1], item[0].start, len(item[0]), item[0].doc._.id))


#%%
# spacy2folia
# references: generate_auto_annotation.py, dimo/jupyter_reader.py, x2folia2y/jupyter_praed.py
import folia.main as folia

# obsidian://open?vault=Obsidian%20Vault&file=dissertation%2FFoLiA
# structure annotation
SET_UTTERANCE = "https://gitlab.com/alexluu_public/folia/set_definitions/-/raw/master/dimo/spacy_utterance.foliaset.xml"
SET_SENTENCE = "https://gitlab.com/alexluu_public/folia/set_definitions/-/raw/master/dimo/spacy_sentence.foliaset.xml"
SET_TOKEN = "https://gitlab.com/alexluu_public/folia/set_definitions/-/raw/master/dimo/token.foliaset.xml"
SET_TOKEN_HIDDEN = "https://gitlab.com/alexluu_public/folia/set_definitions/-/raw/master/dimo/token_hidden.foliaset.xml"

# inline annotation
# SET_POS_UD = "https://gitlab.com/alexluu_public/folia/set_definitions/-/raw/master/dimo/spacy_pos_ud.foliaset.xml"
SET_POS_PTB = "https://gitlab.com/alexluu_public/folia/set_definitions/-/raw/master/dimo/spacy_pos_ptb.foliaset.xml"
SET_LEMMA = "https://gitlab.com/alexluu_public/folia/set_definitions/-/raw/master/dimo/spacy_lemma.foliaset.xml"
SET_SENSE = "https://gitlab.com/alexluu_public/folia/set_definitions/-/raw/master/dimo/spacy_sense.foliaset.xml"

# span annotation
SET_CHUNK = "https://gitlab.com/alexluu_public/folia/set_definitions/-/raw/master/dimo/spacy_chunk.foliaset.xml"
SET_ENTITY = "https://gitlab.com/alexluu_public/folia/set_definitions/-/raw/master/dimo/spacy_entity.foliaset.xml"
SET_DEPENDENCY = "https://gitlab.com/alexluu_public/folia/set_definitions/-/raw/master/dimo/spacy_dependency.foliaset.xml"
SET_SENTIMENT = "https://gitlab.com/alexluu_public/folia/set_definitions/-/raw/master/dimo/spacy_sentiment.foliaset.xml"
# coherence coreference
# SET_COREFERENCE = "https://gitlab.com/alexluu_public/folia/set_definitions/-/raw/master/dimo/spacy_coreference.foliaset.xml"
# as it is cumbersome to add corel info to Self's utterances, which don't really need the spaCy-based analysis, I won't include corel info in FoLiA file

#%%
def spacy2folia(d,id):
    """
    d: dialog - list of utterances - spaCy docs/strings
    id: str of id of FoLiA document (see _finish() in jupyter_dimo.py)
    """
    # CREATE A NEW FOLIA DOCUMENT - https://foliapy.readthedocs.io/en/latest/folia.html#creating-a-new-document
    doc_o = folia.Document(id = id)

    # DECLARATIONS OF ANNOTATION TYPES - https://foliapy.readthedocs.io/en/latest/folia.html#declarations
    # https://foliapy.readthedocs.io/en/latest/folia.
    # "spacy2folia" as a single processor for all annotation performed by this script
    # https://foliapy.readthedocs.io/en/latest/folia.html#structure-annotation-types
    p = doc_o.declare(folia.Utterance, set=SET_UTTERANCE, processor=folia.Processor(name="spaCy2FoLiA"))
    doc_o.declare(folia.Sentence, set=SET_SENTENCE)
    doc_o.declare(folia.Word, set=SET_TOKEN)
    doc_o.declare(folia.Hiddenword, set=SET_TOKEN_HIDDEN)
    # https://foliapy.readthedocs.io/en/latest/folia.html#inline-annotation-types
    # doc_o.declare(folia.PosAnnotation, set=SET_POS_UD)
    doc_o.declare(folia.PosAnnotation, set=SET_POS_PTB)
    doc_o.declare(folia.LemmaAnnotation, set=SET_LEMMA)
    doc_o.declare(folia.SenseAnnotation, set=SET_SENSE)
    # https://foliapy.readthedocs.io/en/latest/folia.html#span-annotation-types
    doc_o.declare(folia.Chunk, set=SET_CHUNK)
    doc_o.declare(folia.Entity, set=SET_ENTITY)
    doc_o.declare(folia.Dependency, set=SET_DEPENDENCY)
    doc_o.declare(folia.Sentiment, set=SET_SENTIMENT)
    # doc_o.declare(folia.CoreferenceChain, set=SET_COREFERENCE)
    # https://foliapy.readthedocs.io/en/latest/folia.html#higher-order-annotations
    doc_o.declare(folia.Description)
    doc_o.declare(folia.Comment)

    # ADDING STRUCTURE & ANNOTATIONS - https://foliapy.readthedocs.io/en/latest/folia.html#adding-structure, https://foliapy.readthedocs.io/en/latest/folia.html#adding-annotations
    # https://folia.readthedocs.io/en/latest/introduction.html#speech-1
    conversation = doc_o.append(folia.Speech)

    for u in d:
        # if not u._.is_self:
        if isinstance(u, spacy.tokens.Doc):
            speaker = u._.speaker
            # u is https://v2.spacy.io/api/doc
            utterance = conversation.append(folia.Utterance, speaker=speaker, cls='Other') # id cannot be 0
            # hidden token of speaker
            hw_speaker = utterance.append(folia.Hiddenword, f'{speaker}', cls='speaker')
            tc_speaker = hw_speaker.annotation(folia.TextContent)
            tc_speaker.append(folia.Description, value=u.text)

            for s in u.sents:            
                # s is https://v2.spacy.io/api/span
                sentence = utterance.append(folia.Sentence, set=SET_SENTENCE, cls=s._.clause_type)
                
                # spaCy words
                ws = list(s)

                for w in ws:
                    # w is https://v2.spacy.io/api/token
                    space = (w.whitespace_ != "")
                    word = sentence.append(
                        folia.Word,
                        w.text, 
                        space=space
                    )
                    # word.append(folia.PosAnnotation, set=SET_POS_UD, cls=w.pos_)
                    pos_ptb = word.append(folia.PosAnnotation, set=SET_POS_PTB, cls=w.tag_)
                    pos_ptb.append(folia.Description, value = w.pos_)
                    # https://foliapy.readthedocs.io/en/latest/folia.html#features
                    if w._.morph:
                        # print(w, w._.morph)
                        for (k,v) in w._.morph.items():
                            pos_ptb.append(folia.Feature, subset=k, cls=v)

                    word.append(folia.LemmaAnnotation, cls=w.lemma_)
                    if w._.wn_sense:
                        sense = word.append(folia.SenseAnnotation, cls=w._.wn_sense)
                        # reference: get_wordnet_senses(), get_wordnet_sense(), get_wn_sense_vector()
                        sense.append(folia.Description, value=w._.wn_senses[w._.wn_sense][0])
                        if w._.wn_polysemous=='y':
                            # sn: sense name (str), sd: sense definition (str)
                            senses = '\n\n'.join([f'{sn}: {sd}' for sn,(sd,_) in w._.wn_senses.items()])
                            sense.append(folia.Comment, value=senses)
                            
                            if w._.modified_content:
                                modified_context = w._.modified_content[0]
                                modified_index = w._.modified_content[1]
                                
                                tc = word.annotation(folia.TextContent)
                                tc.append(folia.Description, value=f'Modified context: {modified_context}\nModified index: {modified_index}')
                        

                words = list(sentence.select(folia.Word))
                
                # within a sentence
                layer_dep = sentence.append(folia.DependenciesLayer)
                # reference: https://github.com/proycon/spacy2folia/blob/master/spacy2folia/spacy2folia.py
                # print(ws)
                # print(words)
                for w in ws:
                    if w.dep_:
                        # print(w, w.i, w.head.i, s.start)
                        w_dep = words[w.i-s.start]
                        w_hd  = words[w.head.i-s.start]
                        if w_hd and w_dep:
                            dependency = layer_dep.append(folia.Dependency, cls=w.dep_)
                            # https://github.com/proycon/foliapy/blob/master/folia/main.py
                            # DependencyHead = Headspan #alias, backwards compatibility with FoLiA 0.8
                            dependency.append(folia.DependencyHead, w_hd)
                            dependency.append(folia.DependencyDependent, w_dep)
            
            # within an utterance
            # words_utt = list(utterance.select(folia.Word)) # this includes wref in dependency trees
            words_utt = [w for s in utterance.sentences() for w in s.words()]

            layer_chunk = utterance.append(folia.ChunkingLayer)

            # reference: https://github.com/proycon/spacy2folia/blob/master/spacy2folia/spacy2folia.py
            for nc in u.noun_chunks:
                contents = words_utt[nc.start: nc.end]
                layer_chunk.append(folia.Chunk, cls=nc.label_, contents=contents)
            
            for wn_np in u._.wn_nps:
                contents = words_utt[wn_np[1]: wn_np[2]]
                chunk = layer_chunk.append(folia.Chunk, cls="NP_WN", contents=contents)
                # spaCy Span of wn_np
                p = u[wn_np[1]:wn_np[2]] # 'p' for 'phrase'
                if p._.wn_sense:
                    chunk.append(folia.Description, value=p._.wn_senses[p._.wn_sense][0])
                    if p._.wn_polysemous=='y':
                        # sn: sense name (str), sd: sense definition (str)
                        senses = '\n\n'.join([f'{sn}: {sd}' for sn,(sd,_) in p._.wn_senses.items()])
                        chunk.append(folia.Comment, value=senses)

            layer_ent = utterance.append(folia.EntitiesLayer)

            # reference: https://github.com/proycon/spacy2folia/blob/master/spacy2folia/spacy2folia.py
            for e in u.ents:
                contents = words_utt[e.start: e.end]
                layer_ent.append(folia.Entity, cls=e.label_, contents=contents)

            # sentiment
            # https://pypi.org/project/spacytextblob/0.1.7/
            # https://textblob.readthedocs.io/en/latest/quickstart.html#sentiment-analysis
            # https://textblob.readthedocs.io/en/latest/api_reference.html#textblob.blob.TextBlob.sentiment
            layer_sentiment = utterance.append(folia.SentimentLayer)

            for w in u:
                sentiment_assessments = w._.sentiment.assessments
                if sentiment_assessments:
                    # https://textblob.readthedocs.io/en/latest/quickstart.html#sentiment-analysis
                    polarity = w._.sentiment.polarity
                    # reference: https://folia.readthedocs.io/en/latest/sentiment_annotation.html
                    # for simplicity: cls is subjectivity, which doesn't conform to FoLiA convention because subjectivity in FoLiA framework is an inline annotation (https://foliapy.readthedocs.io/en/latest/_autosummary/folia.main.SubjectivityAnnotation.html#)
                    sentiment = layer_sentiment.append(folia.Sentiment, cls=str(w._.sentiment.subjectivity), polarity="positive" if polarity>0 else "negative", strength=str(polarity))
                    sentiment.append(folia.Headspan, words_utt[w.i])

        else:
            utterance = conversation.append(folia.Utterance, speaker=u['speaker'], cls='Self')
            # https://foliapy.readthedocs.io/en/latest/_autosummary/folia.main.TextContent.html
            # hidden token of speaker
            hw_speaker = utterance.append(folia.Hiddenword, u['speaker'], cls='speaker')
            tc_speaker = hw_speaker.annotation(folia.TextContent)
            # tc_speaker.append(folia.Description, value=u)
            tc_speaker.append(folia.Description, value=u['text'])
            # utterance.append(folia.TextContent, u)
            # HARDCODE: '.'
            sentence = utterance.append(folia.Sentence, set=SET_SENTENCE, cls='.')
            # sentence.append(folia.TextContent, u)
            sentence.append(folia.TextContent, u['text'])

    doc_o.save(''.join([id, '.folia.xml']))


# # https://github.com/explosion/spaCy/blob/v2.3.5/spacy/attrs.pyx
# morph_keys = [
#     "PunctType",
#     "PunctSide",
#     "Other",
#     "Degree",
#     "AdvType",
#     "Number",
#     "VerbForm",
#     "PronType",
#     "Aspect",
#     "Tense",
#     "PartType",
#     "Poss",
#     "Hyph",
#     "ConjType",
#     "NumType",
#     "Foreign",
#     "VerbType",
#     "NounType",
#     "Gender",
#     "Mood",
#     "Negative",
#     "Tense",
#     "Voice",
#     "Abbr",
#     "Derivation",
#     "Echo",
#     "Foreign",
#     "NameType",
#     "NounType",
#     "NumForm",
#     "NumValue",
#     "PartType",
#     "Polite",
#     "StyleVariant",
#     "PronType",
#     "AdjType",
#     "Person",
#     "Variant",
#     "AdpType",
#     "Reflex",
#     "Negative",
#     "Mood",
#     "Aspect",
#     "Case",
#     "Polarity",
#     "PrepCase",
#     "Animacy",  # U20
# ]

# # https://gitlab.com/alexluu_public/folia/set_definitions/-/blob/master/dimo/spacy_pos_ptb.foliaset.xml
# # https://gitlab.com/alexluu_public/folia/set_definitions/-/raw/master/dimo/spacy_pos_ptb.foliaset.xml
# text_for_folia_set_definition = '\n    '.join([f'<subset xml:id="{x}" type="open"></subset>' for x in sorted(morph_keys)])