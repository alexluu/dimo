graph [
  directed 1
  multigraph 1
  last_utterance "13.-1.0.0&#10;Dimo"
  root "13.-1.0.0&#10;Dimo"
  corel_nodes "[]"
  node [
    id 0
    label "00.-1.0.0&#10;Anonymous"
    speaker "Anonymous"
    utterance_id 0
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 0"
  ]
  node [
    id 1
    label "00.02.3.0&#10;a new job"
    text "a new job"
    shape "box"
    color "cyan"
    size 10
    syn_role "dobj (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    title "syn_role: dobj (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']"
  ]
  node [
    id 2
    label "00.05.1.0&#10;today"
    text "today"
    shape "box"
    color "cyan"
    size 10
    syn_role "npadvmod (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "ent"
    title "syn_role: npadvmod (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['ent']"
  ]
  node [
    id 3
    label "00.00.1.0&#10;I"
    text "I"
    shape "box"
    color "yellow"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "."
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['sfs']&#10;clause_type: ."
  ]
  node [
    id 4
    label "00.03.1.0&#10;new"
    text "new"
    shape "box"
    color "pink"
    size 11
    syn_role "amod (head: dobj)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.13636363636363635
    title "syn_role: amod (head: dobj)&#10;wrt_ROOT: right&#10;cats: ['exp']&#10;polarity: 0.13636363636363635"
  ]
  node [
    id 5
    label "01.-1.0.0&#10;Dimo"
    speaker "Dimo"
    utterance_id 1
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 1"
  ]
  node [
    id 6
    label "01.-1.0.1&#10;a new job"
    reference "00.02.3.0&#10;a new job"
    shape "box"
    color "cyan"
    size 10
    title "reference: 00.02.3.0&#10;a new job"
  ]
  node [
    id 7
    label "01.-1.0.3"
    reference "00.00.1.0&#10;I"
    shape "box"
    color "yellow"
    size 10
    title "reference: 00.00.1.0&#10;I"
  ]
  node [
    id 8
    label "02.-1.0.0&#10;Anonymous"
    speaker "Anonymous"
    utterance_id 2
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 2"
  ]
  node [
    id 9
    label "02.03.3.0&#10;a computational linguist"
    text "a computational linguist"
    shape "box"
    color "cyan"
    size 10
    syn_role "attr (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.6043524026874281, 2, 3, 0)]"
    title "syn_role: attr (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.6043524026874281, 2, 3, 0)]"
  ]
  node [
    id 10
    label "02.00.1.0&#10;I"
    text "I"
    shape "box"
    color "yellow"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "sfs"
    clause_type "."
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['sfs']&#10;clause_type: ."
  ]
  node [
    id 11
    label "02.04.2.0&#10;computational linguist"
    text "computational linguist"
    shape "box"
    color "cyan"
    size 10
    syn_role "attr (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "wn_np"
    wn_sense "computational_linguist.n.01"
    definition "someone trained in computer science and linguistics who uses computers for natural language processing"
    title "syn_role: attr (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['wn_np']&#10;wn_sense: computational_linguist.n.01&#10;definition: someone trained in computer science and linguistics who uses computers for natural language processing"
  ]
  node [
    id 12
    label "03.-1.0.0&#10;Dimo"
    speaker "Dimo"
    utterance_id 3
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 3"
  ]
  node [
    id 13
    label "03.-1.0.1&#10;a computational linguist"
    reference "02.03.3.0&#10;a computational linguist"
    shape "box"
    color "cyan"
    size 10
    title "reference: 02.03.3.0&#10;a computational linguist"
  ]
  node [
    id 14
    label "03.-1.0.3"
    reference "02.00.1.0&#10;I"
    shape "box"
    color "yellow"
    size 10
    title "reference: 02.00.1.0&#10;I"
  ]
  node [
    id 15
    label "04.-1.0.0&#10;Anonymous"
    speaker "Anonymous"
    utterance_id 4
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 4"
  ]
  node [
    id 16
    label "04.04.3.0&#10;my favorite job"
    text "my favorite job"
    shape "box"
    color "cyan"
    size 10
    syn_role "attr (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.6043524026874281, 3, 3, 2)]"
    title "syn_role: attr (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['nc']&#10;corels: [(0.6043524026874281, 3, 3, 2)]"
  ]
  node [
    id 17
    label "04.05.1.0&#10;favorite"
    text "favorite"
    shape "box"
    color "pink"
    size 15
    syn_role "amod (head: attr)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.5
    title "syn_role: amod (head: attr)&#10;wrt_ROOT: right&#10;cats: ['exp']&#10;polarity: 0.5"
  ]
  node [
    id 18
    label "05.-1.0.0&#10;Dimo"
    speaker "Dimo"
    utterance_id 5
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 5"
  ]
  node [
    id 19
    label "06.-1.0.0&#10;Anonymous"
    speaker "Anonymous"
    utterance_id 6
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 6"
  ]
  node [
    id 20
    label "06.02.1.0&#10;right"
    text "right"
    shape "box"
    color "pink"
    size 13
    syn_role "acomp (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.2857142857142857
    title "syn_role: acomp (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['exp']&#10;polarity: 0.2857142857142857"
  ]
  node [
    id 21
    label "07.-1.0.0&#10;Dimo"
    speaker "Dimo"
    utterance_id 7
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 7"
  ]
  node [
    id 22
    label "08.-1.0.0&#10;Anonymous"
    speaker "Anonymous"
    utterance_id 8
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 8"
  ]
  node [
    id 23
    label "08.00.4.0&#10;My company's name"
    text "My company's name"
    shape "box"
    color "cyan"
    size 10
    syn_role "nsubjpass (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.7301246988253927, 4, 3, 4)]"
    title "syn_role: nsubjpass (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['nc']&#10;corels: [(0.7301246988253927, 4, 3, 4)]"
  ]
  node [
    id 24
    label "08.07.3.0&#10;the Charles river"
    text "the Charles river"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: agent)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "nc"
    title "syn_role: pobj (head: agent)&#10;wrt_ROOT: right&#10;cats: ['nc']"
  ]
  node [
    id 25
    label "08.08.2.0&#10;Charles river"
    text "Charles river"
    shape "box"
    color "cyan"
    size 10
    syn_role "pobj (head: agent)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "wn_np"
    wn_sense "charles.n.09"
    definition "a river in eastern Massachusetts that empties into Boston Harbor and that separates Cambridge from Boston"
    title "syn_role: pobj (head: agent)&#10;wrt_ROOT: right&#10;cats: ['wn_np']&#10;wn_sense: charles.n.09&#10;definition: a river in eastern Massachusetts that empties into Boston Harbor and that separates Cambridge from Boston"
  ]
  node [
    id 26
    label "08.08.1.0&#10;Charles"
    text "Charles"
    shape "box"
    color "cyan"
    size 10
    syn_role "compound (head: pobj)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "ent"
    title "syn_role: compound (head: pobj)&#10;wrt_ROOT: right&#10;cats: ['ent']"
  ]
  node [
    id 27
    label "09.-1.0.0&#10;Dimo"
    speaker "Dimo"
    utterance_id 9
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 9"
  ]
  node [
    id 28
    label "10.-1.0.0&#10;Anonymous"
    speaker "Anonymous"
    utterance_id 10
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 10"
  ]
  node [
    id 29
    label "10.03.2.0&#10;its bank"
    text "its bank"
    shape "box"
    color "cyan"
    size 10
    syn_role "nsubj (head: ROOT)"
    wrt_ROOT "left"
    cats "_networkx_list_start"
    cats "nc"
    corels "[(0.8916567935541966, 7, 3, 8)]"
    title "syn_role: nsubj (head: ROOT)&#10;wrt_ROOT: left&#10;cats: ['nc']&#10;corels: [(0.8916567935541966, 7, 3, 8)]"
  ]
  node [
    id 30
    label "10.06.1.0&#10;gorgeous"
    text "gorgeous"
    shape "box"
    color "pink"
    size 17
    syn_role "acomp (head: ROOT)"
    wrt_ROOT "right"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.7
    title "syn_role: acomp (head: ROOT)&#10;wrt_ROOT: right&#10;cats: ['exp']&#10;polarity: 0.7"
  ]
  node [
    id 31
    label "11.-1.0.0&#10;Dimo"
    speaker "Dimo"
    utterance_id 11
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 11"
  ]
  node [
    id 32
    label "11.-1.0.1&#10;its bank"
    reference "10.03.2.0&#10;its bank"
    shape "box"
    color "cyan"
    size 10
    title "reference: 10.03.2.0&#10;its bank"
  ]
  node [
    id 33
    label "11.-1.0.2"
    reference "10.06.1.0&#10;gorgeous"
    shape "box"
    color "pink"
    size 10
    title "reference: 10.06.1.0&#10;gorgeous"
  ]
  node [
    id 34
    label "12.-1.0.0&#10;Anonymous"
    speaker "Anonymous"
    utterance_id 12
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 12"
  ]
  node [
    id 35
    label "12.00.1.0&#10;Definitely"
    text "Definitely"
    shape "box"
    color "pink"
    size 10
    syn_role "ROOT (head: ROOT)"
    wrt_ROOT "same"
    cats "_networkx_list_start"
    cats "exp"
    polarity 0.0
    title "syn_role: ROOT (head: ROOT)&#10;wrt_ROOT: same&#10;cats: ['exp']&#10;polarity: 0.0"
  ]
  node [
    id 36
    label "13.-1.0.0&#10;Dimo"
    speaker "Dimo"
    utterance_id 13
    shape "box"
    color "lightgray"
    size 10
    title "utterance_id: 13"
  ]
  edge [
    source 0
    target 1
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 0
    target 4
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 0
    target 3
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 0
    target 5
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 1
    target 4
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 1
    target 2
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 1
    target 9
    key 0
    label "corel"
    value 6
    weight 0.6043524026874281
    title "weight: 0.6043524026874281"
  ]
  edge [
    source 5
    target 6
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 5
    target 7
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 5
    target 8
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 6
    target 1
    key 0
    label "reference"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 7
    target 3
    key 0
    label "reference"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 8
    target 9
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 8
    target 10
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 8
    target 12
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 9
    target 11
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 9
    target 11
    key 1
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 9
    target 16
    key 0
    label "corel"
    value 6
    weight 0.6043524026874281
    title "weight: 0.6043524026874281"
  ]
  edge [
    source 12
    target 13
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 12
    target 14
    key 0
    label "NOR"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 12
    target 15
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 13
    target 9
    key 0
    label "reference"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 14
    target 10
    key 0
    label "reference"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 15
    target 16
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 15
    target 17
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 15
    target 18
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 16
    target 17
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 16
    target 23
    key 0
    label "corel"
    value 7
    weight 0.7301246988253927
    title "weight: 0.7301246988253927"
  ]
  edge [
    source 18
    target 19
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 19
    target 20
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 19
    target 21
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 21
    target 22
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 22
    target 23
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 22
    target 27
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 23
    target 24
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 24
    target 25
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 24
    target 26
    key 0
    label "child"
    value 2
    weight 0.2
    title "weight: 0.2"
  ]
  edge [
    source 24
    target 26
    key 1
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 24
    target 29
    key 0
    label "corel"
    value 8
    weight 0.8916567935541966
    title "weight: 0.8916567935541966"
  ]
  edge [
    source 26
    target 25
    key 0
    label "hor"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 27
    target 28
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 28
    target 29
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 28
    target 30
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 28
    target 31
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 31
    target 32
    key 0
    label "INF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 31
    target 33
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 31
    target 34
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 32
    target 29
    key 0
    label "reference"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 33
    target 30
    key 0
    label "reference"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 34
    target 35
    key 0
    label "AFF"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
  edge [
    source 34
    target 36
    key 0
    label "ver"
    value 1
    weight 0.1
    title "weight: 0.1"
  ]
]
